#include <string.h>

extern "C" {
#include "api/types/WiFi/utility/wl_definitions.h"
#include "api/types/WiFi/utility/wl_types.h"
}

#include "api/types/WiFi/WiFi.h"
#include "api/types/WiFi/WiFiClient.h"

#include "Wiconnect.h"

extern Wiconnect *driver;


/*************************************************************************************************/
WiFiClient::WiFiClient() :  _socket(sizeof(rxBuffer), rxBuffer, sizeof(txBuffer), txBuffer)
{
}

/*************************************************************************************************/
//WiFiClient::WiFiClient(uint8_t sock)
//{
//}

/*************************************************************************************************/
int WiFiClient::connect(const char* host, uint16_t port)
{
    if(driver == NULL)
    {
        return 255;
    }
    else if(_socket.isConnected())
    {
        return 0;
    }
    else
    {
        WiconnectResult result;

        while(WICONNECT_IS_PROCESSING(result, driver->tcpConnect(_socket, host, port)))
        {
        }

        return (result == WICONNECT_SUCCESS) ? 1 : 0;
    }
}

/*************************************************************************************************/
int WiFiClient::connect(IPAddress ip, uint16_t port)
{
    char buf[17];
    return connect(Wiconnect::ipToStr((uint32_t)ip, buf), port);
}

/*************************************************************************************************/
size_t WiFiClient::write(uint8_t b)
{
    return write(&b, 1);
}

/*************************************************************************************************/
size_t WiFiClient::write(const uint8_t *buf, size_t size)
{
    if(driver == NULL || size==0 || !_socket.isConnected())
    {
        setWriteError();
        return 0;
    }
    return (_socket.write(buf, size, true) == WICONNECT_SUCCESS) ? size : 0;
}

/*************************************************************************************************/
int WiFiClient::available()
{
    return available(0);
}

/*************************************************************************************************/
int WiFiClient::available(uint8_t autoClose)
{
    WiconnectResult result;
    bool dataAvailable;
    if(driver == NULL || !_socket.isConnected())
    {
        return 0;
    }

    while(WICONNECT_IS_PROCESSING(result, _socket.poll(&dataAvailable, autoClose)))
    {
    }
    return dataAvailable;
}

/*************************************************************************************************/
int WiFiClient::read()
{
    if (available())
    {
        uint8_t b;
        WiconnectResult result = _socket.getc(&b);
        return (result == WICONNECT_SUCCESS) ? b : -1;
    }
    else
    {
        return -1;
    }
}

/*************************************************************************************************/
int WiFiClient::read(uint8_t* buf, size_t size)
{
    if (available())
    {
        WiconnectResult result;
        uint16_t bytesRead;
        while(WICONNECT_IS_PROCESSING(result, _socket.read(buf, size, &bytesRead)))
        {
        }

        return (result == WICONNECT_SUCCESS) ? bytesRead : -1;
    }
    else
    {
        return -1;
    }
}

/*************************************************************************************************/
int WiFiClient::peek()
{
    // TODO: support this call
    return -1;
}

/*************************************************************************************************/
void WiFiClient::flush()
{
    while (available())
        read();
}

/*************************************************************************************************/
void WiFiClient::stop()
{
    WiconnectResult result;

    while(WICONNECT_IS_PROCESSING(result, _socket.close()))
    {
    }
}

/*************************************************************************************************/
uint8_t WiFiClient::connected()
{
    if(!_socket.isConnected())
    {
        return 0;
    }
    else
    {
        WiconnectResult result;
        bool connected = false;

        while(WICONNECT_IS_PROCESSING(result, _socket.poll(&connected, true)))
        {
        }

        return _socket.isConnected();
    }
}

/*************************************************************************************************/
uint8_t WiFiClient::status()
{
    return connected();
}

/*************************************************************************************************/
WiFiClient::operator bool()
{
    return true;
}
