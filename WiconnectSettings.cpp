/**
 * ACKme WiConnect Host Library is licensed under the BSD licence:
 *
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 */

#include "Wiconnect.h"
#include "internal/common.h"



/*************************************************************************************************/
WiconnectResult Wiconnect::setSetting(const char *settingStr, uint32_t value)
{
	WiconnectResult result;

	_CHECK_OTHER_COMMAND_EXECUTING();

	result = sendCommand("set %s %u", settingStr, value);

    _CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult Wiconnect::setSetting(const char *settingStr, const char *value)
{
	WiconnectResult result;

	_CHECK_OTHER_COMMAND_EXECUTING();

	result = sendCommand("set %s %s", settingStr, value);

    _CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult Wiconnect::getSetting(const char *settingStr, uint32_t *valuePtr)
{
    WiconnectResult result = WICONNECT_ERROR;


	_CHECK_OTHER_COMMAND_EXECUTING();

	if(WICONNECT_SUCCEEDED(result, sendCommand("get %s", settingStr)))
	{
		result = responseToUint32(valuePtr);
	}

    _CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult Wiconnect::getSetting(const char *settingStr, char **valuePtr)
{
    WiconnectResult result = WICONNECT_ERROR;


	_CHECK_OTHER_COMMAND_EXECUTING();

	if(WICONNECT_SUCCEEDED(result, sendCommand("get %s", settingStr)))
	{
		*valuePtr = internalBuffer;
	}

    _CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult Wiconnect::getSetting(const char *settingStr, char *valueBuffer, uint16_t valueBufferLen)
{
    WiconnectResult result = WICONNECT_ERROR;


	_CHECK_OTHER_COMMAND_EXECUTING();

	result = sendCommand(valueBuffer, valueBufferLen, "get %s", settingStr);

    _CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult Wiconnect::saveSettings()
{
    WiconnectResult result = WICONNECT_ERROR;


    _CHECK_OTHER_COMMAND_EXECUTING();

    result = sendCommand("save");

    _CHECK_CLEANUP_COMMAND();

    return result;
}
