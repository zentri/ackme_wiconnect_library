/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once

#include "WiconnectTypes.h"


#include "types/LogFunc.h"
#include "types/ReaderFunc.h"
#include "types/Callback.h"
#include "types/TimeoutTimer.h"
#include "types/PeriodicTimer.h"
#include "types/Gpio.h"
#include "types/WiconnectSerial.h"

#ifdef WICONNECT_ASYNC_TIMER_ENABLED
#include "types/QueuedCommand.h"
#include "types/CommandQueue.h"
#endif

#include "NetworkInterface.h"
#include "SocketInterface.h"
#include "FileInterface.h"
#include "GhmInterface.h"


#ifdef WICONNECT_ENABLE_MALLOC
/// These are optional arguments for host specific malloc/free
#define WICONNECT_MALLOC_ARGS , void* (*malloc_)(size_t) = WICONNECT_DEFAULT_MALLOC, void (*free_)(void*) = WICONNECT_DEFAULT_FREE
#else
#define WICONNECT_MALLOC_ARGS
#endif


/**
 * @namespace wiconnect
 */
namespace wiconnect {


/**
 * @ingroup api_core_types
 *
 * @brief The root WiConnect library class. This class
 * inheriets all WiConnect functionality.
 *
 * This class is implemented as a 'singleton'. This means it
 * only needs to be instantiated once. Subsequent class may either
 * use the class instance or the static function: @ref Wiconnect::getInstance()
 *
 */
class Wiconnect : public NetworkInterface,
                  public SocketInterface,
                  public FileInterface,
                  public GhmInterface
{
public:

    /**
     * @brief WiConnect class constructor
     *
     * @note This should only be called once within a program as the WiConnect
     *       library is implemented as a singleton.
     *
     * @note If this constructor is used, then all commands must be supplied with an external response buffer.
     *       This means most the API functions will not work as they use the internal buffer.
     *       It's recommended to use the other constructor that supplies an internal buffer. See @ref setting_alloc
     *
     * @param[in] serialConfig The serial (i.e. UART) configuration connected to a WiConnect module.
     * @param[in] reset Optional, The pin connected to the WiConnect module reset signal. Default: No connection
     * @param[in] wake Optional, The pin connected to the WiConnect module wake signal. Default: No connection
     * @param[in] nonBlocking Optional, indicates if the API blocking mode. See @ref setting_blocking_modes
     */
    Wiconnect(const SerialConfig &serialConfig, Pin reset = PIN_NC, Pin wake = PIN_NC, bool nonBlocking = WICONNECT_DEFAULT_NONBLOCKING WICONNECT_MALLOC_ARGS);

    /**
     * @brief WiConnect class constructor
     *
     * @note This should only be called once within a program as the WiConnect
     *       library is implemented as a singleton.
     *
     * @note This is the recommended construstor as it supplies the WiConnect library with an
     *       internal buffer. Most API calls require the internal buffer.
     *
     * @param[in] serialConfig The serial (i.e. UART) configuration connected to a WiConnect module.
     * @param[in] internalBufferSize The size of the internal buffer. If internalBuffer is NULL, then this size will be dynamically allocated. See @ref setting_alloc
     * @param[in] internalBuffer Optional, a user allocated buffer. See @ref setting_alloc
     * @param[in] reset Optional, The pin connected to the WiConnect module reset signal. Default: No connection
     * @param[in] wake Optional, The pin connected to the WiConnect module wake signal. Default: No connection
     * @param[in] nonBlocking Optional, indicates if the API blocking mode. See @ref setting_blocking_modes
     */
    Wiconnect(const SerialConfig &serialConfig, int internalBufferSize, void *internalBuffer = NULL, Pin reset = PIN_NC, Pin wake = PIN_NC, bool nonBlocking = WICONNECT_DEFAULT_NONBLOCKING WICONNECT_MALLOC_ARGS);
    ~Wiconnect();


    // ------------------------------------------------------------------------


    /**
     * @ingroup  api_core_misc
     *
     * @brief Get instance of previously instantiated Wiconnect Library
     *
     * @return Pointer to instance of @ref Wiconnect Library.
     */
    static Wiconnect* getInstance();

    /**
     * @ingroup  api_core_misc
     *
     * @brief Initialize library and communication link with WiConnect WiFi module.
     *
     * @note This function is always blocking regardless of configured mode.
     *
     * @param[in] bringNetworkUp Flag indicating if the module should try to bring the network up upon initialization.
     * @return Result of initialization. See @ref WiconnectResult
     */
    WiconnectResult init(bool bringNetworkUp=false);

    /**
     * @ingroup  api_core_misc
     *
     * @brief De-initialize library.
     */
    void deinit();

    /**
     * @ingroup api_core_misc
     *
     * @brief Return TRUE if library is able to communicated with WiConnect WiFi module.
     * FALSE else.
     *
     * @return TRUE if library can communicate with WiFi module, FALSE else.
     */
    bool isInitialized();

    /**
     * @ingroup api_core_misc
     *
     * @brief Return TRUE if the WiFi module's firmware supports the SDK version,
     *               FALSE if the WiFi module's firmware needs to be updated. See
     *               @ref updateFirmware() to update the module's firmware.
     *
     * @return TRUE WiFi firmware version is supported, FALSE else
     */
    bool updateRequired();

    /**
     * @ingroup api_core_misc
     *
     * @brief Toggle the WiConnect WiFi module reset signal.
     *
     * @note This only resets the module if the library was instantiated with the 'reset' pin
     *       parameter in the Wiconnect::Wiconnect constructor.
     * @note This method is always blocking. A small (1s) delay is added to ensure the module
     *       has returned from reset and ready.
     *
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult reset();

    /**
     * @ingroup api_core_misc
     *
     * @brief Toggle the WiConnect WiFi moduel wakeup signal.
     *
     * @note This only wakes the module if the library was instantiated with the 'wake' pin
     *       parameter in the Wiconnect::Wiconnect constructor.
     * @note This method is always blocking.
     *
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult wakeup();

    /**
     * @ingroup api_core_misc
     *
     * @brief Flush any received data in serial RX buffer and terminate any commands on WiConnect WiFi module.
     *
     * The delayMs parameter is used as the delay between terminating commands on the module and flushing
     * the serial RX buffer. This is needed because after terminating commands on the module, the module will
     * returns a response. These responses are invalid at this point and should be flushed from the serial RX buffer.
     *
     * @param[in] delayMs Optional, if not specificed this only flushes the serial RX buffer.
     */
    void flush(int delayMs = 500);

    /**
     * @ingroup api_core_misc
     *
     * @brief Return current version of WiConnect WiFi module.
     * @param[in] versionBuffer Optional, Buffer to hold received version string
     * @param[in] versionBufferSize Optional, required if versionBuffer specified.
     * @param[in] completeCallback Optional, callback when version is received. arg1 of callback contains version buffer pointer.
     *
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult getVersion(char *versionBuffer = NULL, int versionBufferSize = 0, const Callback &completeCallback = Callback());


    /**
     * @ingroup api_core_misc
     *
     * @brief Update the wifi module's internal firmware.
     * @param[in] forced Optional, If true, force update of all firmware files to latest version, else only update out-dated files.
     * @param[in] versionStr Optional, If specified, update to specific firmware version, else update to latest version.
     * @param[in] completeCallback Optional, callback when update is complete. 'result' callback argument contains result of update.
     *
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult updateFirmware(bool forced = false, const char *versionStr = NULL, const Callback &completeCallback = Callback());


    // ------------------------------------------------------------------------


    /**
     * @ingroup api_core_send_command
     *
     * @brief Send command to WiConnect WiFi module
     *
     * @note Refer to @ref send_command_desc for more info
     *
     * @param[in] completeCallback Callback when command completes. arg1 of callback contains responseBuffer pointer, arg2 contains the response length
     * @param[in] responseBuffer Buffer to hold command response
     * @param[in] responseBufferLen Length of responseBuffer
     * @param[in] timeoutMs Maximum time in milliseconds this command should execute
     * @param[in] reader Callback for reading data to be read from host and send to module during command
     * @param[in] user User data struct used during read Callback. Library doesn't use this. Set NULL if not used.
     * @param[in] cmd WiConnect command to send to module
     * @param[in] vaList Varaible list of arguments
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult sendCommand(const Callback &completeCallback, char *responseBuffer, int responseBufferLen,
                               TimerTimeout timeoutMs, const ReaderFunc &reader, void *user, const char *cmd, va_list vaList);
    /**
     * @ingroup api_core_send_command
     *
     * @brief Send command to WiConnect WiFi module
     *
     * @note Refer to @ref send_command_desc for more info
     *
     * @param[in] responseBuffer Buffer to hold command response
     * @param[in] responseBufferLen Length of responseBuffer
     * @param[in] timeoutMs Maximum time in milliseconds this command should execute
     * @param[in] reader Callback for reading data to be read from host and send to module during command
     * @param[in] user User data struct used during read Callback. Library doesn't use this. Set NULL if not used.
     * @param[in] cmd WiConnect command to send to module
     * @param[in] vaList Varaible list of arguments
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult sendCommand(char *responseBuffer, int responseBufferLen, TimerTimeout timeoutMs, const ReaderFunc &reader,
                                void *user, const char *cmd, va_list vaList);

    /**
     * @ingroup api_core_send_command
     *
     * @brief Send command to WiConnect WiFi module
     *
     * @note Refer to @ref send_command_desc for more info
     * @note This method supports variable arguments
     *
     * @param[in] responseBuffer Buffer to hold command response
     * @param[in] responseBufferLen Length of responseBuffer
     * @param[in] timeoutMs Maximum time in milliseconds this command should execute
     * @param[in] reader Callback for reading data to be read from host and send to module during command
     * @param[in] user User data struct used during read Callback. Library doesn't use this. Set NULL if not used.
     * @param[in] cmd WiConnect command to send to module
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult sendCommand(char *responseBuffer, int responseBufferLen, TimerTimeout timeoutMs, const ReaderFunc &reader,
                                void *user, const char *cmd, ...);

    /**
     * @ingroup api_core_send_command
     *
     * @brief Send command to WiConnect WiFi module
     *
     * This method uses the library internal buffer.
     *
     * @note Refer to @ref send_command_desc for more info
     * @note This method supports variable arguments
     *
     * @param[in] timeoutMs Maximum time in milliseconds this command should execute
     * @param[in] reader Callback for reading data to be read from host and send to module during command
     * @param[in] user User data struct used during read Callback. Library doesn't use this. Set NULL if not used.
     * @param[in] cmd WiConnect command to send to module
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult sendCommand(TimerTimeout timeoutMs, const ReaderFunc &reader, void *user, const char *cmd, ...);

    /**
     * @ingroup api_core_send_command
     *
     * @brief Send command to WiConnect WiFi module
     *
     * - This method uses the library internal buffer and
     * - default timeout. See setCommandDefaultTimeout()
     *
     * @note Refer to @ref send_command_desc for more info
     * @note This method supports variable arguments
     *
     * @param[in] reader Callback for reading data to be read from host and send to module during command
     * @param[in] user User data struct used during read Callback. Library doesn't use this. Set NULL if not used.
     * @param[in] cmd WiConnect command to send to module
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult sendCommand(const ReaderFunc &reader, void *user, const char *cmd, ...);

    /**
     * @ingroup api_core_send_command
     *
     * @brief Send command to WiConnect WiFi module
     *
     * @note Refer to @ref send_command_desc for more info
     * @note This method supports variable arguments
     *
     * @param[in] responseBuffer Buffer to hold command response
     * @param[in] responseBufferLen Length of responseBuffer
     * @param[in] timeoutMs Maximum time in milliseconds this command should execute
     * @param[in] cmd WiConnect command to send to module
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult sendCommand(char *responseBuffer, int responseBufferLen, TimerTimeout timeoutMs, const char *cmd, ...);

    /**
     * @ingroup api_core_send_command
     *
     * @brief Send command to WiConnect WiFi module
     *
     * @note Refer to @ref send_command_desc for more info
     * @note This method supports variable arguments
     *
     * @param[in] completeCallback Callback when command completes. arg1 of callback contains responseBuffer pointer, arg2 contains the response length
     * @param[in] responseBuffer Buffer to hold command response
     * @param[in] responseBufferLen Length of responseBuffer
     * @param[in] cmd WiConnect command to send to module
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult sendCommand(const Callback &completeCallback, char *responseBuffer, int responseBufferLen, const char *cmd, ...);

    /**
     * @ingroup api_core_send_command
     *
     * @brief Send command to WiConnect WiFi module
     *
     * @note Refer to @ref send_command_desc for more info
     * @note This method supports variable arguments
     *
     * @param[in] responseBuffer Buffer to hold command response
     * @param[in] responseBufferLen Length of responseBuffer
     * @param[in] cmd WiConnect command to send to module
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult sendCommand(char *responseBuffer, int responseBufferLen, const char *cmd, ...);

    /**
     * @ingroup api_core_send_command
     *
     * @brief Send command to WiConnect WiFi module
     *
     * - This method uses the library internal buffer and
     * - default timeout. See setCommandDefaultTimeout()
     *
     * @note Refer to @ref send_command_desc for more info
     * @note This method supports variable arguments
     *
     * @param[in] completeCallback Callback when command completes. arg1 of callback contains responseBuffer pointer, arg2 contains the response length
     * @param[in] cmd WiConnect command to send to module
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult sendCommand(const Callback &completeCallback, const char *cmd, ...);

    /**
     * @ingroup api_core_send_command
     *
     * @brief Send command to WiConnect WiFi module
     *
     * - This method uses the library internal buffer and
     * - default timeout. See setCommandDefaultTimeout()
     *
     * @note Refer to @ref send_command_desc for more info
     * @note This method supports variable arguments
     *
     * @param[in] cmd WiConnect command to send to module
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult sendCommand(const char *cmd, ...);

    /**
     * @ingroup api_core_send_command
     *
     * This method uses the library internal buffer
     *
     * @note Refer to @ref send_command_desc for more info
     * @note This method supports variable arguments
     *
     * @param[in] completeCallback Callback when command completes. arg1 of callback contains responseBuffer pointer, arg2 contains the response length
     * @param[in] timeoutMs Maximum time in milliseconds this command should execute
     * @param[in] cmd WiConnect command to send to module
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult sendCommand(const Callback &completeCallback, TimerTimeout timeoutMs, const char *cmd, ...);

    /**
     * @ingroup api_core_send_command
     *
     * @brief Send command to WiConnect WiFi module
     *
     * This method uses the library internal buffer
     *
     * @note Refer to @ref send_command_desc for more info
     * @note This method supports variable arguments
     *
     * @param[in] timeoutMs Maximum time in milliseconds this command should execute
     * @param[in] cmd WiConnect command to send to module
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult sendCommand(TimerTimeout timeoutMs, const char *cmd, ...);

    /**
     * @ingroup api_core_send_command
     *
     * - This method uses the library internal buffer and
     * - default timeout. See setCommandDefaultTimeout()
     *
     * @note Refer to @ref send_command_desc for more info
     *
     * @param[in] cmd WiConnect command to send to module
     * @param[in] vaList Varaible list of arguments
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult sendCommand(const char *cmd, va_list vaList);

    /**
     * @ingroup api_core_send_command
     *
     * @brief Check the status of the currently executing command.
     *
     * Refer to @ref WiconnectResult for more information about the return code.
     *
     * @note This command is only applicable for non-blocking mode. Refer to @ref setting_blocking_modes.
     *
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult checkCurrentCommand();

    /**
     * @ingroup api_core_send_command
     *
     * @brief Stop the currently executing command.
     *
     * @note This command is only applicable for non-blocking mode. Refer to @ref setting_blocking_modes.
     */
    void stopCurrentCommand();


    // ------------------------------------------------------------------------


    /**
     * @ingroup api_core_misc
     *
     * @brief When the WiConnect WiFi module returns a response, it contains a
     * response code in the header. This function converts the previous response code
     * to a readable string.
     *
     * @return string representation of module response code
     */
    const char* getLastCommandResponseCodeStr();

    /**
     * @ingroup api_core_misc
     *
     * @brief Return the length in bytes of the previous response.
     *
     * @return length of previous response
     */
    uint16_t getLastCommandResponseLength();

    /**
     * @ingroup api_core_misc
     *
     * @brief Return pointer to internal response buffer.
     *
     * @return pointer to internal response buffer
     */
    char* getResponseBuffer();

    /**
     * @ingroup api_core_misc
     *
     * @brief Helper method to convert previous response to uint32
     *
     * @note This uses the internal response buffer.
     *
     * @param[out] uint32Ptr Pointer to hold result of conversion.
     * @return Result of conversion. See @ref WiconnectResult
     */
    WiconnectResult responseToUint32(uint32_t *uint32Ptr);

    /**
     * @ingroup api_core_misc
     *
     * @brief Helper method to convert previous response to int32
     *
     * @note This uses the internal response buffer.
     *
     * @param[out] int32Ptr Pointer to hold result of conversion.
     * @return Result of conversion. See @ref WiconnectResult
     */
    WiconnectResult responseToInt32(int32_t *int32Ptr);


    // ------------------------------------------------------------------------

    /**
     * @ingroup api_core_settings
     *
     * @brief Set a module setting
     *
     * Refer to: http://wiconnect.ack.me/2.0/variables
     * for a list of the available settings and descriptions
     *
     * @param settingStr String module setting name.
     * @param value The integer value to set
     *
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult setSetting(const char *settingStr, uint32_t value);

    /**
     * @ingroup api_core_settings
     *
     * @brief Set a module setting
     *
     * Refer to: http://wiconnect.ack.me/2.0/variables
     * for a list of the available settings and descriptions
     *
     * @param settingStr String module setting name.
     * @param value The string value to set
     *
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult setSetting(const char *settingStr, const char *value);

    /**
     * @ingroup api_core_settings
     *
     * @brief Get a module setting
     *
     * Refer to: http://wiconnect.ack.me/2.0/variables
     * for a list of the available settings and descriptions
     *
     * @param settingStr String module setting name.
     * @param valuePtr Pointer to buffer to contain integer value
     *
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult getSetting(const char *settingStr, uint32_t *valuePtr);

    /**
     * @ingroup api_core_settings
     *
     * @brief Get a module setting
     *
     * Refer to: http://wiconnect.ack.me/2.0/variables
     * for a list of the available settings and descriptions
     *
     * @param settingStr String module setting name.
     * @param valuePtr Pointer to hold pointer to internal API buffer containing retrieved setting result
     *
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult getSetting(const char *settingStr, char **valuePtr);

    /**
     * @ingroup api_core_settings
     *
     * @brief Get a module setting
     *
     * Refer to: http://wiconnect.ack.me/2.0/variables
     * for a list of the available settings and descriptions
     *
     * @param settingStr String module setting name.
     * @param valueBuffer Buffer to hold retrieved setting result
     * @param valueBufferLen The length of the input buffer
     *
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult getSetting(const char *settingStr, char *valueBuffer, uint16_t valueBufferLen);

    /**
     * @ingroup api_core_settings
     *
     * @brief Save settings to Non-Volatile Memory
     *
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult saveSettings();

    /**
     * @ingroup api_core_settings
     *
     * @brief Sets if API calls are blocking or non-blocking.
     *
     * @param[in] blockingEnabled The new blocking value
     */
    void setBlockingEnabled(bool blockingEnabled);

    /**
     * @ingroup api_core_settings
     *
     * @brief Gets if API calls are blocking or non-blocking.
     */
    bool getBlockingEnabled(void);

    /**
     * @ingroup api_core_settings
     *
     * @brief Sets the default maximum time an API method may execute before
     * terminating and return a timeout error code.
     *
     * @note All API methods (execpt some sendCommand()) use this default value.
     *
     * @param[in] timeoutMs Default command timeout in milliseconds
     */
    void setCommandDefaultTimeout(TimerTimeout timeoutMs);

    /**
     * @ingroup api_core_settings
     *
     * @brief Returns the current default maximum API execution time.
     *
     * @return Default command timeout in milliseconds
     */
    TimerTimeout getCommandDefaultTimeout();

    /**
     * @ingroup api_core_settings
     *
     * @brief Sets a mapping function used to convert from a host Pin to WiConnect WiFi module GPIO.
     *
     * @param[in] mapper Pin to GPIO mapper function pointer
     */
    void setPinToGpioMapper(PinToGpioMapper mapper);

    /**
     * @ingroup api_core_settings
     *
     * @brief Sets callback function used to debug WiConnect WiFi module RX/TX serial data.
     *
     * @param[in] logFunc Logging function pointer
     */
    void setDebugLogger(LogFunc logFunc);

    /**
     * @ingroup api_core_settings
     *
     * @brief Sets callback used when Wiconnect Library hits and internal assertion.
     *
     * @note This is mainly for debugging. There's nothing the callback can do to fix the assertion.
     *
     * @param[in] assertLogFunc Logging function pointer
     */
    void setAssertLogger(LogFunc assertLogFunc);

    // ------------------------------------------------------------------------


#ifdef WICONNECT_ASYNC_TIMER_ENABLED
    /**
     * @ingroup api_core_send_command
     *
     * @brief Add user command to be executed asynchronously.
     *
     * Refer to @ref setting_async_processing for more info.
     *
     * @param[in] command Pointer to QueuedCommand to be executed asynchronously
     * @param[in] commandCompleteHandler Callback to be executed when processing is complete.
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult enqueueCommand(QueuedCommand *command, const Callback &commandCompleteHandler = Callback());

    /**
     * @ingroup api_core_settings
     *
     * @brief Set the period at which an asynchronous command should be processed.
     *
     * Refer to @ref setting_async_processing for more info.
     *
     * @param[in] periodMs Processing period in milliseconds
     */
    void setCommandProcessingPeriod(uint32_t periodMs);
#endif


    /**
     * @ingroup conversion_util
     *
     * @brief Converts a @ref WiconnectResult to string representation.
     *
     * @param[in] wiconnectResult Result code
     * @return String representaion of result code
     */
    static const char* getWiconnectResultStr(WiconnectResult wiconnectResult);

    /**
     * @ingroup conversion_util
     *
     * @brief Converts wiconnect version string to uint32_t representation.
     *
     * @param[in] Output of @ref getVersion() API call.
     * @return 32bit integer WiConnect firmware version
     */
    static uint32_t wiconnectVersionToInt(char *versionStr);

protected:

#ifdef WICONNECT_ENABLE_MALLOC
    void* (*_malloc)(size_t);
    void (*_free)(void *);
    friend class QueuedCommand;
    friend class WiconnectSerial;
    friend class ScanResult;
#endif

    wiconnect::WiconnectSerial serial;
    wiconnect::Gpio resetGpio;
    wiconnect::Gpio wakeGpio;

    volatile bool commandExecuting;
    bool initialized;
    bool needUpdate;
    bool nonBlocking;

    PinToGpioMapper pinToGpioMapper;

    char *internalBuffer;
    int internalBufferSize;
    bool internalBufferAlloc;
    uint8_t internalProcessingState;
    void *currentCommandId;

    wiconnect::TimeoutTimer timeoutTimer;
    TimerTimeout defaultTimeoutMs;

    uint8_t commandHeaderBuffer[32];
    char commandFormatBuffer[WICONNECT_MAX_CMD_SIZE];
    uint8_t commandContext[96];

    void prepare(void *internalBuffer, int internalBufferSize, bool nonBlocking);
    bool configureModuleDataBus(void);
    WiconnectResult inline receiveResponse();
    WiconnectResult inline receivePacket();
    void issueCommandCallback(WiconnectResult result);

#ifdef WICONNECT_ENABLE_DEBUGGING
    LogFunc debugLogger;
#endif
    LogFunc assertLogger;

    void debugLog(const char *msg, ...);

#ifdef WICONNECT_ASYNC_TIMER_ENABLED
    wiconnect::PeriodicTimer commandProcessorTimer;
    uint32_t commandProcessingPeriod;
    CommandQueue commandQueue;
    wiconnect::QueuedCommand *currentQueuedCommand;

    void commandProcessingTimerHandler(void);
    void processNextQueuedCommand();
    void checkQueuedCommandTimeout();
#endif

    friend class NetworkInterface;
    friend class SocketInterface;
    friend class FileInterface;
    friend class GhmInterface;
    friend class GhmMessageList;
    friend class GhmMessage;
    friend class ScanResultList;
    friend class WiconnectFile;
    friend class WiconnectSocket;
    friend class WiconnectUdpServer;
};

}


#include "sdkTypes.h"

