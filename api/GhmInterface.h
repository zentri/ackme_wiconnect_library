/**
 * ACKme WiConnect Host Library is licensed under the BSD licence:
 *
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 */
#pragma once

#include "Wiconnect.h"

#include "api/types/WiconnectSocket.h"
#include "api/types/GhmMessageList.h"

/**
 * @namespace wiconnect
 */
namespace wiconnect {

/**
 * @ingroup api_ghm_macro
 * @brief Helper macro for adding an integer value to a @ref GhmElementArray
 */
#define GHM_ADD_INT(array, name, integerVal)            \
{                                                       \
    array.elementName = name;                           \
    array.type = GHM_VALUE_INT;                         \
    array.u.intValue = (uint32_t)integerVal;            \
}


/**
 * @ingroup api_ghm_macro
 * @brief Helper macro for adding a string value to a @ref GhmElementArray
 */
#define GHM_ADD_STR(array, name, stringVal)             \
{                                                       \
    array.elementName = name;                           \
    array.type = GHM_VALUE_STR;                         \
    array.u.strValue = (const char*)stringVal;          \
}




/**
 * @ingroup api_ghm_types
 *
 * @brief The provides an interface for http://goHACK.me
 *
 * The goHACK.me API provided by WiConnect enables monitoring and control of your ACKme device.
 * You can set up a goHACK.me account, then using goHACK.me commands and goHACK.me variables,
 * you can activate your device and synchronize data, control and messages with the goHACK.me cloud service.
 *
 * More information here: http://wiconnect.ack.me/2.0/gohackme
 *
 * @note This class is an interface to the Wiconnect class. It should never be
 *       independently instantiated or the parent of another class.
 */
class GhmInterface
{
public:

    /**
     * @ingroup api_ghm_activate
     *
     * @brief Download a device capabilities file to WiFi module's internal
     *        file system.
     *
     * @param[in] capsNameOrCustomUrl Optional, specify the caps file name or URL to caps file
     * @param[in] version Optional, version to set downloaded caps file as
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmDownloadCapabilities(const char *capsNameOrCustomUrl = NULL, uint32_t version = 0);

    /**
     * @ingroup api_ghm_activate
     *
     * @brief Activate WiFi module with http://goHACK.me
     *
     * @note You must first sign-up with http://goHACK.me before you can
     *       activate the device. Go to http://goHACK.me to sign-up.
     *
     * @param[in] userName Your username
     * @param[in] password Your password
     * @param[in] capsFilename Optional, filename of existing capabilities file on module's internal file system.
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmActivate(const char *userName, const char *password, const char *capsFilename = NULL);

    /**
     * @ingroup api_ghm_activate
     *
     * @brief Deactivate WiFi module with http://goHACK.me
     *
     * @note You must first sign-up with http://goHACK.me before you can
     *       deactivate the device. Go to http://goHACK.me to sign-up.
     *
     * @param[in] userName Your username
     * @param[in] password Your password
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmDeactivate(const char *userName, const char *password);

    /**
     * @ingroup api_ghm_activate
     *
     * @brief Return if WiFi module is activated with http://goHACK.me
     *
     * @param[out] statusPtr Pointer to hold activation status.
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmIsActivated(bool *statusPtr);


    // ------------------------------------------------------------------------


    /**
     * @ingroup api_ghm_com
     *
     * @brief Read control data from http://goHACK.me
     *
     * @note The WiFi module must first be activated. See @ref ghmActivate()
     *
     * @param[in] controlName The name of the control in the activated capabilities file.
     * @param[in] valueStrPtr Pointer to pointer to hold control's string value
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmRead(const char *controlName, const char **valueStrPtr);

    /**
     * @ingroup api_ghm_com
     *
     * @brief Read control data from http://goHACK.me
     *
     * @note The WiFi module must first be activated. See @ref ghmActivate()
     *
     * @param[in] controlName The name of the control in the activated capabilities file.
     * @param[in] valueIntPtr Pointer hold control's integer value
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmRead(const char *controlName, uint32_t *valueIntPtr);

    /**
     * @ingroup api_ghm_com
     *
     * @brief Read control data from http://goHACK.me
     *
     * @note The WiFi module must first be activated. See @ref ghmActivate()
     *
     * @param[in] controlName The name of the control in the activated capabilities file.
     * @param[in] valueBuffer Buffer to hold control's string value
     * @param[in] valueBufferLen Length of valueBuffer
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmRead(const char *controlName, char *valueBuffer, uint16_t valueBufferLen);

    /**
     * @ingroup api_ghm_com
     *
     * @brief Write stream or control data to http://goHACK.me
     *
     * @note The WiFi module must first be activated. See @ref ghmActivate()
     *
     * @param[in] elementName The name of the control or stream in the activated capabilities file.
     * @param[in] strValue String value of control or stream
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmWrite(const char *elementName, const char *strValue);

    /**
     * @ingroup api_ghm_com
     *
     * @brief Write stream or control data to http://goHACK.me
     *
     * @note The WiFi module must first be activated. See @ref ghmActivate()
     *
     * @param[in] elementName The name of the control or stream in the activated capabilities file.
     * @param[in] uintValue Unsigned integer value of control or stream
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmWrite(const char *elementName, uint32_t uintValue);

    /**
     * @ingroup api_ghm_com
     *
     * @brief Write stream or control data to http://goHACK.me
     *
     * @note The WiFi module must first be activated. See @ref ghmActivate()
     *
     * @param[in] elementName The name of the control or stream in the activated capabilities file.
     * @param[in] intValue signed integer value of control or stream
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmWrite(const char *elementName, int32_t intValue);

    /**
     * @ingroup api_ghm_com
     *
     * @brief Write stream or control data to http://goHACK.me
     *
     * @note The WiFi module must first be activated. See @ref ghmActivate()
     *
     * @param[in] elementArray Array of stream and/or control values to send to http://goHACK.me
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmWrite(const GhmElementArray *elementArray);

    /**
     * @ingroup api_ghm_com
     *
     * @brief Synchronize WiFi module with http://goHACK.me
     *
     * @note The WiFi module must first be activated. See @ref ghmActivate()
     *
     * @param[in] type Type of synchronization. See @ref GhmSyncType
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmSynchronize(GhmSyncType type = GHM_SYNC_ALL);


    // ------------------------------------------------------------------------


    /**
     * @ingroup api_ghm_msg
     *
     * @brief POST message to http://goHACK.me
     *
     * @note The WiFi module must first be activated. See @ref ghmActivate()
     *
     * @param[out] socket @ref WiconnectSocket used to send message data to http://goHACK.me
     * @param[in] jsonFormatted If true, the input message data should be JSON formatted, else the message data should be ASCII formatted.
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmPostMessage(WiconnectSocket &socket, bool jsonFormatted=false);

    /**
     * @ingroup api_ghm_msg
     *
     * @brief GET message from http://goHACK.me
     *
     * @note The WiFi module must first be activated. See @ref ghmActivate()
     *
     * @param[out] socket @ref WiconnectSocket used to receive message data from http://goHACK.me
     * @param[in] getType The additional message data to receive. See @ref GhmMessageGetType
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmGetMessage(WiconnectSocket &socket, GhmMessageGetType getType = GHM_MSG_GET_DATA_ONLY);

    /**
     * @ingroup api_ghm_msg
     *
     * @brief GET message from http://goHACK.me
     *
     * @note @ref ghmListMessages() must be called first before using this method.
     *
     * @note The WiFi module must first be activated. See @ref ghmActivate()
     *
     * @param[out] socket @ref WiconnectSocket used to receive message data from http://goHACK.me
     * @param[in] listIndex The index of the message returned from ghmListMessages()
     * @param[in] getType The additional message data to receive. See @ref GhmMessageGetType
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmGetMessage(WiconnectSocket &socket, uint8_t listIndex, GhmMessageGetType getType = GHM_MSG_GET_DATA_ONLY);

    /**
     * @ingroup api_ghm_msg
     *
     * @brief GET message from http://goHACK.me
     *
     * @note The WiFi module must first be activated. See @ref ghmActivate()
     *
     * @param[out] socket @ref WiconnectSocket used to receive message data from http://goHACK.me
     * @param[in] msgId The http://goHACK.me message ID.
     * @param[in] getType The additional message data to receive. See @ref GhmMessageGetType
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmGetMessage(WiconnectSocket &socket, const char *msgId, GhmMessageGetType getType = GHM_MSG_GET_DATA_ONLY);

    /**
     * @ingroup api_ghm_msg
     *
     * @brief Delete message from http://goHACK.me
     *
     * @note @ref ghmListMessages() must be called first before using this method.
     *
     * @note The WiFi module must first be activated. See @ref ghmActivate()
     *
     * @param[in] listIndex The index of the message returned from ghmListMessages()
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmDeleteMessage(uint8_t listIndex);

    /**
     * @ingroup api_ghm_msg
     *
     * @brief Delete message from http://goHACK.me
     *
     * @note The WiFi module must first be activated. See @ref ghmActivate()
     *
     * @param[in] msgId The http://goHACK.me message ID.
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmDeleteMessage(const char *msgId);

    /**
     * @ingroup api_ghm_msg
     *
     * @brief List available messages for device on http://goHACK.me
     *
     * @note The WiFi module must first be activated. See @ref ghmActivate()
     *
     * @param[out] msgList @ref GhmMessageList to hold received messages.
     * @param[in] maxCount Optional, the maximum number of messages to receive. If 0,
     *            receive all messages (max of 25).
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ghmListMessages(GhmMessageList &msgList, uint8_t maxCount = 0);


protected:
    GhmInterface(Wiconnect *wiconnect);

    WiconnectResult ghmGetMessage(WiconnectSocket &socket, uint8_t listIndex, const char *msgId, GhmMessageGetType getType);
    WiconnectResult processMessageList(char *resultStr, GhmMessageList &resultList);

private:
    Wiconnect *wiconnect;

};


}
