/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once


#include "WiconnectTypes.h"
#include "api/types/ScanResultList.h"


namespace wiconnect {


#ifdef WICONNECT_USE_DEFAULT_STRING_BUFFERS
#define ALLOW_NULL_STRING_BUFFER = NULL
#else
#define ALLOW_NULL_STRING_BUFFER
#endif

/**
 * @ingroup api_network_types
 *
 * @brief The provides an interface for joining and/or creating a network.
 * It provides other utilities such as scanning for networks, pinging a network,
 * resolving a domain name to IP address.
 *
 * @note This class is an interface to the Wiconnect class. It should never be
 *       independently instantiated or the parent of another class.
 */
class NetworkInterface
{
public:
    /**
     * @ingroup api_network_setup
     *
     * @brief Start the WiConnect WiFi module 'web setup' feature.
     *
     * This command has an optional background processing feature.
     * Background processing is enabled if the completeHandler parameter
     * is specified. If enabled, the library will poll the module every second
     * for the web setup status (essentially it'll call isWebSetupRunning() every
     * second in the background). When the web setup is no longer running the
     * callback will be executed. The background processing is disabled when stopWebSetup()
     * is called.
     *
     * @note only the 'result' parameter of the callback handler is valid.
     *
     * Refer to @ref setting_async_processing for more info.
     *
     * @param[in] ssid Optional, optionally set the SSID of module's softAp
     * @param[in] password Optional, optionally set the WPA2-PSK password for the module'S softap
     *                     Note: make an OPEN softAp, set this parameter to a null string (i.e. "")
     * @param[in] completeHandler Optional, callback to be executed when module web setup completes.
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult startWebSetup(const char *ssid = NULL, const char *password = NULL, const Callback &completeHandler = Callback());

    /**
     * @ingroup api_network_setup
     *
     * @brief Stop the WiConnect WiFi module 'web setup' feature.
     *
     * This method should be called AFTER startWebSetup() to prematurely terminate
     * web setup. Note that this is not needed if web setup completes by itself
     * (i.e. if the user exits web setup from the webpage).
     *
     *  @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult stopWebSetup();

    /**
     * @ingroup api_network_setup
     *
     * @brief Return status of WiConnect WiFi module 'web setup' feature.
     *
     * This may be called at any time (whether web setpu has been stared or not).
     *
     * @param[out] isRunningPtr Pointer to bool to contain TRUE if web setup is running, FALSE else
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult isWebSetupRunning(bool *isRunningPtr);


    // ------------------------------------------------------------------------


    /**
     * @ingroup api_network_wlan
     *
     * @brief Join a WiFi network.
     *
     * This command has an optional background processing feature.
     * Background processing is enabled if the completeHandler parameter
     * is specified. If enabled, the library will poll the module every second
     * for the join status (essentially it'll call getNetworkStatus() every
     * second in the background). When the module join sequence complete the callback will be executed.
     * The background processing is disabled when leave() is called.
     *
     * * If completeHandler parameter is NOT specified:
     *   This command will BLOCK/return WICONNECT_PROCESSING until the module has
     *   either successfully joined the network or failed.
     * * If the completeHandler parameter IS specified:
     *   This command will return and use the background processing feature described above.
     *
     * @note only the 'result' parameter of the callback handler is valid.
     *
     * Refer to @ref setting_async_processing for more info.
     *
     * @param[in] ssid Optional, optionally set the SSID of the network to join
     * @param[in] password Optional, optionally set the passkey of the network to join
     *                     Note: to join an OPEN network, set this parameter to a null string (i.e. "")
     * @param[in] completeHandler Optional, callback to be executed when the join sequence completes.
     *                             The 'result' callback parameter contains the WiconnectResult of joining.
     *                             The 'arg1' parameter is a @ref NetworkJoinResult of joining.
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult join(const char* ssid = NULL, const char *password = NULL, const Callback &completeHandler = Callback());

    /**
     * @ingroup api_network_wlan
     *
     * @brief Leave a WiFi network.
     *
     * This method may be called to either terminate a join sequence or
     * leave a previously connected networked.
     *
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult leave();

    /**
     * @ingroup api_network_wlan
     *
     * @brief Get connection status to WiFi network.
     *
     * Refer to @ref NetworkStatus for more info.
     *
     * @param[out] statusPtr Point to a @ref NetworkStatus which will hold current network status of module
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult getNetworkStatus(NetworkStatus *statusPtr);

    /**
     * @ingroup api_network_wlan
     *
     * @brief Get the result of joining the network
     *
     * Refer to @ref NetworkJoinResult for more info.
     *
     * @param[out] joinResultPtr Point to a @ref NetworkJoinResult which will hold the result of joining the network
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult getNetworkJoinResult(NetworkJoinResult *joinResultPtr);

    /**
     * @ingroup api_network_wlan
     *
     * @brief Get @ref NetworkSignalStrength of WiFi network module is connected
     */
    WiconnectResult getSignalStrength(NetworkSignalStrength *signalStrengthPtr);

    /**
     * @ingroup api_network_wlan
     *
     * @brief Get the RSSI in dBm of WiFi network
     */
    WiconnectResult getRssi(int32_t *rssiPtr);

    /**
     * @ingroup api_network_wlan
     *
     * @brief Get MAC address of the WiFi module
     */
    WiconnectResult getMacAddress(MacAddress *macAddress);


    // ------------------------------------------------------------------------


//    WiconnectResult startSoftAp(const char* ssid = NULL, const char *password = NULL, const Callback &clientConnectedCallback = Callback());
//    WiconnectResult stopSoftAp();
//    WiconnectResult getSoftApClientList();


    // ------------------------------------------------------------------------


    /**
     * @ingroup api_network_util
     *
     * @brief Scan for available WiFi networks.
     *
     * The populate the supplied @ref ScanResultList with @ref ScanResult of each found network.
     *
     * Optionally only scan of specific channels by supplying a null terminated list of channels.
     * Example:
     * @code
     * const uint8_t channelsToScan[] = {1, 6, 11, 0};
     * @endcode
     *
     * @param[out] resultList List to populate with scan results.
     * @param[in] channelList Optional, null terminated list of channels to scan.
     * @param[in] ssid Optional, specific network name to scan for.
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult scan(ScanResultList &resultList, const uint8_t *channelList = NULL, const char* ssid = NULL);

    /**
     * @ingroup api_network_util
     *
     * @brief Ping a WiFi network.
     *
     * Optionally ping a specific server and return the time in milliseconds it took
     * for the network to response. If no domain is supplied, the module pings to gateway
     * (i.e router it's connected to).
     *
     * @param[in] domain Optional, the domain name to ping
     * @param[out] timeMsPtr Optional, pointer to uint32 to hold time in milliseconds the ping took
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult ping(const char *domain = NULL, uint32_t *timeMsPtr = NULL);

    /**
     * @ingroup api_network_util
     *
     * @brief Resolve domain name into IP address.
     *
     * @param[in] domain The domain name to resolve
     * @param[out] ipAddressPtr pointer to uint32 to hold resolved IP address. Note, the IP address is in network-byte-order.
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult lookup(const char *domain, uint32_t *ipAddressPtr);


    // ------------------------------------------------------------------------


    /**
     * @ingroup api_network_settings
     *
     * @brief Set DHCP enabled.
     *
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult setDhcpEnabled(bool enabled);

    /**
     * @ingroup api_network_settings
     *
     * @brief Get if DHCP enabled.
     */
    WiconnectResult getDhcpEnabled(bool *enabledPtr);

    /**
     * @ingroup api_network_settings
     *
     * @brief Set static IP settings
     */
    WiconnectResult setIpSettings(uint32_t ip, uint32_t netmask, uint32_t gateway);

    /**
     * @ingroup api_network_settings
     *
     * @brief Set static IP settings (with string parameters)
     */
    WiconnectResult setIpSettings(const char* ip, const char* netmask, const char* gateway);

    /**
     * @ingroup api_network_settings
     *
     * @brief Get network IP settings
     */
    WiconnectResult getIpSettings(uint32_t *ip, uint32_t *netmask, uint32_t *gateway);

    /**
     * @ingroup api_network_settings
     *
     * @brief Set static DNS Address
     */
    WiconnectResult setDnsAddress(uint32_t dnsAddress);

    /**
     * @ingroup api_network_settings
     *
     * @brief Get the static DNS address
     */
    WiconnectResult getDnsAddress(uint32_t *dnsAddress);

    /**
     * @ingroup api_network_settings
     *
     * @note This method is only supported in blocking mode.
     *
     * @brief Return the current IP address of the module if possible, else
     *        return 0.0.0.0
     * @param[in] buffer Optional, buffer to IP address string. If omitted,
     *            the IP address string is stored in a local static buffer (this is non-reentrant!)
     */
    const char* getIpAddress(char *buffer ALLOW_NULL_STRING_BUFFER);


    // ------------------------------------------------------------------------


    /**
     * @ingroup conversion_util
     *
     * @brief Convert string to IP address
     */
    static bool strToIp(const char *str, uint32_t *intPtr);

    /**
     * @ingroup conversion_util
     *
     * @brief Convert IP address to string
     */
    static const char* ipToStr(uint32_t ip, char *ipStrBuffer ALLOW_NULL_STRING_BUFFER);


    /**
     * @ingroup conversion_util
     *
     * @brief Convert @ref NetworkStatus to string
     */
    static const char* networkStatusToStr(NetworkStatus status);

    /**
     * @ingroup conversion_util
     *
     * @brief Convert @ref NetworkJoinResult to string
     */
    static const char* networkJoinResultToStr(NetworkJoinResult joinResult);

    /**
     * @ingroup conversion_util
     *
     * @brief Convert @ref NetworkSignalStrength to string
     */
    static const char* signalStrengthToStr(NetworkSignalStrength signalStrenth);

    /**
     * @ingroup conversion_util
     *
     * @brief Convert RSSI (in dBm) to @ref NetworkSignalStrength
     */
    static NetworkSignalStrength rssiToSignalStrength(int rssi);

    /**
     * @ingroup conversion_util
     *
     * @brief Convert string to @ref NetworkSecurity
     */
    static NetworkSecurity strToNetworkSecurity(const char *str);

    /**
     * @ingroup conversion_util
     *
     * @brief Convert @ref NetworkSecurity to string
     */
    static const char* networkSecurityToStr(NetworkSecurity security);

    /**
     * @ingroup conversion_util
     *
     * @brief Convert string @ref Ssid
     */
    static bool strToSsid(const char *str, Ssid *ssid);

    /**
     * @ingroup conversion_util
     *
     * @brief Convert @ref Ssid to string
     */
    static const char* ssidToStr(const Ssid *ssid, char *ssidStrBuffer ALLOW_NULL_STRING_BUFFER);

    /**
     * @ingroup conversion_util
     *
     * @brief Convert string @ref MacAddress
     */
    static bool strToMacAddress(const char *str, MacAddress *macAddress);

    /**
     * @ingroup conversion_util
     *
     * @brief Convert @ref MacAddress to string
     */
    static const char* macAddressToStr(const MacAddress *macAddress, char *macStrBuffer ALLOW_NULL_STRING_BUFFER);

protected:
    NetworkInterface(Wiconnect *wiconnect);

    WiconnectResult processScanResults(char *resultStr, ScanResultList &resultList);

#ifdef WICONNECT_ASYNC_TIMER_ENABLED
    Callback completeHandler;
    PeriodicTimer monitorTimer;

    void webSetupStatusMonitor();
    void webSetupStatusCheckCallback(WiconnectResult result, void *arg1, void *arg2);

    void joinStatusMonitor();
    void joinStatusCheckCallback(WiconnectResult result, void *arg1, void *arg2);

    //void scanCompleteCallback(WiconnectResult result, void *arg1, void *arg2);
#endif

private:
    Wiconnect *wiconnect;
};

}
