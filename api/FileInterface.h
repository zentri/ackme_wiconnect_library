/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once


#include "WiconnectTypes.h"
#include "types/FileList.h"


/**
 * @ingroup api_file_macro
 * @brief The maximum filename size of a file on the WiConnect WiFi module filesystem
 */
#define FILE_NAME_MAX_SIZE 96

/**
 * @ingroup api_file_macro
 * @def FILE_MAKE_VERSION(major, minor, patch, rc)
 * @brief Combine <\a major>.<\a minor>.<\a patch>.<\a rc> and create version as a uint32_t
 */
#define FILE_MAKE_VERSION(major, minor, patch, rc) ((unsigned int)((major) << 27)|(unsigned int)((minor) << 21)|(unsigned int)((patch) << 8)|(unsigned int)((rc) << 0))
/**
 * @ingroup api_file_macro
 * @def FILE_VERSION_ARGS(version)
 * @brief Given a uint32_t \a version, return arguments for a variable argument function such as printf(). The format string is: %d.%d.%d.%d
 */
#define FILE_VERSION_ARGS(version) (unsigned int)((version >> 27) & 0x1F),(unsigned int)((version >> 21) & 0x3F),(unsigned int)((version >> 8) & 0x1FFF),(unsigned int)(version & 0xFF)


namespace wiconnect {


/**
 * @ingroup api_file_types
 *
 * @brief The provides an interface for creating TCP/UDP/TLS/HTTP client sockets.
 * A client socket connects to a remote server.
 *
 * @note This class is an interface to the Wiconnect class. It should never be
 *       independently instantiated or the parent of another class.
 */
class FileInterface
{
public:
    /**
     * @ingroup api_file_methods
     *
     * @brief Create a file on the Wiconnect WiFi module filesystem.
     *
     * This creates a file on the module's filesystem. The file's name and size are required.
     * Optionally specify the version, type and if it's essential (i.e. if it should never be automatically deleted, careful with
     * this optional as it could cause the the module to not be able to update its firmware).
     *
     * When this method is executed, the file is created on the module then the 'reader' parameter callback is
     * called until all the file data is read from the HOST and written to the module file.
     *
     * @param[in] reader Callback to be executed until all file data has been read from the HOST and written to the module
     * @param[in] user This is supplied to the @ref ReaderFunc callback. It is not used by the library. Leave NULL if not needed.
     * @param[in] name The name of the file to create
     * @param[in] size The size in bytes of the file
     * @param[in] version Optional, the version of the file, defaults to 1.0.0.0
     * @param[in] type Optional, the file type, defaults to FILE_TYPE_MISC_FIX_LEN
     * @param[in] isEssential Optional, specify if the file should never be automatically deleted during a firmware upgrade
     * @param[in] checksum The CRC16 checksum of the file data. The module verifies the written data against this checksum
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult createFile(const ReaderFunc &reader, void *user, const char *name, uint32_t size, uint32_t version = 0, FileType type = FILE_TYPE_ANY, bool isEssential = false, int32_t checksum = -1);

    /**
     * @ingroup api_file_methods
     *
     * @brief Open a file on the Wiconnect WiFi module filesystem for reading.
     *
     * Once opened, the returned @ref WiconnectFile object may only be read.
     *
     * @param[out] file The @ref WiconnectFile object to read data from
     * @param[in] name The name of the file to open
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult openFile(WiconnectFile &file, const char *name);

    /**
     * @ingroup api_file_methods
     *
     * @brief Delete a file for the Wiconnect WiFi module filesystem.
     *
     * @param[in] name The name of the file to delete
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult deleteFile(const char *name);

    /**
     * @ingroup api_file_methods
     *
     * @brief Delete a file for the Wiconnect WiFi module filesystem.
     *
     * @param[in] file The @ref WiconnectFile object of the file to delete
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult deleteFile(const WiconnectFile &file);

    /**
     * @ingroup api_file_methods
     *
     * @brief List the files on the Wiconnect WiFi module filesystem.
     *
     * This lists all the files on the filesystem.
     * Optionally filter by one or more parameters:
     * * name - list files only with given name. If the name started with the wildcard character '*', then
     *          only the characters after it are used for filter.
     *          Example:
     *          @code
     *          wiconnect.listFiles(fileList, "*.txt"); // only list files with '.txt' extension
     *          @endcode
     * * type - only list files with given type
     * * version - only list file with given version
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult listFiles(FileList &list, const char *name = NULL, FileType type = FILE_TYPE_ANY, uint32_t version = 0);


    // ------------------------------------------------------------------------


    /**
     * @ingroup conversion_util
     *
     * @brief Convert file version uint32 to string.
     */
    static const char* fileVersionIntToStr(uint32_t version, bool verbose = true, char *buffer = NULL);

    /**
     * @ingroup conversion_util
     *
     * @brief Convert string to file version uint32.
     */
    static bool fileVersionStrToInt(const char *versionStr, uint32_t *versionIntPtr);

    /**
     * @ingroup conversion_util
     *
     * Convert @ref FileType to string.
     */
    static const char* fileTypeToStr(FileType type);

    /**
     * @ingroup conversion_util
     *
     * @brief Convert @ref FileFlags to string.
     */
    static const char* fileFlagsToStr(FileFlags flags, char *buffer = NULL);

protected:
    FileInterface(Wiconnect *wiconnect);

    WiconnectResult processFileList(char *responseStr, FileList &list, const char *name, FileType type, uint32_t version);
private:
    Wiconnect *wiconnect;
};

}
