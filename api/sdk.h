/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once


#define ARDUINO_SDK


#include "Arduino.h"
#include "HardwareSerial.h"

namespace wiconnect
{

/**
 * @ingroup api_core_macro
 * @brief This is the minimum supported WiConnect version. If your
 *        WiFi module's firmware is out-dated, use the updateFirmware()
 *        API call to update your module's firmware.
 */
#define WICONNECT_MINIMUM_VERSION FILE_MAKE_VERSION(2, 0, 0, 11)

/**
 * @ingroup api_core_macro
 * @brief The default Host<->Wiconnect Module serial BAUD rate
 */
//#define WICONNECT_DEFAULT_BAUD 115200UL
#define WICONNECT_DEFAULT_BAUD 115200UL
/**
 * @ingroup api_core_macro
 * @brief The default command timeout (i.e max command executing time)
 */
#define WICONNECT_DEFAULT_TIMEOUT 15000UL // ms

/**
 * @ingroup api_core_macro
 * @brief The default blocking mode of the Library.
 */
#define WICONNECT_DEFAULT_NONBLOCKING true

/**
 * @ingroup api_core_macro
 * @brief If defined, enables low-level debug messages
 */
#define WICONNECT_ENABLE_DEBUGGING


// ----------------------------------------------------------------------------


#define WICONNECT_SERIAL_BASE_CLASS : public _HardwareSerial
#define WICONNECT_GPIO_BASE_CLASS : public _DigitalOut
#define WICONNECT_PERIODIC_TIMER_BASE_CLASS


/**
 * @ingroup api_core_macro
 * @brief Default value for a pin, Not connected
 */
#define PIN_NC NOT_A_PIN

/**
 * @ingroup api_core_types
 * @brief Pin name on HOST
 */
typedef uint8_t Pin;

/**
 * @ingroup api_core_types
 * @brief Host<->Wiconnect Module serial configuration
 */
typedef struct _SerialConfig
{
    HardwareSerial *serial;
    uint32_t baud;
    Pin rtsPin;
    Pin ctsPin;

    _SerialConfig(HardwareSerial *serial, uint32_t baud = WICONNECT_DEFAULT_BAUD)
    {
        this->serial = serial;
        this->baud = baud;
        this->ctsPin = PIN_NC;
        this->rtsPin = PIN_NC;
    }

    _SerialConfig(HardwareSerial *serial, Pin rtsPin, Pin ctsPin, uint32_t baud = WICONNECT_DEFAULT_BAUD)
    {
        this->serial = serial;
        this->baud = baud;
        this->ctsPin = ctsPin;
        this->rtsPin = rtsPin;
    }

} SerialConfig;


class _DigitalOut
{
protected:
    uint8_t pinNumber;
};

class _HardwareSerial
{
public:
    HardwareSerial *serial;
};

/**
 * @ingroup api_core_macro
 * @brief Function to stop processor for specified number of milliseconds
 */
#define delayMs(ms) delay(ms)

}
