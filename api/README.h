/**
 * @mainpage Library Overview
 *
 * @section Overview
 * The WiConnect Library runs on a host MCU and controls a
 * WiConnect enabled WiFi module. This library is essentially a
 * programming API for the WiConnect serial command set.
 * More infomation about the serial command set may be found here:
 * <a href="http://wiconnect.ack.me" target="_blank">WiConnect Reference Guide</a>
 *
 * @if mbed_sdk
 * The library may be downloaded from here: <a href="http://developer.mbed.org/teams/ACKme/code/WiConnect/" target="_blank">WiConnect Repository</a>
 * @else
 * The library may be downloaded from here: <a href="https://bitbucket.org/ackme/ackme_wiconnect_library/src" target="_blank">WiConnect Repository</a>
 * @endif
 *
 * @section notes Important Notes
 * - This class is implemented as a 'singleton'. This means it only needs to be
 *   instantiated once.
 * - The WiConnect library does NOT call the global 'new' or 'malloc' functions.
 *   All memory allocation (if required) comes from a user supplied malloc function pointer or buffer
 *
 *
 * @section features Library Settings
 * The WiConnect Library has multiple settings so as to allow
 * for the most flexibility within a user's application.
 *
 * Some of these configurations are as follows:
 * - Blocking / Non-blocking
 * - Dynamic / Static allocation
 * - Asynchronous Processing
 *
 *
 * @subsection setting_blocking_modes Blocking / Non-blocking Modes
 * The WiConnect Library may be configured for either 'blocking' or
 * 'non-blocking' operation:
 * - blocking - API calls do not return until they complete successfully or timeout.
 * - non-blocking - API calls return immediately. The caller should check the return code to see
 *                  if the command is still processing, completed, or an error occurred. The caller
 *                  should continue to call the API function (as often as possible) until it returns
 *                  either a success or error code.
 *
 * @subsubsection setting_blocking_mode Blocking Mode
 * In blocking mode, an API function will block until it completes.
 * More details to come...
 *
 * @subsubsection setting_nonblocking_mode Non-Blocking Mode
 * In non-blocking mode, an API function returns immediately.
 * More details to come...
 *
 *
 *
 * @subsection setting_alloc Dynamic / Static Allocation
 * There are two cases when memory allocation is required:
 * - Buffer allocation
 * - Class instantiation
 *
 * In both cases, either static or dynamic memory allocation may be used.
 *
 * In cases when memory allocation is needed, the API call requires a buffer pointer
 * and length parameters. If both are supplied, the library uses the supplied external buffer.
 * This is considered static allocation (however the buffer could have been dynamically allocated).
 * The caller is responsible for maintaining the supplied buffer.
 *
 * If, however, only the buffer length is supplied and the buffer pointer is NULL
 * the Wiconnect Library will call the user supplied malloc() function. This is considered
 * dynamic allocation. In this case, the library will maintain the buffer and release it
 * when necessary using the user supplied free() function.
 *
 * @note To use dynamic allocation the WiConnect Library must be compiled with
 * @ref WICONNECT_ENABLE_MALLOC defined, and the Wiconnect() constructor must be
 * supplied with pointers to malloc() and free() functions.
 *
 * @note The Wiconnect Library does NOT call the global 'new' operator. Classes that are
 *       internally instantiated overload the 'new' operator and either call the user
 *       supplied malloc() function or use the supplied static buffer.
 *
 *
 * @subsection setting_async_processing Asynchronous Processing
 * When applicable, the WiConnect Library will asynchronously process
 * commands in the background. When the background processing completes, the
 * supplied callback is called.
 *
 * User commands may also be executed in the background using the enqueueCommand()
 * API function.
 *
 * @note The WiConnect Library must be compiled with @ref WICONNECT_ASYNC_TIMER_ENABLED
 *       defined for background processing to be enabled.
 *
 *
 *
 * @section send_command_desc Sending Commands To WiFi Module
 * More details to come...
 *
 *
 */

// ----------------------------------------------------------------------------

/**
 * @defgroup api_core Core
 * @brief   This contains all core related operations
 *
 * @{
 */

/**
 * @defgroup api_core_settings Settings Methods
 * @brief    API getters/setters for core library settings
 */

/**
 * @defgroup api_core_send_command Send Command Methods
 * @brief    API methods for sending commands to WiConnect WiFi module
 */

/**
 * @defgroup api_core_misc Miscellaneous Methods
 * @brief    Other core methods
 */

/**
 * @defgroup api_core_types Types
 * @brief    Core Types
 */


/**
 * @defgroup api_core_macro Macros
 * @brief    Core macros
 */

/**
 * @defgroup api_core_examples Examples
 * @brief    Core examples
 * @if mbed_sdk
 * * @ref ota/example.cpp
 * @endif
 */

// @}


// ----------------------------------------------------------------------------

/**
 * @defgroup api_network Network
 * @brief   This contains all network related operations
 *
 * @{
 */

/**
 * @defgroup api_network_settings Settings Methods
 * @brief    API getters/setters for module network settings
 */

/**
 * @defgroup api_network_wlan WLAN Methods
 * @brief    Methods for joining/leaving a WLAN
 */

/**
 * @defgroup api_network_setup Web Setup Methods
 * @brief    Methods for enabled/disabling module web setup
 */

/**
 * @defgroup api_network_util Utility Methods
 * @brief    Network utility methods
 */

/**
 * @defgroup api_network_types Types
 * @brief    Network Types
 */

/**
 * @defgroup api_network_macros Macros
 * @brief    Network specific macros
 */

/**
 * @defgroup api_network_examples Examples
 * @brief Network examples
 * @if mbed_sdk
 * * @ref web_setup/example.cpp
 * * @ref join/example.cpp
 * * @ref tcp_echo_client/main.cpp
 * * @ref udp_echo_client/main.cpp
 * @elseif arduino_sdk
 * * @ref ConnectNoEncryption/ConnectNoEncryption.cpp
 * * @ref ConnectWithWPA/ConnectWithWPA.cpp
 * @endif
 */

// @}


// ----------------------------------------------------------------------------

/**
 * @defgroup api_socket Socket
 * @brief   This contains all socket related operations
 *
 * @{
 */

/**
 * @defgroup api_socket_tcp TCP Methods
 * @brief    TCP methods
 */

/**
 * @defgroup api_socket_udp UDP Methods
 * @brief    UDP methods
 */

/**
 * @defgroup api_socket_tls TLS Methods
 * @brief    TLS methods
 */

/**
 * @defgroup api_socket_http HTTP Methods
 * @brief    HTTP methods
 */

/**
 * @defgroup api_socket_misc Miscellaneous Methods
 * @brief    Miscellaneous socket methods
 */

/**
 * @defgroup api_socket_types Types
 * @brief    Socket Types
 */

/**
 * @defgroup api_socket_macro Macros
 * @brief    Socket specific macros
 */

/**
 * @defgroup api_socket_examples Examples
 * @brief    Socket examples
 * @if mbed_sdk
 * * @ref http_get/example.cpp
 * * @ref tcp_server/example.cpp
 * @elseif arduino_sdk
 * * @ref SimpleWebServerWiFi/SimpleWebServerWiFi.cpp
 * * @ref WiFiChatServer/WiFiChatServer.cpp
 * * @ref WiFiUdpNtpClient/WiFiUdpNtpClient.cpp
 * * @ref WiFiUdpSendReceiveString/WiFiUdpSendReceiveString.cpp
 * * @ref WiFiWebClient/WiFiWebClient.cpp
 * * @ref WiFiWebClientRepeating/WiFiWebClientRepeating.cpp
 * * @ref WiFiWebServer/WiFiWebServer.cpp
 * @endif
 *
 */

// @}


// ----------------------------------------------------------------------------

/**
 * @defgroup api_file File
 * @brief   This contains all file related operations
 *
 * @{
 */

/**
 * @defgroup api_file_methods Methods
 * @brief    File methods
 */

/**
 * @defgroup api_file_types Types
 * @brief    File types
 */

/**
 * @defgroup api_file_macro Macros
 * @brief    API specific macros
 */

/**
 * @defgroup api_file_examples Examples
 * @brief    File examples
 */

// @}

// ----------------------------------------------------------------------------

/**
 * @defgroup api_ghm goHACK.me
 * @brief   This contains all goHACK.me related operations
 *
 * @{
 */

/**
 * @defgroup api_ghm_activate Activation
 * @brief    Methods for activating/deactivating the module with http://goHACK.me
 */

/**
 * @defgroup api_ghm_com Read/Write
 * @brief    Methods for reading & writing data from/to http://goHACK.me
 */

/**
 * @defgroup api_ghm_msg Message
 * @brief    Methods for getting, posting, listing, deleting messages wtih http://goHACK.me
 */

/**
 * @defgroup api_ghm_types Types
 * @brief    goHACK.me Types
 */

/**
 * @defgroup api_ghm_macro Macros
 * @brief    API specific macros
 */

/**
 * @defgroup api_ghm_examples Examples
 * @brief    goHACK.me examples
 * @if mbed_sdk
 * @elseif arduino_sdk
 * * @ref GhmLedMatrix/GhmLedMatrix.cpp
 * @endif
 */

// @}

// ----------------------------------------------------------------------------

/**
 * @defgroup conversion_util Conversion Utilities
 * @brief   This contains all the conversion utility static functions.
 *
 */


