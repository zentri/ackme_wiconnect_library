/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once


#include "Wiconnect.h"

namespace wiconnect
{


/**
 * @ingroup api_file_types
 *
 * @brief WiConnect WiFi module file object.
 *
 */
class WiconnectFile
{
public:
#undef getc
    WiconnectFile(int rxBufferLen = 0, void *rxBuffer = NULL);
    ~WiconnectFile();

    const char* getName() const;
    uint32_t getSize() const;
    FileType getType() const;
    FileFlags getFlags() const;
    uint32_t getVersion() const;
    const char* getVersionStr(char *buffer = NULL) const;

    WiconnectResult close();
    WiconnectResult read(void* buffer, uint16_t maxLength, uint16_t *bytesRead);
    WiconnectResult read(uint8_t **bufferPtr = NULL, uint16_t *bytesReadPtr = NULL);
    WiconnectResult getc(uint8_t *c);
    void clearRxBuffer();

    const WiconnectFile* getNext() const;
    const WiconnectFile* getPrevious() const;

protected:
    WiconnectResult openForRead(uint8_t handle, const char *filename);
    WiconnectResult initWithListing(const char *typeStr, const char *flagsStr, const char* sizeStr, const char *versionStr, const char *nameStr);

    uint8_t handle;
    bool readEnabled;
    char name[WICONNECT_MAX_FILENAME_SIZE];
    uint32_t size;
    FileType type;
    FileFlags flags;
    uint32_t version;
    Wiconnect *wiconnect;
    WiconnectFile *next;
    WiconnectFile *previous;

    Buffer rxBuffer;

    void* operator new(size_t size);
    void operator delete(void*);

    friend class FileInterface;
    friend class FileList;
};

}
