/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once

#include "Wiconnect.h"
#include "PinIrqHandler.h"

namespace wiconnect
{

/**
 * Generic callback function.
 *
 * @note Internal use only.
 */
class SocketIrqHandlerMap
{
public:
    /*************************************************************************************************/
    SocketIrqHandlerMap()
    {
        memset(handlers, 0, sizeof(handlers));
    }

    /*************************************************************************************************/
    ~SocketIrqHandlerMap()
    {
        for(int i = 0; i < WICONNECT_MAX_PIN_IRQ_HANDLERS; ++i)
        {
            if(handlers[i] != NULL)
            {
                handlers[i]->~PinIrqHandler();
            }
        }
    }

    /*************************************************************************************************/
    bool pinIsRegistered(Pin pin)
    {
        for(int i = 0; i < WICONNECT_MAX_PIN_IRQ_HANDLERS; ++i)
        {
            if(handlers[i] != NULL && handlers[i]->irqPin == pin)
            {
                return true;
            }
        }
        return false;
    }

    /*************************************************************************************************/
    WiconnectResult registerHandler(Pin pin, const Callback &callback)
    {
        if(pinIsRegistered(pin))
        {
            return WICONNECT_DUPLICATE;
        }

        PinIrqHandler *handler = NULL;

        for(int i = 0; i < WICONNECT_MAX_PIN_IRQ_HANDLERS; ++i)
        {
            if(handlers[i] == NULL)
            {
                handler = (PinIrqHandler*)&handlerBuffers[i];
                handlers[i] = handler;
            }
        }

        if(handler == NULL)
        {
            return WICONNECT_NOT_FOUND;
        }

        *handler = PinIrqHandler(pin, callback);

        return WICONNECT_SUCCESS;
    }

    /*************************************************************************************************/
    WiconnectResult unregisterHandler(Pin pin)
    {
        for(int i = 0; i < WICONNECT_MAX_PIN_IRQ_HANDLERS; ++i)
        {
            if(handlers[i] != NULL && handlers[i]->irqPin == pin)
            {
                handlers[i]->~PinIrqHandler();
                handlers[i] = NULL;
                return WICONNECT_SUCCESS;
            }
        }

        return WICONNECT_NOT_FOUND;
    }

private:
    PinIrqHandler *handlers[WICONNECT_MAX_PIN_IRQ_HANDLERS];
    PinIrqHandlerBuffer handlerBuffers[WICONNECT_MAX_PIN_IRQ_HANDLERS];
};



}
