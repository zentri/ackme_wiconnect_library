/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once



#include "api/WiconnectTypes.h"
#include "api/StringUtil.h"


namespace wiconnect
{

/**
 * @ingroup api_ghm_types
 *
 * @brief Contains single entry from goHACK.me message listing
 *
 */
class GhmMessage
{
public:
    const char* getMessageId(char *buffer = NULL) const;
    const char* getTimestamp(char *buffer = NULL) const;
    uint16_t    getLength() const;

    const GhmMessage* getNext() const;
    const GhmMessage* getPrevious() const;

protected:
    GhmMessage();

    WiconnectResult init(const char *msgIdStr, const char* timestampStr, const char *lengthStr);
    GhmMessage *next;
    GhmMessage *previous;
    char messageId[37];
    char timestamp[14];
    uint16_t length;

#ifdef WICONNECT_ENABLE_MALLOC
    void* operator new(size_t size);
    void operator delete(void*);
#endif

    friend class GhmMessageList;
};






}
