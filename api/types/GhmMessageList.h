/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once

#include "api/WiconnectTypes.h"
#include "GhmMessage.h"



namespace wiconnect
{


/**
 * @ingroup api_ghm_types
 *
 * @brief List of goHACK.me messages.
 *
 */
class GhmMessageList
{
public:
    GhmMessageList(int bufferLen = 0, void *buffer = NULL);
    ~GhmMessageList();

    void reset(void);
    const GhmMessage* getListHead() const;
    int getCount() const;
    const GhmMessage* getResult(int i) const;
    const GhmMessage* operator [](int i) const;

protected:
    GhmMessage *listHead, *listTail;
    uint8_t *buffer;
    uint8_t *bufferPtr;
    uint16_t bufferLen;
    uint16_t bufferRemaining;
    int count;

    WiconnectResult add(const char *msgId, const char* timestamp, const char *length);


    friend class GhmInterface;
};


}
