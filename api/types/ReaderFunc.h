/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once

#include "api/WiconnectTypes.h"
#include "FunctionPointer.h"


namespace wiconnect
{

typedef WiconnectResult (*_ReaderFunc)(void *user, void *data, int maxReadSize, int *bytesRead);

/**
 * @ingroup api_core_types
 *
 * @brief Generic data reading callback function.
 */
class ReaderFunc : public FunctionPointer
{
public:

    /*************************************************************************************************/
    ReaderFunc(_ReaderFunc func = 0)
    {
        _function = (void*)func;
        _membercaller = NULL;
        _object = NULL;
    }

    /*************************************************************************************************/
    template<typename T>
    ReaderFunc(T *object, WiconnectResult (T::*member)(void *user, void *data, int maxReadSize, int *bytesRead))
    {
        _object = static_cast<void*>(object);
        memcpy(_member, (char*)&member, sizeof(member));
        _membercaller = (void*)&ReaderFunc::membercaller<T>;
        _function = 0;
    }

    /*************************************************************************************************/
    WiconnectResult call(void *user, void *data, int maxReadSize, int *bytesRead)
    {
        if (_function)
        {
            return ((_ReaderFunc)_function)(user, data, maxReadSize, bytesRead);
        }
        else if (_object)
        {
            typedef WiconnectResult (*membercallerFunc)(void*, char*, void *user, void *data, int maxReadSize, int *bytesRead);
            return ((membercallerFunc)_membercaller)(_object, _member, user, data, maxReadSize, bytesRead);
        }
        else
        {
            return WICONNECT_ERROR;
        }
    }

private:

    /*************************************************************************************************/
    template<typename T>
    static WiconnectResult membercaller(void *object, char *member, void *user, void *data, int maxReadSize, int *bytesRead)
    {
        T* o = static_cast<T*>(object);
        WiconnectResult (T::*m)(void *user, void *data, int maxReadSize, int *bytesRead);
        memcpy((char*)&m, member, sizeof(m));
        return (o->*m)(user, data, maxReadSize, bytesRead);
    }
};

}
