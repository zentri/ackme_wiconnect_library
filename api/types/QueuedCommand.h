/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once


#include "api/WiconnectTypes.h"


namespace wiconnect
{
/**
 * @ingroup api_core_types
 *
 * @brief Class which contains command for asynchronous processing.
 */

class QueuedCommand
{
public:
    void *userData;


    QueuedCommand(int responseBufferLen, char *responseBuffer, int timeoutMs, const ReaderFunc &reader, void *user, const char *cmd, va_list vaList);
    QueuedCommand(int responseBufferLen, char *responseBuffer, int timeoutMs, const char *cmd, ...);
    QueuedCommand(int responseBufferLen, char *responseBuffer, const char *cmd, ...);
    QueuedCommand(int timeoutMs_, const char *cmd, ...);
    QueuedCommand(const char *cmd, ...);
    ~QueuedCommand();

    char *getResponseBuffer();
    int getResponseBufferLen();
    int getTimeoutMs();
    ReaderFunc getReader();
    void * getReaderUserData();
    char* getCommand();
    Callback getCompletedCallback();
    void setCompletedCallback(const Callback &cb);

    QueuedCommand& operator=( const QueuedCommand& other );
    void* operator new(size_t size);
    void operator delete(void*);

protected:
    char *responseBuffer;
    int responseBufferLen;
    int timeoutMs;
    ReaderFunc reader;
    void *user;
    char command[WICONNECT_MAX_CMD_SIZE];
    Callback completeCallback;
#ifdef WICONNECT_ENABLE_MALLOC
    bool allocatedBuffer;
#endif
    friend class NetworkInterface;
    friend class Wiconnect;

    void initialize(int responseBufferLen, char *responseBuffer_, int timeoutMs_, const ReaderFunc &reader_, void *user_, const char *cmd_, va_list vaList);
};



}
