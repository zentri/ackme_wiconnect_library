/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once


#include "api/WiconnectTypes.h"

namespace wiconnect
{

/**
 * @ingroup api_socket_types
 *
 * @brief Connection object to remote server.
 *
 */
class WiconnectSocket
{
public:
#undef putc
#undef puts
#undef getc
    WiconnectSocket(int rxBufferLen = 0, void *rxBuffer = NULL, int txBufferLen = 0, void *txBuffer = NULL);
    ~WiconnectSocket();

    WiconnectResult close();
    WiconnectResult poll(bool *rxDataAvailablePtr, bool autoClose = false);
    WiconnectResult write(const void* buffer, int length, bool flush = true);
    WiconnectResult write(int length, bool flush = true);
    WiconnectResult read(void* buffer, uint16_t maxLength, uint16_t *bytesRead);
    WiconnectResult read(uint8_t **bufferPtr = NULL, uint16_t *bytesReadPtr = NULL);
    WiconnectResult putc(uint8_t c, bool flush = false);
    WiconnectResult puts(const char *s, bool flush = true);
    WiconnectResult getc(uint8_t *c);
    WiconnectResult printf(const char* format, ...);
    virtual WiconnectResult flushTxBuffer();
    void clearRxBuffer();

    uint8_t *getTxBuffer();
    int getTxBufferSize();
    int getTxBufferBytesPending();
    uint8_t *getRxBuffer();
    int getRxBufferSize();
    int getRxBufferBytesPending();

    bool isConnected();
    SocketType getType();
    const char* getHost();
    uint16_t getLocalPort();
    uint16_t getRemotePort();
    uint8_t getHandle();

protected:
    bool connected;
    bool enableAutoClose;
    SocketType type;
    uint8_t handle;
    char host[WICONNECT_MAX_HOST_SIZE];
    uint16_t localPort;
    uint16_t remotePort;
    Wiconnect *wiconnect;
    Buffer txBuffer;
    Buffer rxBuffer;

    WiconnectResult init(uint8_t handle, SocketType type, const char *host, uint16_t remotePort, uint16_t localPort);

    friend class SocketInterface;
    friend class GhmInterface;
};


}
