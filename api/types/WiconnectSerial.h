/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once

#include "api/WiconnectTypes.h"


namespace wiconnect
{

/**
 * Serial class for interfacing to WiConnect WiFi module.
 *
 * @note Internal use only.
 */
class WiconnectSerial WICONNECT_SERIAL_BASE_CLASS
{
public:
    WiconnectSerial(const SerialConfig  &config, Wiconnect *wiconnect = NULL);
    virtual ~WiconnectSerial();

    void initialize(void);
    void flush(void);
    int write(const void *data, int bytesToWrite, TimerTimeout timeoutMs);
    int read(void *data, int bytesToRead, TimerTimeout timeoutMs);

    uint32_t getBaud(){ return baudRate; }

protected:
    TimeoutTimer timeoutTimer;
    uint32_t baudRate;

#ifdef WICONNECT_SERIAL_RX_BUFFER
    uint8_t ringBuffer[32];
    bool bufferAlloc;
    void rxIrqHandler(void);
#endif
};


}


