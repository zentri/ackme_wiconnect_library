/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once

#include "api/WiconnectTypes.h"
#include "FunctionPointer.h"

namespace wiconnect
{

typedef int (*_LogFunc)(const char *str);


/**
 * @ingroup api_core_types
 *
 * @brief Logging callback function.
 */
class LogFunc : public FunctionPointer
{
public:
    /*************************************************************************************************/
    LogFunc(_LogFunc func = 0)
    {
        _function = (void*)func;
        _membercaller = NULL;
        _object = NULL;
    }

    /*************************************************************************************************/
    template<typename T>
    LogFunc(T *object, WiconnectResult (T::*member)(const char *str))
    {
        _object = static_cast<void*>(object);
        memcpy(_member, (char*)&member, sizeof(member));
        _membercaller = (void*)&LogFunc::membercaller<T>;
        _function = 0;
    }

    /*************************************************************************************************/
    int call(const char *str)
    {
        if (_function)
        {
            return ((_LogFunc)_function)(str);
        }
        else if (_object)
        {
            typedef int (*membercallerFunc)(void*, char*, const char *str);
            return ((membercallerFunc)_membercaller)(_object, _member, str);
        }
        else
        {
            return -1;
        }
    }

private:

    /*************************************************************************************************/
    template<typename T>
    static int membercaller(void *object, char *member, const char *str)
    {
        T* o = static_cast<T*>(object);
        int (T::*m)(const char *str);
        memcpy((char*)&m, member, sizeof(m));
        return (o->*m)(str);
    }
};




}
