/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once


#include "api/WiconnectTypes.h"
#include "FunctionPointer.h"



namespace wiconnect
{


typedef void (*_Callback)(WiconnectResult result, void *arg1, void *arg2);


/**
 * @ingroup api_core_types
 *
 * @brief Generic callback function.
 */
class Callback : public FunctionPointer
{
public:
    /*************************************************************************************************/
    Callback(_Callback func = 0)
    {
        _function = (void*)func;
        _membercaller = NULL;
        _object = NULL;
    }

    /*************************************************************************************************/
    template<typename T>
    Callback(T *object, void (T::*member)(WiconnectResult result, void *arg1, void *arg2))
    {
        _object = static_cast<void*>(object);
        memcpy(_member, (char*)&member, sizeof(member));
        _membercaller = (void*)&Callback::membercaller<T>;
        _function = 0;
    }

    /*************************************************************************************************/
    void call(WiconnectResult result, void *arg1, void *arg2)
    {
        if (_function)
        {
            ((_Callback)_function)(result, arg1, arg2);
        }
        else if (_object)
        {
            typedef void (*membercallerFunc)(void*, char*, WiconnectResult result, void *arg1, void *arg2);
            ((membercallerFunc)_membercaller)(_object, _member, result, arg1, arg2);
        }
    }

private:

    /*************************************************************************************************/
    template<typename T>
    static void membercaller(void *object, char *member, WiconnectResult result, void *arg1, void *arg2)
    {
        T* o = static_cast<T*>(object);
        void (T::*m)(WiconnectResult result, void *arg1, void *arg2);
        memcpy((char*)&m, member, sizeof(m));
        (o->*m)(result, arg1, arg2);
    }

};


}
