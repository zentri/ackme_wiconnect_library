#ifndef wifiserver_h
#define wifiserver_h


#include "Server.h"


class WiFiClient;

class WiFiServer : public Server {
private:
    uint16_t _listenPort;
    bool _serverActive;
    WiFiClient *_client;

public:
  WiFiServer(uint16_t);
  int available(WiFiClient &client);
  void begin();
  virtual size_t write(uint8_t);
  virtual size_t write(const uint8_t *buf, size_t size);
  uint8_t status();
  uint16_t getListeningPort() { return _listenPort; }
  using Print::write;
};

#endif
