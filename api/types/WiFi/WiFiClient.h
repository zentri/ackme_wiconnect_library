#ifndef wificlient_h
#define wificlient_h
#include "Arduino.h"	
#include "Print.h"
#include "Client.h"
#include "IPAddress.h"

#include "api/types/WiconnectSocket.h"


#define CLIENT_TX_PACKET_MAX_SIZE 64
#define CLIENT_RX_PACKET_MAX_SIZE 64


class WiFiClient : public Client {

public:
  WiFiClient();
  //WiFiClient(uint8_t sock);

  uint8_t status();
  virtual int connect(IPAddress ip, uint16_t port);
  virtual int connect(const char *host, uint16_t port);
  virtual size_t write(uint8_t);
  virtual size_t write(const uint8_t *buf, size_t size);
  int available(uint8_t autoClose);
  virtual int available();
  virtual int read();
  virtual int read(uint8_t *buf, size_t size);
  virtual int peek();
  virtual void flush();
  virtual void stop();
  virtual uint8_t connected();
  virtual operator bool();

  friend class WiFiServer;

  using Print::write;

protected:
  uint8_t txBuffer[CLIENT_TX_PACKET_MAX_SIZE];
  uint8_t rxBuffer[CLIENT_RX_PACKET_MAX_SIZE];
  wiconnect::WiconnectSocket _socket;

};

#endif
