/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once

#include "Wiconnect.h"
#include "types/WiconnectSocket.h"
#include "types/WiconnectUdpServer.h"

#ifdef WICONNECT_GPIO_IRQ_ENABLED
#include "types/SocketIrqHandlerMap.h"
#endif

/**
 * @namespace wiconnect
 */
namespace wiconnect {

#ifdef WICONNECT_GPIO_IRQ_ENABLED
#define GPIO_IRQ_ARG_NC ,Pin irqPin = PIN_NC
#define GPIO_IRQ_ARG ,Pin irqPin
#define GPIO_IRQ_PARAM ,irqPin
#else
#define GPIO_IRQ_ARG_NC
#define GPIO_IRQ_ARG
#define GPIO_IRQ_PARAM
#endif

/**
 * @ingroup api_socket_types
 *
 * @brief The provides an interface for creating TCP/UDP/TLS/HTTP client sockets.
 * A client socket connects to a remote server.
 *
 * @note This class is an interface to the Wiconnect class. It should never be
 *       independently instantiated or the parent of another class.
 */
class SocketInterface
{
public:
    /**
     * @ingroup api_socket_misc
     *
     * @brief Close all opened sockets.
     *
     * @note This closes all open sockets on the MODULE side.
     *       Socket objects on the HOST side will be still open until
     *       issuing a read/write command to the module using the socket handle.
     *
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult closeAllSockets();

#ifdef WICONNECT_GPIO_IRQ_ENABLED
    /**
     * @ingroup api_socket_misc
     *
     * @brief Register a host pin as an external interrupt. When the external interrupt is
     * triggered, the supplied callback is executed.
     *
     * @note WICONNECT_GPIO_IRQ_ENABLED must be defined to use this feature
     *
     * This should be called before calling one of the connect methods below
     * with an irqPin parameter.
     *
     * Basically how this works is:
     * 1. The supplied irqPin is configured as an external interrupt pin.
     * 2. A connection is opened and configured with the same irqPin. This
     *   irqPin physically connected to a GPIO on the WiFi module.
     * 3. When the WiFi module has data to send to the HOST it asserts the irqPin.
     * 4. The irqPin interrupt executes and calls the supplied handler.
     * 5. The handler should notify the HOST that the given irqPin has triggered
     *    and have the associated socket read data from the module.
     *
     *  @note arg1 of the handler contains the irqPin
     *
     *
     * @param[in] irqPin The HOST pin to configure as an external interrupt.
     *                   This pin should be physically connected to a module GPIO.
     * @param[in] handler Callback to be executed with the external irqPin interrupt triggers
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult registerSocketIrqHandler(Pin irqPin, const Callback &handler);

    /**
     * @ingroup api_socket_misc
     *
     * @brief Unregister a previously registered IRQ pin.
     *
     * This disables the given irqPin as an external interrupt.
     * Refer to registerSocketIrqHandler() for more information.
     *
     * @param[in] irqPin The HOST pin to unregister
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult unregisterSocketIrqHandler(Pin irqPin);
#endif

    /**
     * @ingroup api_socket_misc
     *
     * @brief Connect to remote server.
     *
     * This is the base method used by all the other connect methods.
     *
     * @param[out] socket @ref WiconnectSocket object of opened connection.
     * @param[in] type The @ref SocketType of connection to open
     * @param[in] host The host/IP address of the remote server
     * @param[in] remortPort The port of the remote server
     * @param[in] localPort The port of the module's side of the connection
     * @param[in] args Depedent on the connection type
     * @param[in] irqPin Data available external interrupt pin. See registerSocketIrqHandler() for more info
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult connect(WiconnectSocket &socket, SocketType type, const char *host, uint16_t remortPort, uint16_t localPort, const void *args GPIO_IRQ_ARG);


    // ------------------------------------------------------------------------

    /**
     * @ingroup api_socket_tcp
     *
     * @brief Connect to remote TCP server.
     *
     * @param[out] socket TCP @ref WiconnectSocket object of opened connection.
     * @param[in] host The host/IP address of the remote TCP server
     * @param[in] remortPort The port of the remote server
     * @param[in] irqPin Optional, Data available external interrupt pin. See registerSocketIrqHandler() for more info
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult tcpConnect(WiconnectSocket &socket, const char *host, uint16_t remortPort GPIO_IRQ_ARG_NC);


    // ------------------------------------------------------------------------

    /**
     * @ingroup api_socket_tcp
     *
     * @brief Start internal TCP server and listen on specified port.
     *
     * @param[in] listeningPort The local port the server should listen on
     * @param[in] maxClients Optional, the maximum simultaneous connected clients, 0 is default, 1-8 valid range
     * @param[in] irqPin Optional, Data available external interrupt pin. See registerSocketIrqHandler() for more info
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult tcpListen(uint16_t listeningPort, int maxClients = 0 GPIO_IRQ_ARG_NC);


    // ------------------------------------------------------------------------

    /**
     * @ingroup api_socket_tcp
     *
     * @brief Wait for next client to connect to TCP server.
     *
     * @param[in] socket Socket to connected client
     * @param[in] timeoutMs Optional, specifiy maximum amount of time in ms to wait for a client
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult tcpAccept(WiconnectSocket &socket, uint32_t timeoutMs = WICONNECT_WAIT_FOREVER);


    // ------------------------------------------------------------------------

    /**
     * @ingroup api_socket_tcp
     *
     * @brief Stop TCP server from listening on port. Close all connected clients.
     *
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult tcpServerStop(void);


    // ------------------------------------------------------------------------

    /**
     * @ingroup api_socket_tls
     *
     * @brief Connect to remote TLS server.
     *
     * @param[out] socket TLS @ref WiconnectSocket object of opened connection.
     * @param[in] host The host/IP address of the remote TLS server
     * @param[in] remortPort The port of the remote server
     * @param[in] certFilename Optional, filename of certificate on module's file system
     * @param[in] irqPin Optional, Data available external interrupt pin. See registerSocketIrqHandler() for more info
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult tlsConnect(WiconnectSocket &socket, const char *host, uint16_t remortPort, const char *certFilename = NULL GPIO_IRQ_ARG_NC);


    // ------------------------------------------------------------------------

    /**
     * @ingroup api_socket_udp
     *
     * @brief Connect to remote UDP server.
     *
     * @param[out] socket UDP @ref WiconnectSocket object of opened connection.
     * @param[in] host The host/IP address of the remote UDP server
     * @param[in] remortPort The port of the remote server
     * @param[in] localPort Optional, port of module's side of the connection
     * @param[in] irqPin Optional, Data available external interrupt pin. See registerSocketIrqHandler() for more info
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult udpConnect(WiconnectSocket &socket, const char *host, uint16_t remortPort, uint16_t localPort = SOCKET_ANY_PORT GPIO_IRQ_ARG_NC);

    /**
     * @ingroup api_socket_udp
     *
     * @brief Start a UDP server listening on the given port
     *
     * @param[out] udpServer UDP @ref WiconnectUdpServer object listening server
     * @param[in] listeningPort The port the UDP server listens on
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult udpListen(WiconnectUdpServer &udpServer, uint16_t listeningPort);


    // ------------------------------------------------------------------------

    /**
     * @ingroup api_socket_http
     *
     * @brief Connect to remote HTTP server.
     *
     * This is the base method for the other HTTP methods.
     *
     * @section secure_http_connection Secure HTTP
     * Each HTTP method is able to connect to a secure HTTP server. To do this,
     * the URL string  parameter must start with 'https://'
     * To connect to a secure HTTP server a TLS certificate is needed. The certificate
     * is specified in the certFilename parameter of the method (or @ref HttpSocketArgs parameter).
     * This is the filename of an existing certificate on the module file system.
     *
     * @note If the URL starts with 'https://' and no certificate filename is specified,
     *       the module's default certificate is used.
     *
     * @param[out] socket HTTP @ref WiconnectSocket object of opened connection.
     * @param[in] url URL of HTTP request
     * @param[in] args Configuration @ref HttpSocketArgs for HTTP connection
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult httpConnect(WiconnectSocket &socket, const char *url, const HttpSocketArgs *args);

    /**
     * @ingroup api_socket_http
     *
     * @brief Issue HTTP GET Request
     *
     * This method has the open to only 'open' the connection (disabled by default). This means a connection
     * to the remote HTTP server is opened, but the HTTP request isn't issued. This
     * allow for addition data to be added to the request. For instance, use httpAddHeader() to add
     * additional headers to the request.
     * Use httpGetStatus() to issue the HTTP request and receive the HTTP response.
     *
     * @param[out] socket HTTP @ref WiconnectSocket object of opened connection.
     * @param[in] url URL of HTTP GET request
     * @param[in] openOnly Optional, if TRUE this will only open a connection to the server (it won't issue the request)
     * @param[in] certFilename Optional, filename of existing TLS certificate on module's file system. See @ref secure_http_connection
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult httpGet(WiconnectSocket &socket, const char *url, bool openOnly = false, const char *certFilename = NULL);

    /**
     * @ingroup api_socket_http
     *
     * @brief Issue HTTP POST Request
     *
     * This method has the open to only 'open' the connection which enabled by default. This means a connection
     * to the remote HTTP server is opened, but the HTTP request isn't issued. This
     * allow for addition data to be added to the request. Use the returned @ref WiconnectSocket object's 'write' methods
     * to add POST data to the request.
     * When all POST data has been written, use httpGetStatus() to issue the HTTP request and receive the HTTP response.
     *
     * @param[out] socket HTTP @ref WiconnectSocket object of opened connection.
     * @param[in] url URL of HTTP POST request
     * @param[in] contextType The value to go into the 'content-type' HTTP header (e.g. 'application/json')
     * @param[in] openOnly Optional, if FALSE this will immediately issue the POST request.
     * @param[in] certFilename Optional, filename of existing TLS certificate on module's file system. See @ref secure_http_connection
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult httpPost(WiconnectSocket &socket, const char *url, const char *contextType, bool openOnly = true, const char *certFilename = NULL);

    /**
     * @ingroup api_socket_http
     *
     * @brief Issue HTTP HEAD Request
     *
     * @param[out] socket HTTP @ref WiconnectSocket object of opened connection.
     * @param[in] url URL of HTTP HEAD request
     * @param[in] certFilename Optional, filename of existing TLS certificate on module's file system. See @ref secure_http_connection
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult httpHead(WiconnectSocket &socket, const char *url, const char *certFilename = NULL);

    /**
     * @ingroup api_socket_http
     *
     * @brief Add HTTP header key/value pair to opened HTTP request.
     *
     * To use this function, the supplied @ref WiconnectSocket parameter must have been created
     * using either httpGet() or httpPost() and the 'openOnly' parameter TRUE.
     *
     * This will add additional header to the HTTP request.
     *
     * Use httpGetStatus() to issue the request.
     *
     * @param[in] socket Opened socket to add additonal HTTP header
     * @param[in] key Header key (e.g. 'content-type')
     * @param[in] value Header value (e.g. 'application/json')
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult httpAddHeader(WiconnectSocket &socket, const char *key, const char *value);

    /**
     * @ingroup api_socket_http
     *
     * @brief Get the HTTP status code from HTTP request.
     *
     * This may be used to either issue an HTTP request of an opened HTTP connection
     * or return the status code of a request already issued.
     *
     * @param[in] socket Opened socket to get http response status code
     * @param[out] statusCodePtr Pointer to uint32 to hold http status code
     * @return Result of method. See @ref WiconnectResult
     */
    WiconnectResult httpGetStatus(WiconnectSocket &socket, uint32_t *statusCodePtr);

protected:
    SocketInterface(Wiconnect *wiconnect);
    ~SocketInterface();

#ifdef WICONNECT_GPIO_IRQ_ENABLED
    SocketIrqHandlerMap irqHandlers;
#endif

    uint32_t serverConnectedClientList;

    WiconnectResult pollForServerClient(uint8_t *handle = NULL, uint16_t *localPort = NULL, uint16_t *remotePort = NULL, uint32_t *ipAddress = NULL);

    void socketClosedCallback(const WiconnectSocket *socket);

    friend class GhmInterface;

private:
    Wiconnect *wiconnect;
};

}
