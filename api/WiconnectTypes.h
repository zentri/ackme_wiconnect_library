/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once

#include <stdint.h>
#include <stdarg.h>
#include <stddef.h>

#include "sdk.h"

/**
 * @namespace wiconnect
 *
 * @brief The entire Wiconnect Library is contained within the 'wiconnect' namespace
 */
namespace wiconnect {


#ifndef MIN
/**
 * @ingroup api_core_macro
 * @def MIN(x,y)
 * @brief Computes the minimum of \a x and \a y.
 */
#define MIN(x,y) ((x) < (y) ? (x) : (y))
#endif
#ifndef MAX
/**
 * @ingroup api_core_macro
 * @def MAX(x,y)
 * @brief Computes the maximum of \a x and \a y.
 */
#define MAX(x,y)  ((x) > (y) ? (x) : (y))
#endif
/**
 * @ingroup api_core_macro
 * @def ALIGN_n(x, n)
 * @brief Align \a x to \a n bytes (\a n must be power of 2)
 */
#define ALIGN_n(x, n) ((((uint32_t)x) + ((n)-1)) & ~((n)-1))
/**
 * @ingroup api_core_macro
 * @def ALIGN_8(x)
 * @brief Align \a x to 8 bytes
 */
#define ALIGN_8(x) ALIGN_n(x, 8)
/**
 * @ingroup api_core_macro
 * @def ALIGN_4(x)
 * @brief Align \a x to 4 bytes
 */
#define ALIGN_4(x) ALIGN_n(x, 4)
/**
 * @ingroup api_core_macro
 * @def ARRAY_COUNT(x)
 * @brief Return number of elements in static array \a x
 */
#define ARRAY_COUNT(x) (sizeof (x) / sizeof *(x))



/**
 * @ingroup api_core_macro
 * @def WICONNECT_FAILED(result, func)
 * @brief Populates \a result with return value from \a func, returns TRUE if return value contains error.
 */
#define WICONNECT_FAILED(result, func) ((int)(result = (func)) < (int)wiconnect::WICONNECT_SUCCESS)

/**
 * @ingroup api_core_macro
 * @def WICONNECT_SUCCEEDED(result, func)
 * @brief Populates \a result with return value from \a func, returns TRUE if return value is WICONNECT_SUCCESS.
 */
#define WICONNECT_SUCCEEDED(result, func) ((result = (func)) == wiconnect::WICONNECT_SUCCESS)

/**
 * @ingroup api_core_macro
 * @def WICONNECT_IS_PROCESSING(result, func)
 * @brief Populates \a result with return value from \a func, returns TRUE if return value is WICONNECT_PROCESSING.
 */
#define WICONNECT_IS_PROCESSING(result, func) ((result = (func)) == wiconnect::WICONNECT_PROCESSING)

/**
 * @ingroup api_core_macro
 * @brief The maximum command size that may be sent to the WiConnect WiFi module
 */
#define WICONNECT_MAX_CMD_SIZE 128
/**
 * @ingroup api_core_macro
 * @brief The maximum WiConnect WiFi module version string size
 */
#define WICONNECT_MAX_VERSION_SIZE 96
/**
 * @ingroup api_core_macro
 * @brief The maximum number of simulanteous opened sockets
 */
#define WICONNECT_MAX_SOCKETS 8
/**
 * @ingroup api_core_macro
 * @brief The maximum server string length
 */
#define WICONNECT_MAX_HOST_SIZE 64
/**
 * @ingroup api_core_macro
 * @brief The maximum Wiconnect Module flash filesystem filename length
 */
#define WICONNECT_MAX_FILENAME_SIZE 96
/**
 * @ingroup api_core_macro
 * @brief Never timeout
 */
#define WICONNECT_WAIT_FOREVER 0xFFFFFFFFUL
/**
 * @ingroup api_core_macro
 * @brief Timeout immediately
 */
#define WICONNECT_NO_WAIT 0
/**
 * @ingroup api_core_macro
 * @brief Default firmware update timeout in ms
 */
#define WICONNECT_FIRMWARE_UPDATE_TIMEOUT 90000UL

/**
 * @ingroup api_socket_macro
 * @brief Default which indicates to use the most optimal port
 */
#define SOCKET_ANY_PORT (uint16_t)0
/**
 * @ingroup api_socket_macro
 * @brief Default which indicates to use the most optimal port
 */
#define SOCKET_INVALID_HANDLE ((uint8_t)0xFF)


/**
 * @ingroup api_core_types
 * @brief API Result code
 */
typedef enum
{
    // Status Codes
    WICONNECT_ABORTED               = 3,    ///< Command was aborted
    WICONNECT_IDLE                  = 2,    ///< Library not processing any commands
    WICONNECT_PROCESSING            = 1,    ///< Library processing current command
    WICONNECT_SUCCESS               = 0,    ///< Command successfully completed

    // Error codes
    WICONNECT_ERROR                 = -1,   ///< Generic error
    WICONNECT_CMD_RESPONSE_ERROR    = -2,   ///< Module returned error code
    WICONNECT_NULL_BUFFER           = -3,   ///< Null buffer supplied
    WICONNECT_NOT_INITIALIZED       = -4,   ///< Library not initialed
    WICONNECT_OVERFLOW              = -5,   ///< Buffer overflowed
    WICONNECT_TIMEOUT               = -6,   ///< Command timed out
//    WICONNECT_RESPONSE_HANDLER_NULL = -7,   ///<
    WICONNECT_RESPONSE_PARSE_ERROR  = -8,   ///< Failed to parse module response
    WICONNECT_ANOTHER_CMD_EXECUTING = -9,   ///< Currently executing another command
    WICONNECT_BAD_ARG               = -10,  ///< Bad argument supplied
    WICONNECT_UNSUPPORTED           = -11,  ///< Command / parameter not supported
    WICONNECT_PINNAME_TO_GPIO_MAPPER_NULL = -12,    ///< The pinToGpioMapper hasn't been set
    WICONNECT_DUPLICATE             = -13,  ///< Duplicate value
    WICONNECT_NOT_FOUND             = -14,  ///< Not found
    WICONNECT_PINNAME_TO_GPIO_NO_MAPPING = -15, ///< No mapping found for given pin
    WICONNECT_NOT_CONNECTED         = -16,  ///< Not connected
    WICONNECT_UNDERFLOW             = -17,  ///< Data underflow
    WICONNECT_MONITOR_NOT_AVAILABLE = -18,  ///< Background processing monitor is not available (i.e in use)
    WICONNECT_NOT_OPENED_FOR_READING = -19, ///< The file is not open for reading
    WICONNECT_FIRMWARE_OUTDATED     = -20,  ///< The WiFi module's firmware is out-dated. See updateFirmware() to update the firmware.
} WiconnectResult;



/**
 * @ingroup types_core
 * @brief Function pointer for mapping from a host pin to a WiConnect Module GPIO.
 *
 * @param[in] pin A host pin
 * @return The corresponding WiConnect Module GPIO (which the given pin is physically connected).
 *         Return -1 if no mapping is available.
 */
typedef int8_t (*PinToGpioMapper)(Pin pin);

/**
 * @ingroup types_core
 * @brief Timeout type
 */
typedef unsigned long TimerTimeout;


/**
 * @brief Generic buffer type
 *
 * @note Internal use only
 */
typedef struct
{
    int size;
    uint8_t *buffer;
    uint8_t *ptr;
    int bytesPending;
    bool allocated;
} Buffer;


// ----------------------------------------------------------------------------



/**
 * @ingroup api_network_types
 * @brief Network connection status
 */
typedef enum
{
    NETWORK_STATUS_DOWN,            ///< Not connected to network
    NETWORK_STATUS_WIFI_ONLY,       ///< Connected to network but don't have IP address
    NETWORK_STATUS_UP,              ///< Conntected to network and have IP address
    NETWORK_STATUS_JOINING          ///< Joining a network
} NetworkStatus;

/**
 * @ingroup api_network_types
 * @brief Network connection status
 */
typedef enum
{
    NETWORK_JOIN_RESULT_NONE,           ///< Haven't attempted to join
    NETWORK_JOIN_RESULT_SUCCESS,        ///< Successfully joined the network
    NETWORK_JOIN_RESULT_JOINING,        ///< Currently attempting to join
    NETWORK_JOIN_RESULT_NO_SSID,        ///< The SSID has not be configured
    NETWORK_JOIN_RESULT_NO_PASSWORD,    ///< The network requires a password and none has been set
    NETWORK_JOIN_RESULT_BAD_SECURITY,   ///< The specified security type is not supported by the network
    NETWORK_JOIN_RESULT_NOT_FOUND,      ///< The network with the configured SSID was not found
    NETWORK_JOIN_RESULT_FAILED,         ///< Failed to join the network
    NETWORK_JOIN_RESULT_ABORTED,        ///< Joining was aborted (via command)
} NetworkJoinResult;

/**
 * @ingroup api_network_types
 * @brief Network RSSI signal level
 */
typedef enum
{
    NETWORK_RSSI_EXCELLENT              = 0,    ///< \> -20 dBm
    NETWORK_RSSI_VERY_GOOD              = 1,    ///< \> -35 dBm
    NETWORK_RSSI_GOOD                   = 2,    ///< \> -50 dBm
    NETWORK_RSSI_POOR                   = 3,    ///< \> -70 dBm
    NETWORK_RSSI_VERY_POOR              = 4,    ///< < -71 dBm
    NETWORK_RSSI_UNKNOWN                = 5     ///< Not available
} NetworkSignalStrength;

/**
 * @ingroup api_network_types
 * @brief Network security type
 */
typedef enum
{
    NETWORK_SECURITY_OPEN,
    NETWORK_SECURITY_WEP_PSK,
    NETWORK_SECURITY_WPA_AES_PSK,
    NETWORK_SECURITY_WPA_TKIP_PSK,
    NETWORK_SECURITY_WPA2_AES_PSK,
    NETWORK_SECURITY_WPA2_MIXED_PSK,
    NETWORK_SECURITY_WPA2_TKIP_PSK,
    NETWORK_SECURITY_UNKNOWN
} NetworkSecurity;

/**
 * @ingroup api_network_types
 * @brief Network SSID type
 */
typedef struct
{
    uint8_t val[32];        ///< The raw data of the SSID (not necessarily a string)
    uint8_t len;            ///< The length in bytes of the SSID raw data
} Ssid;

/**
 * @ingroup api_network_types
 * @brief Network MAC Address type
 */
typedef struct
{
    uint8_t octet[6];
} MacAddress;

/**
 * @ingroup api_network_types
 * @brief Buffer to hold a MAC address string
 */
typedef char MacAddressStrBuffer[18];

/**
 * @ingroup api_network_types
 * @brief Buffer to hold a SSID string
 */
typedef char SsidStrBuffer[129];

/**
 * @ingroup api_network_types
 * @brief Buffer to hold an IP address string
 */
typedef char IpStrBuffer[18];


// ----------------------------------------------------------------------------


/**
 * @ingroup api_socket_types
 * @brief Socket type
 */
typedef enum
{
    SOCKET_TYPE_UNKNOWN,    ///< Socket type not known
    SOCKET_TYPE_TCP,        ///< TCP Socket type
    SOCKET_TYPE_TLS,        ///< TLS Socket type
    SOCKET_TYPE_UDP,        ///< UDP Socket type
    SOCKET_TYPE_HTTP,       ///< HTTP Socket type,
    SOCKET_TYPE_UDPS,       ///< UDP Server Socket type
    SOCKET_TYPE_GHM,        ///< goHACK.me message type
} SocketType;

/**
 * @ingroup api_socket_types
 * @brief HTTP Socket sub-type
 */
typedef enum
{
    SOCKET_HTTP_GET,        ///< HTTP GET Request socket type
    SOCKET_HTTP_POST,       ///< HTTP POST Request socket type
    SOCKET_HTTP_HEAD,       ///< HTTP HEAD Request socket type
} HttpSocketType;

/**
 * @ingroup api_socket_types
 * @brief Struct for hold HTTP socket configuration
 */
typedef struct
{
    const char *contextType;    ///< A POST Request 'context-type' value
    const char *certName;       ///< TLS certificate filename on module flash file system
    bool openOnly;              ///< Only open the connection, don't issue the request yet
    HttpSocketType type;        ///< The type of HTTP connection
} HttpSocketArgs;


// ----------------------------------------------------------------------------


/**
 * @ingroup api_file_types
 * @brief File flags type
 */
typedef enum
{
    FILE_FLAG_NONE          = 0,            ///< No flags

    FILE_FLAG_VALID         = (1 << 0),     ///< File valid
    FILE_FLAG_EXECUTABLE    = (1 << 1),     ///< File executable
    FILE_FLAG_ENCRYPTED     = (1 << 2),     ///< File encrypted
    FILE_FLAG_INTERNAL      = (1 << 3),     ///< File on internal module flash
    FILE_FLAG_BOOTABLE      = (1 << 4),     ///< File bootable
    FILE_FLAG_USER          = (1 << 5),     ///< File created by user
    FILE_FLAG_ESSENTIAL     = (1 << 6),     ///< File is essential

    FILE_FLAG_INVALID       = 0xFFFF        ///< File flags invalid
} FileFlags;

/**
 * @ingroup api_file_types
 * @brief File type type
 */
typedef enum
{
    FILE_TYPE_UPGRADE_APP   = 0x00,         ///< Internal upgrade application
    FILE_TYPE_WIFI_FW       = 0x01,         ///< Wifi firmware binary

    FILE_TYPE_REGULAR_APP   = 0x81,         ///< Regular application

    FILE_TYPE_USER_RANGE_START = 150,       ///< User type start index
    FILE_TYPE_USER_RANGE_END = 199,         ///< User type end index

    FILE_TYPE_TEMPORY       = 0xF9,         ///< Temporary file
    FILE_TYPE_GPIO_CONFIG   = 0xFA,         ///< GPIO configuration file
    FILE_TYPE_COMMAND_HELP  = 0xFB,         ///< WiConnect command help file
    FILE_TYPE_SDC_CAPS      = 0xFC,         ///< SDC / goHACK.me file
    FILE_TYPE_SETUP_SCRIPT  = 0xFD,         ///< Setup script file
    FILE_TYPE_MISC_FIX_LEN  = 0xFE,         ///< Miscellaneous fixed length file
    FILE_TYPE_UNKNOWN       = 0xFF,         ///< Unknown file type
    FILE_TYPE_ANY           = FILE_TYPE_UNKNOWN
} FileType;


// ----------------------------------------------------------------------------

/**
 * @ingroup api_ghm_types
 * @brief goHACK.me stream or control value type
 */
typedef enum
{
    GHM_VALUE_INT,
    GHM_VALUE_STR
} GhmElementValueType;

/**
 * @ingroup api_ghm_types
 * @brief goHACK.me stream or control value
 */
typedef struct
{
    const char *elementName;
    GhmElementValueType type;
    union
    {
       const char *strValue;
       uint32_t intValue;
    } u;
} GhmElement;

/**
 * @ingroup api_ghm_types
 * @brief Array of goHACK.me stream or control values
 */
typedef struct
{
    uint16_t count;
    GhmElement elements[1];
} GhmElementArray;

/**
 * @ingroup api_ghm_types
 * @brief goHACK.me sychronization type
 */
typedef enum
{
    GHM_SYNC_ALL,           ///< Push and pull all streams and controls
    GHM_SYNC_PUSH_ONLY,     ///< Only push streams and controls to remote server
    GHM_SYNC_PULL_ONLY      ///< Only pull controls from remote server
} GhmSyncType;

/**
 * @ingroup api_ghm_types
 * @brief goHACK.me message GET type
 */
typedef enum
{
    GHM_MSG_GET_DATA_ONLY,  ///< Only GET the message data
    GHM_MSG_GET_BODY,       ///< Get the message body. This is JSON formatted.
    GHM_MSG_GET_ALL         ///< Get entire message. This includes headers. Is JSON formatted.
} GhmMessageGetType;



// ----------------------------------------------------------------------------


// Forward declarations

class Wiconnect;
class TimeoutTimer;
class PeriodicTimer;
class QueuedCommand;
class CommandQueue;
class LogFunc;
class ReaderFunc;
class ReaderFuncCallback;
class Callback;
class ScanResult;
class ScanResultList;
class WiconnectSocket;
class WiconnectSerial;
class WiconnectFile;
class FileList;
class Gpio;
class SocketIrqHandlerMap;
class WiconnectUdpServer;
class GhmMessageList;
class GhmMessage;

}
