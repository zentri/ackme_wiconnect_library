/*
 * Copyright 2014, ACKme Networks
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of ACKme Networks;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of ACKme Networks.
 */


#include "Wiconnect.h"
#include "internal/common.h"


#define ATTEMPT_TIMEOUT 500


/*************************************************************************************************/
// attempt to configure the module data bus for the uart the wiconnect library was configured
// to use. this is done as follows:
// IF configured uart == Serial1, then
//   use Serial0 to issue 'set bus.data_bus uart1'
// ELIF configured uart == Serial0, then
//   use Serial1 to issue 'set bus.data_bus uart0'
// ELSE
//   not supported, return false
//
bool Wiconnect::configureModuleDataBus(void)
{
    HardwareSerial *cfgUart = serial.serial;
    HardwareSerial *otherUart;
    char cmd[32];
    char buffer[32];
    if(cfgUart == &Serial)
    {
        otherUart = &Serial1;
        strcpy(cmd, "set bus.data_bus uart1\r\n");
    }
    else if(cfgUart == &Serial1)
    {
        otherUart = &Serial;
        strcpy(cmd, "set bus.data_bus uart0\r\n");
    }
    else
    {
        return false;
    }

    otherUart->begin(serial.getBaud());

    for(int attempt = 0; attempt < 7; ++attempt)
    {
        char *ptr = buffer;
        int remaining = sizeof(buffer)-1;
        timeoutTimer.reset();
        otherUart->write(cmd);

        while(remaining > 0 && !timeoutTimer.timedOut(ATTEMPT_TIMEOUT))
        {
            const int bytesRead = otherUart->readBytes(ptr, remaining);
            ptr += bytesRead;
            *ptr = 0;
            remaining -= bytesRead;

            if(strstr(buffer, "Set OK") != NULL)
            {
                otherUart->write("save\r\nreboot\r\n");
                return true;
            }
        }
    }

    return false;
}
