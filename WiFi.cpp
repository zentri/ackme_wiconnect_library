
#include "api/types/WiFi/WiFi.h"
#include "Wiconnect.h"

extern "C" {
#include "api/types/WiFi/utility/wl_definitions.h"
#include "api/types/WiFi/utility/wl_types.h"
}

#define CHECK_INITIALIZED_WITH_ERROR_CODE() if(driver == NULL) return WL_NO_SHIELD
#define CHECK_INITIALIZED() if(driver == NULL) return
#define NO_SOCKET_AVAIL     255


Wiconnect *driver = NULL;


/*************************************************************************************************/
WiFiClass::WiFiClass()
{
}

/*************************************************************************************************/
void WiFiClass::init()
{
}

/*************************************************************************************************/
//uint8_t WiFiClass::getSocket()
//{
//    CHECK_INITIALIZED_WITH_ERROR_CODE();
//
//    return NO_SOCKET_AVAIL;
//}

/*************************************************************************************************/
char* WiFiClass::firmwareVersion()
{
    WiconnectResult result;

    if(driver == NULL)
    {
        return 0;
    }

    while(WICONNECT_IS_PROCESSING(result, driver->getVersion()))
    {
    }
    return (result == WICONNECT_SUCCESS) ? driver->getResponseBuffer() : 0;
}

/*************************************************************************************************/
int WiFiClass::begin(char* ssid)
{
    return begin(ssid, "\\0");
}

/*************************************************************************************************/
//int WiFiClass::begin(char* ssid, uint8_t key_idx, const char *key)
//{
//    // TODO: WEP currently isn't supported by the Wiconnect API.
//    // (The Wiconnect module supports WEP however, so the API could/will be modified to support it)
//   return WL_CONNECT_FAILED;
//}

/*************************************************************************************************/
int WiFiClass::begin(char* ssid, const char *passphrase)
{
    WiconnectResult result;

    CHECK_INITIALIZED_WITH_ERROR_CODE();

    while(WICONNECT_IS_PROCESSING(result, driver->join(ssid, passphrase)))
    {
    }

    if(result == WICONNECT_SUCCESS)
    {
        return WL_CONNECTED;
    }
    else
    {
        return status();
    }
}

/*************************************************************************************************/
void WiFiClass::config(IPAddress local_ip)
{
    config(local_ip, INADDR_NONE, INADDR_NONE, INADDR_NONE);
}

/*************************************************************************************************/
void WiFiClass::config(IPAddress local_ip, IPAddress dns_server)
{
    config(local_ip, dns_server, INADDR_NONE, INADDR_NONE);
}

/*************************************************************************************************/
void WiFiClass::config(IPAddress local_ip, IPAddress dns_server, IPAddress gateway)
{
    config(local_ip, dns_server, gateway, INADDR_NONE);
}

/*************************************************************************************************/
void WiFiClass::config(IPAddress local_ip, IPAddress dns_server, IPAddress gateway, IPAddress subnet)
{
    WiconnectResult result;

    CHECK_INITIALIZED();

    if((uint32_t)local_ip == 0)
    {
        while(WICONNECT_IS_PROCESSING(result, driver->setDhcpEnabled(true)))
        {
        }
    }
    else
    {
        while(WICONNECT_IS_PROCESSING(result, driver->setDhcpEnabled(false)))
        {
        }
        while(WICONNECT_IS_PROCESSING(result, driver->setIpSettings((uint32_t)local_ip, (uint32_t)subnet, (uint32_t)gateway)))
        {
        }
        while(WICONNECT_IS_PROCESSING(result, driver->setDnsAddress((uint32_t)dns_server)))
        {
        }
    }
}

/*************************************************************************************************/
void WiFiClass::setDNS(IPAddress dns_server1)
{
    WiconnectResult result;

    CHECK_INITIALIZED();

    while(WICONNECT_IS_PROCESSING(result, driver->setDnsAddress((uint32_t)dns_server1)))
    {
    }
}

/*************************************************************************************************/
int WiFiClass::disconnect()
{
    WiconnectResult result;

    CHECK_INITIALIZED_WITH_ERROR_CODE();

    while(WICONNECT_IS_PROCESSING(result, driver->leave()))
    {
    }

    return (result == WICONNECT_SUCCESS) ? WL_SUCCESS : WL_FAILURE;
}

/*************************************************************************************************/
uint8_t* WiFiClass::macAddress(uint8_t* mac)
{
    memset(mac, 0, WL_MAC_ADDR_LENGTH);
    if(driver != NULL)
    {
        WiconnectResult result;
        while(WICONNECT_IS_PROCESSING(result, driver->getMacAddress((MacAddress*)mac)))
        {
        }
    }

    return mac;
}

/*************************************************************************************************/
IPAddress WiFiClass::localIP()
{
    IPAddress ret;
    if(driver != NULL)
    {
        WiconnectResult result;
        char *str;
        while(WICONNECT_IS_PROCESSING(result, driver->getSetting("network.ip", &str)))
        {
        }
        if(result == WICONNECT_SUCCESS)
        {
            uint32_t ip;
            Wiconnect::strToIp(str, &ip);
            ret = ip;
        }
    }
    return ret;
}

/*************************************************************************************************/
IPAddress WiFiClass::subnetMask()
{
    IPAddress ret;
    if(driver != NULL)
    {
        WiconnectResult result;
        char *str;
        while(WICONNECT_IS_PROCESSING(result, driver->getSetting("network.netmask", &str)))
        {
        }
        if(result == WICONNECT_SUCCESS)
        {
            uint32_t ip;
            Wiconnect::strToIp(str, &ip);
            ret = ip;
        }
    }
    return ret;
}

/*************************************************************************************************/
IPAddress WiFiClass::gatewayIP()
{
    IPAddress ret;
    if(driver != NULL)
    {
        WiconnectResult result;
        char *str;
        while(WICONNECT_IS_PROCESSING(result, driver->getSetting("network.gateway", &str)))
        {
        }
        if(result == WICONNECT_SUCCESS)
        {
            uint32_t ip;
            Wiconnect::strToIp(str, &ip);
            ret = ip;
        }
    }
    return ret;
}

/*************************************************************************************************/
char* WiFiClass::SSID()
{
    char *ssid = 0;
    if(driver != NULL)
    {
        WiconnectResult result;
        while(WICONNECT_IS_PROCESSING(result, driver->getSetting("wlan.ssid", &ssid)))
        {
        }
    }
    return ssid;
}

/*************************************************************************************************/
//uint8_t* WiFiClass::BSSID(uint8_t* bssid)
//{
//	uint8_t* _bssid = WiFiDrv::getCurrentBSSID();
//	memcpy(bssid, _bssid, WL_MAC_ADDR_LENGTH);
//    return bssid;
//}

/*************************************************************************************************/
int32_t WiFiClass::RSSI()
{
    int32_t rssi = -999;
    if(driver != NULL)
    {
        WiconnectResult result;
        while(WICONNECT_IS_PROCESSING(result, driver->getRssi(&rssi)))
        {
        }
    }
    return rssi;
}

/*************************************************************************************************/
//uint8_t WiFiClass::encryptionType()
//{
//    return WiFiDrv::getCurrentEncryptionType();
//}

/*************************************************************************************************/
//int8_t WiFiClass::scanNetworks()
//{
//	uint8_t attempts = 10;
//	uint8_t numOfNetworks = 0;
//
//	if (WiFiDrv::startScanNetworks() == WL_FAILURE)
//		return WL_FAILURE;
// 	do
// 	{
// 		delay(2000);
// 		numOfNetworks = WiFiDrv::getScanNetworks();
// 	}
//	while (( numOfNetworks == 0)&&(--attempts>0));
//	return numOfNetworks;
//}
//
///*************************************************************************************************/
//char* WiFiClass::SSID(uint8_t networkItem)
//{
//	return WiFiDrv::getSSIDNetoworks(networkItem);
//}
//
///*************************************************************************************************/
//int32_t WiFiClass::RSSI(uint8_t networkItem)
//{
//	return WiFiDrv::getRSSINetoworks(networkItem);
//}
//
///*************************************************************************************************/
//uint8_t WiFiClass::encryptionType(uint8_t networkItem)
//{
//    return WiFiDrv::getEncTypeNetowrks(networkItem);
//}

/*************************************************************************************************/
uint8_t WiFiClass::status()
{
    WiconnectResult result;
    NetworkJoinResult joinResult;

    if(driver == NULL || !driver->isInitialized())
    {
        driver = Wiconnect::getInstance();
        if(driver->init() != WICONNECT_SUCCESS)
        {
            driver = NULL;
            return WL_NO_SHIELD;
        }
    }

    while(WICONNECT_IS_PROCESSING(result, driver->getNetworkJoinResult(&joinResult)))
    {
    }
    if(result == WICONNECT_SUCCESS)
    {
        switch(joinResult)
        {
        case NETWORK_JOIN_RESULT_SUCCESS:
            return WL_CONNECTED;
        case NETWORK_JOIN_RESULT_NO_SSID:
            return WL_NO_SSID_AVAIL;
        case NETWORK_JOIN_RESULT_NONE:
            return WL_IDLE_STATUS;
        default:
            break;
        }
    }

    return WL_CONNECT_FAILED;
}

/*************************************************************************************************/
int WiFiClass::hostByName(const char* aHostname, IPAddress& aResult)
{
    WiconnectResult result;
    uint32_t ip;

    CHECK_INITIALIZED_WITH_ERROR_CODE();

    while(WICONNECT_IS_PROCESSING(result, driver->lookup(aHostname, &ip)))
    {
    }

    aResult = ip;
    return (result == WICONNECT_SUCCESS) ? 1 : -1;
}

WiFiClass WiFi;
