/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#include "Wiconnect.h"
#include "api/types/ScanResult.h"
#include "internal/common.h"


/*************************************************************************************************/
ScanResultList::ScanResultList(int bufferLen_, void *buffer_)
{
    Wiconnect *wiconnect = Wiconnect::getInstance();
    wiconnect_assert(wiconnect, "ScanResultList(), bad buffer", (bufferLen_ == 0 && buffer_ == NULL) || (bufferLen_ != 0 && buffer_ != NULL));
    buffer = (uint8_t*)buffer_;
    bufferLen = bufferLen_;
    reset();
}

/*************************************************************************************************/
ScanResultList::~ScanResultList()
{
    reset();
}

/*************************************************************************************************/
void ScanResultList::reset(void)
{
#ifdef WICONNECT_ENABLE_MALLOC
    if(buffer == NULL)
    {
        ScanResult* result = listHead;
        while(result != NULL)
        {
            ScanResult* tmp = result;
            result = result->next;
            delete tmp;
        }
    }
#endif
    listHead = listTail = NULL;
    bufferPtr = buffer;
    bufferRemaining = bufferLen;
    count = 0;
}


/*************************************************************************************************/
WiconnectResult ScanResultList::add(const char *channelStr, const char *rssiStr, const char* macStr, const char *rateStr, const char *secStr, const char *ssidStr)
{
    WiconnectResult result;
    ScanResult *res;

    if(buffer == NULL)
    {
#ifdef WICONNECT_ENABLE_MALLOC
        res = new ScanResult();
        if(res == NULL)
#endif
        {
            return WICONNECT_NULL_BUFFER;
        }
    }
    else
    {
        if(bufferRemaining < sizeof(ScanResult))
        {
            return WICONNECT_OVERFLOW;
        }
        res = (ScanResult*)bufferPtr;
        memset(res, 0, sizeof(ScanResult));
        bufferRemaining -= sizeof(ScanResult);
        bufferPtr += sizeof(ScanResult);
    }

    if(WICONNECT_FAILED(result, res->init(channelStr, rssiStr, macStr, rateStr, secStr, ssidStr)))
    {
        if(buffer == NULL)
        {
            delete res;
        }
    }
    else
    {
        if(listHead == NULL)
        {
            listHead = listTail = res;
        }
        else
        {
            res->previous = listTail;
            listTail->next = res;
            listTail = res;
        }
        ++count;
    }

    return result;
}

/*************************************************************************************************/
const ScanResult* ScanResultList::getListHead() const
{
    return listHead;
}

/*************************************************************************************************/
int ScanResultList::getCount() const
{
    return count;
}

/*************************************************************************************************/
const ScanResult* ScanResultList::getResult(int i) const
{
    if(i >= count)
        return NULL;

    ScanResult* result = listHead;
    while(i-- != 0)
        result = result->next;

    return result;
}

/*************************************************************************************************/
const ScanResult* ScanResultList::operator [](int i) const
{
    return getResult(i);
}





