/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#include "Wiconnect.h"
#include "api/types/ScanResult.h"
#include "api/NetworkInterface.h"
#include "internal/common.h"


static inline bool floatToFixedPointInt(const char *str, uint32_t *res);



/*************************************************************************************************/
ScanResult::ScanResult()
{
    next = NULL;
    previous = NULL;
    channel = 0xff;
    rssi = -9999;
    rate = 0;
    security = NETWORK_SECURITY_UNKNOWN;
    memset(&mac, 0, (uint32_t)sizeof(mac));
    memset(&ssid, 0, (uint32_t)sizeof(ssid));
}

/*************************************************************************************************/
WiconnectResult ScanResult::init(const char *channelStr, const char *rssiStr, const char* macStr, const char *rateStr, const char *secStr, const char *ssidStr)
{
    intmax_t r;
    if(!StringUtil::parseInt(channelStr, &r, 0, 15))
    {
        return WICONNECT_RESPONSE_PARSE_ERROR;
    }
    channel = (int)r;
    if(!StringUtil::parseInt(rssiStr, &r, -200, 100))
    {
        return WICONNECT_RESPONSE_PARSE_ERROR;
    }
    rssi = (int)r;

    if(!Wiconnect::strToMacAddress(macStr, &mac))
    {
        return WICONNECT_RESPONSE_PARSE_ERROR;
    }

    if(!floatToFixedPointInt(rateStr, &rate))
    {
        return WICONNECT_RESPONSE_PARSE_ERROR;
    }
    security = Wiconnect::strToNetworkSecurity(secStr);

    if(!Wiconnect::strToSsid(ssidStr, &ssid))
    {
        return WICONNECT_RESPONSE_PARSE_ERROR;
    }

    return WICONNECT_SUCCESS;
}

#ifdef WICONNECT_ENABLE_MALLOC
/*************************************************************************************************/
void* ScanResult::operator new(size_t size)
{
    Wiconnect *wiconnect = Wiconnect::getInstance();
    wiconnect_assert(wiconnect, "ScanResult:new, malloc not defined", wiconnect->_malloc != NULL);
    return Wiconnect::getInstance()->_malloc(size);
}

/*************************************************************************************************/
void ScanResult::operator delete(void* ptr)
{
    Wiconnect *wiconnect = Wiconnect::getInstance();
    wiconnect_assert(wiconnect, "ScanResult:delete, free not defined", wiconnect->_free != NULL);
    Wiconnect::getInstance()->_free(ptr);
}
#endif

/*************************************************************************************************/
uint8_t ScanResult::getChannel() const
{
    return channel;
}
/*************************************************************************************************/
NetworkSignalStrength ScanResult::getSignalStrength() const
{
    return Wiconnect::rssiToSignalStrength(rssi);
}
/*************************************************************************************************/
const MacAddress* ScanResult::getMacAddress() const
{
    return &mac;
}
/*************************************************************************************************/
uint32_t ScanResult::getRate() const
{
    return rate;
}
/*************************************************************************************************/
const char* ScanResult::getRateStr(char *buffer) const
{
    SET_STR_BUFFER(buffer, 16);
    uint32_t i = rate / 10;
    uint32_t f = rate % 10;
    sprintf(ptr, "%u.%u", (unsigned int)i, (unsigned int)f);
    return ptr;
}
/*************************************************************************************************/
NetworkSecurity ScanResult::getSecurityType() const
{
    return security;
}
/*************************************************************************************************/
const Ssid* ScanResult::getSsid() const
{
    return &ssid;
}

/*************************************************************************************************/
const ScanResult* ScanResult::getNext() const
{
    return next;
}

/*************************************************************************************************/
const ScanResult* ScanResult::getPrevious() const
{
    return previous;
}


/*************************************************************************************************/
static inline bool floatToFixedPointInt(const char *str, uint32_t *res)
{
    intmax_t i;
    intmax_t f = 0;
    char buffer[32];

    strcpy(buffer, str);

    char* frac = strchr(buffer, '.');
    if(frac != NULL)
    {
        *frac = 0;
        ++frac;
    }

    if(!StringUtil::parseInt(buffer, &i, 0, 1000))
    {
        return false;
    }
    if(frac != NULL && !StringUtil::parseInt(frac, &f, 0, 9))
    {
        return false;
    }

    *res = (((uint32_t)i) * 10) + (uint32_t)f;

    return true;
}

