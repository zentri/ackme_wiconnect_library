
/*
  Web client

 This sketch connects to a website (http://www.google.com)
 using a WiFi shield.

 This example is written for a network using WPA encryption. For
 WEP or WPA, change the Wifi.begin() call accordingly.

 This example is written for a network using WPA encryption. For
 WEP or WPA, change the Wifi.begin() call accordingly.

 Circuit:
 * WiFi shield attached

 created 13 July 2010
 by dlf (Metodo2 srl)
 modified 31 May 2012
 by Tom Igoe
 */

#include <Wiconnect.h>

#include "target_config.h" // this is needed for platform specific configuration



char ssid[] = "yourNetwork"; //  your network SSID (name)
char pass[] = "secretPassword";    // your network password (use for WPA)



void printWifiStatus();


int status = WL_IDLE_STATUS;
// if you don't want to use DNS (and reduce your sketch size)
// use the numeric IP instead of the name for the server:
//IPAddress server(74,125,232,128);  // numeric IP for Google (no DNS)
char server[] = "www.ack.me";    // name address for Google (using DNS)


SerialConfig serialConfig(WICONNECT_UART);
uint8_t wiconnectInternalBuffer[WICONNECT_INTERNAL_BUFFER_SIZE];
Wiconnect wiconnectIfc(serialConfig, sizeof(wiconnectInternalBuffer), wiconnectInternalBuffer, WICONNECT_RESET_PIN); // NOTE: this MUST be initialized first!!


// Initialize the Ethernet client library
// with the IP address and port of the server
// that you want to connect to (port 80 is default for HTTP):
WiFiClient client;

void setup() {
    //Initialize serial and wait for port to open:
    Serial.begin(CONSOLE_BAUD);
    while (!Serial) {
        ; // wait for serial port to connect. Needed for Leonardo only
    }

    //ADD_DEBUG_LOGGER();
    Serial.println("Waiting for WiFi module to initialize...");

    // check for the presence of the shield:
    if (WiFi.status() == WL_NO_SHIELD) {
        Serial.println("WiFi shield not present");
        // don't continue:
        while (true);
    }

    String fv = WiFi.firmwareVersion();
    Serial.print("Firmware version:");
    Serial.println(fv);

    // attempt to connect to Wifi network:
    while (status != WL_CONNECTED) {
        Serial.print("Attempting to connect to SSID: ");
        Serial.println(ssid);
        // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
        status = WiFi.begin(ssid, pass);

        if(status != WL_CONNECTED)
        {
            // wait 10 seconds for connection:
            delay(10000);
        }
    }
    Serial.println("Connected to wifi");
    printWifiStatus();

    Serial.println("\nStarting connection to server...");
    // if you get a connection, report back via serial:
    if (client.connect(server, 80)) {
        Serial.print("connected to: ");
        Serial.println((const char*)server);
        // Make a HTTP request:
        client.println("GET / HTTP/1.1");
        client.print("Host: ");
        client.println((const char*)server);
        client.println("Connection: close");
        client.println();
    }
}

void loop() {
    // if there are incoming bytes available
    // from the server, read them and print them:
    while (client.available()) {
        char c = client.read();
        Serial.write(c);
    }

    // if the server's disconnected, stop the client:
    if (!client.connected()) {
        Serial.println();
        Serial.println("disconnecting from server.");
        client.stop();

        // do nothing forevermore:
        while (true);
    }
}


void printWifiStatus() {
    // print the SSID of the network you're attached to:
    Serial.print("SSID: ");
    Serial.println(WiFi.SSID());

    // print your WiFi shield's IP address:
    IPAddress ip = WiFi.localIP();
    Serial.print("IP Address: ");
    Serial.println(ip);

    // print the received signal strength:
    long rssi = WiFi.RSSI();
    Serial.print("signal strength (RSSI):");
    Serial.print(rssi);
    Serial.println(" dBm");
}




