/*

 This example connects to an unencrypted Wifi network.
 Then it prints the  MAC address of the Wifi shield,
 the IP address obtained, and other network details.

 Circuit:
 * WiFi shield attached

 created 13 July 2010
 by dlf (Metodo2 srl)
 modified 31 May 2012
 by Tom Igoe
 */
#include <Wiconnect.h>

#include "target_config.h" // this is needed for platform specific configuration


char ssid[] = "yourNetwork";     //  your network SSID (name)
char pass[] = "secretPassword";  // your network password
int status = WL_IDLE_STATUS;     // the Wifi radio's status


void printWifiData();

SerialConfig serialConfig(WICONNECT_UART);
uint8_t wiconnectInternalBuffer[WICONNECT_INTERNAL_BUFFER_SIZE];
Wiconnect wiconnectIfc(serialConfig, sizeof(wiconnectInternalBuffer), wiconnectInternalBuffer, WICONNECT_RESET_PIN); // NOTE: this MUST be initialized first!!



void setup() {
    //Initialize serial and wait for port to open:
    Serial.begin(CONSOLE_BAUD);
    while (!Serial) {
        ; // wait for serial port to connect. Needed for Leonardo only
    }

    //ADD_DEBUG_LOGGER();
    Serial.println("Waiting for WiFi module to initialize...");

    // check for the presence of the shield:
    if (WiFi.status() == WL_NO_SHIELD) {
        Serial.println("WiFi shield not present");
        // don't continue:
        while (true);
    }

    String fv = WiFi.firmwareVersion();
    Serial.print("Firmware version:");
    Serial.println(fv);

    // attempt to connect to Wifi network:
    while ( status != WL_CONNECTED) {
        Serial.print("Attempting to connect to WPA SSID: ");
        Serial.println(ssid);
        // Connect to WPA/WPA2 network:
        status = WiFi.begin(ssid, pass);

        if(status != WL_CONNECTED)
        {
            // wait 10 seconds for connection:
            delay(10000);
        }
    }

    // you're connected now, so print out the data:
    Serial.print("You're connected to the network");
    printWifiData();

}

void loop() {
    // check the network connection once every 10 seconds:
    delay(10000);
}

void printWifiData() {
    // print your WiFi shield's IP address:
    IPAddress ip = WiFi.localIP();
    Serial.print("IP Address: ");
    Serial.println(ip);
    Serial.println(ip);

    // print your MAC address:
    byte mac[6];
    WiFi.macAddress(mac);
    Serial.print("MAC address: ");
    Serial.print(mac[5], HEX);
    Serial.print(":");
    Serial.print(mac[4], HEX);
    Serial.print(":");
    Serial.print(mac[3], HEX);
    Serial.print(":");
    Serial.print(mac[2], HEX);
    Serial.print(":");
    Serial.print(mac[1], HEX);
    Serial.print(":");
    Serial.println(mac[0], HEX);

}
