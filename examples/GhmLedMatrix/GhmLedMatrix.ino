/*
 * Copyright (C) 2014, ACKme Networks, Inc. http://ack.me
 * All Rights Reserved.
 * Version: 7b472aa
 *
 * This software may be used and modified by customers of ACKme Networks.
 * It is provided on an AS-IS basis. The software may only be used in
 * conjunction with ACKme products and all code derivatives must include
 * this header.
 *
 * This and other reference materials may be found at http://ack.me
 *
 * SUMMARY
 *   This demo is a wireless display that can be updated remotely
 *   using a web browser on your phone, tablet or PC connected
 *   to http://goHACK.me
 *
 * HARDWARE
 *   Wi-Fi
 *      AWM006-A02 Mantis board from ACKme Networks
 *      User Guide:
 *      OR
 *      AMW004-E03 Mackerel board from ACKme Networks
 *      User Guide: http://ack.me/resources/download/231/AUG_MW004E
 *      OR
 *      Any other ACKme WiFi module will work as well
 *
 *   Display:
 *      1.2" 8x8 Matrix LED from Adafruit
 *      Install: https://learn.adafruit.com/adafruit-led-backpack/1-2-8x8-matrix
 *
 *   Arduino:
 *      Mega2560 (other Arduinos ok too, but must be 3.3V)
 *
 * DESCRIPTION
 *   The app first checks if the ACKme board is activated with
 *   http://goHACK.me. If not, using the specified username and password,
 *   it activates the module.
 *   The app then connects to goHACK.me via Wi-Fi and retrieves messages
 *   and control information such as:
 *     - Text rotation
 *     - Text Blink
 *     - Display Brightness
 *     - Display On/Off
 *
 * NOTE:
 *   Grab the Adafruit LED backpack library from
 *   the Adafruit website (link above), program the Arduino, and
 *   connect the ACKme board's UART to the Arduino Serial interface.
 *
 *
 */


#define NETWORK_NAME      "YOUR_NETWORK"
#define NETWORK_PASSWORD  "your_password"

// goto http://gohack.me to register
#define GHM_USERNAME "Your_GHM_Username"
#define GHM_PASSWORD "Your_GHM_Pasword"


/*--------------------------------------------------------------------------------------
 Includes
 --------------------------------------------------------------------------------------*/
#include <Wiconnect.h>
#include <Wire.h>
#include "Adafruit_GFX.h"
#include "Adafruit_LEDBackpack.h"
#include "matrix_defines.h"
#include "target_config.h" // this is needed for platform specific configuration



// function proto-types
void update_matrix();
void ghm_poll();
void processMessage();
void writeToDisplay(int cursor, const uint8_t *bitmap);



/*--------------------------------------------------------------------------------------
 Serial Interface Definitions
 --------------------------------------------------------------------------------------*/
Adafruit_8x8matrix matrix = Adafruit_8x8matrix();
#define LOG_SERIAL       Serial
#define LOG_PRINT(a)     LOG_SERIAL.println(a)
#define LOG_WRITE(a)     LOG_SERIAL.write(a)
#define BAUD_RATE        9600

/*--------------------------------------------------------------------------------------
 Defines
 --------------------------------------------------------------------------------------*/

// goHACK.me caps control slug names
#define GHM_CAPS_ONOFF          "onoff"
#define GHM_CAPS_BLINK          "blink"
#define GHM_CAPS_BLINK_RATE     "blinkrate"
#define GHM_CAPS_ROTATION       "rotation"
#define GHM_CAPS_BRIGHTNESS     "brightness"
#define GHM_CAPS_SCROLL_RATE    "scrollrate"
#define GHM_CAPS_SCROLL_ENABLED "scrollenabled"


#define DEFAULT_BLINK_RATE 40 //ms
#define DEFAULT_SCROLL_RATE 100 // ms
#define GHM_CONTROL_POLL_INTERVAL 4000 //ms
#define GHM_MSG_POLL_INTERVAL 4000 //ms

/*--------------------------------------------------------------------------------------
 Variables
 --------------------------------------------------------------------------------------*/

// Internal buffer for wiconnect library
uint8_t wiconnectInternalBuffer[WICONNECT_INTERNAL_BUFFER_SIZE];
// Instantiate the WiConnect Library
Wiconnect wifi(SerialConfig(WICONNECT_UART), sizeof(wiconnectInternalBuffer), wiconnectInternalBuffer, WICONNECT_RESET_PIN);


// Globals controlling the matrix
int    onoff            = MATRIX_ON;
int    blink            = MATRIX_BLINK_OFF;
int    blinkState       = MATRIX_ON;
int    blinkRate        = DEFAULT_BLINK_RATE;
int    rotation         = MATRIX_ROTATE_UP;
int    brightness       = MATRIX_BRIGHTNESS;
char   msgBuffer[256]   = { 0 };
int    msgCursor        = -1;
int    scrollRate       = DEFAULT_SCROLL_RATE;
bool   scrollEnabled    = true;
bool   displayText      = false;

uint32_t    msgScrollTimestamp = 0;
uint32_t    blinkTimestamp = 0;
uint32_t    ghmPollTimestamp = 0;


typedef enum
{
    GHM_STATE_WAIT_FOR_TIMEOUT,
    GHM_STATE_SYNC,
    GHM_STATE_READ_CONTROLS,
    GHM_STATE_GET_MSG_LIST,
    GHM_STATE_OPEN_MSG,
    GHM_STATE_READ_MSG
} ghmPollStateType;
ghmPollStateType ghmPollState = GHM_STATE_WAIT_FOR_TIMEOUT;

// note that these object must be statically allocated to support the API's non-blocking nature
char msgListBuffer[sizeof(GhmMessage)];
GhmMessageList msgList(sizeof(msgListBuffer), msgListBuffer);
WiconnectSocket ghmMsgSocket;


int previousCursor;
const uint8_t *previousSmiley;

typedef enum
{
    SMILEY_NONE,
    SMILEY_UNKNOWN,
    SMILEY_NEUTRAL,
    SMILEY_HAPPY,
    SMILEY_WINK,
    SMILEY_SAD
} SmileyType;
SmileyType smiley_type = SMILEY_UNKNOWN;

/*--------------------------------------------------------------------------------------
 setup()
 Called by the Arduino framework once, before the main loop begins
 --------------------------------------------------------------------------------------*/
void setup()
{
    WiconnectResult result;
    bool isActivated;

    // uncomment to via Wiconnect library debug msgs
    //wifi.setDebugLogger(wiconnectDebugLogger);

    // Setup WiConnect serial ports
    LOG_SERIAL.begin(BAUD_RATE);
    LOG_PRINT("Booting the goHACK.me LED Matrix Demo...");

    // The WiConnect API should be blocking while we're setting up
    wifi.setBlockingEnabled(true);

    // Initialize WiConnect library communication with WiFi module
    if(WICONNECT_FAILED(result, wifi.init()))
    {
        LOG_PRINT("Failed to initialize communications with WiFi module. Is it connected correctly?");
        for(;;); // loop forever
    }

    // attempt to join WiFi network
    if(WICONNECT_FAILED(result, wifi.join(NETWORK_NAME, NETWORK_PASSWORD)))
    {
        LOG_PRINT("Failed to join Wi-Fi network. Are your credentials correct?");
        for(;;); // loop forever
    }

    // Is the WiFi module activated with http://goHACK.me?
    wifi.ghmIsActivated(&isActivated);
    if(!isActivated)
    {
        LOG_PRINT("Activating module with http://goHACK.me");
        // We're not activated, attempt to activate now.
        // first download the LED Matrix capabilities file from the ACKme server
        if(WICONNECT_FAILED(result, wifi.ghmDownloadCapabilities("arduino_ledmatrix")))
        {
            LOG_PRINT("Failed to download ACKme LED Matrix capabilities file");
            for(;;); // loop forever
        }
        // now attempt to activate the module with http://goHACK.me
        else if(WICONNECT_FAILED(result, wifi.ghmActivate(GHM_USERNAME, GHM_PASSWORD, "arduino_ledmatrix.json")))
        {
            LOG_PRINT("Failed to activate module with http://goHACK.me\r\nIs your username and password correct?");
            for(;;); // loop forever
        }

    }

    // Set the I2C address of the matrix
    matrix.begin(MATRIX_I2C_ADDRESS);

    // Initialize Matrix
    matrix.setTextSize(1);
    matrix.setTextWrap(false);  // Don't want text to wrap, make it scroll instead

    LOG_PRINT("Ready. Login to http://goHACK.me to control the LED matrix.");
}

/*--------------------------------------------------------------------------------------
 Arduino main loop
 --------------------------------------------------------------------------------------*/
void loop()
{
    ghm_poll();
    update_matrix();
}

/*--------------------------------------------------------------------------------------
 LED Matrix: Update the display
 --------------------------------------------------------------------------------------*/
void update_matrix()
{
    if(smiley_type != SMILEY_NONE)
    {
        const uint8_t *smile_bmp_ptr;

        switch(smiley_type)
        {
        case SMILEY_NEUTRAL:    smile_bmp_ptr = neutral_bmp; break;
        case SMILEY_HAPPY:      smile_bmp_ptr = smile_bmp; break;
        case SMILEY_WINK:       smile_bmp_ptr = wink_bmp; break;
        case SMILEY_SAD:        smile_bmp_ptr = frown_bmp; break;
        default:                smile_bmp_ptr = cross_bmp; break;
        }

        smiley_type = SMILEY_NONE;
        writeToDisplay(-1, smile_bmp_ptr);
    }
    else if(displayText && scrollEnabled)
    {
        const unsigned long now = millis();
        if((now - msgScrollTimestamp) > (uint32_t)scrollRate)
        {
            msgScrollTimestamp = now;
            writeToDisplay(msgCursor, NULL);
            msgCursor = (msgCursor + 1) % (strlen(msgBuffer)*6);
        }

    }

    if(blink && onoff)
    {
        const unsigned long now = millis();
        if((now - blinkTimestamp) > (uint32_t)blinkRate)
        {
            blinkTimestamp = now;
            writeToDisplay(-1, NULL);
            blinkState = !blinkState;
        }
    }

}


/*--------------------------------------------------------------------------------------
 Poll http://goHACK.me for control updates
 --------------------------------------------------------------------------------------*/
void ghm_poll()
{
    WiconnectResult result;

    // wait until the polling period times out
    if(ghmPollState == GHM_STATE_WAIT_FOR_TIMEOUT)
    {
        // only poll at specified intervals
        const unsigned long now = millis();
        if((now - ghmPollTimestamp) < GHM_CONTROL_POLL_INTERVAL)
        {
            // still haven't timed-out, just return
            return;
        }
        // we timed-out
        ghmPollTimestamp = now;
        // set state to synchronize with http://goHACK.me
        ghmPollState = GHM_STATE_SYNC;
        // this command can take awhile to complete so enable non-blocking mode
        wifi.setBlockingEnabled(false);
    }

    if(ghmPollState == GHM_STATE_SYNC)
    {
        // continuously call until the api call completes
        if(!WICONNECT_IS_PROCESSING(result, wifi.ghmSynchronize()))
        {
            // the call has completed, read the control values
            ghmPollState = GHM_STATE_READ_CONTROLS;
            // these calls are fast so lets re-enabled blocking mode (to make our lives easier ;)
            wifi.setBlockingEnabled(true);
        }
    }
    if(ghmPollState == GHM_STATE_READ_CONTROLS)
    {
        uint32_t newval;
        bool valueChanged = false;

        // read the onoff control
        wifi.ghmRead(GHM_CAPS_ONOFF, &newval);
        if((int)newval != onoff)
        {
            valueChanged = true;
            onoff = newval;
            writeToDisplay(-1, NULL);
        }

        // read the rotation control
        wifi.ghmRead(GHM_CAPS_ROTATION, &newval);
        if((int)newval != rotation)
        {
            valueChanged = true;
            rotation = newval;
            matrix.setRotation(rotation);
            writeToDisplay(-1, NULL);
        }

        // read the blink control
        wifi.ghmRead(GHM_CAPS_BLINK, &newval);
        if((int)newval != blink)
        {
            valueChanged = true;
            blink = newval;
            if(blink == 0)
            {
                blinkState = MATRIX_ON;
            }
        }

        // read the blinkrate control
        wifi.ghmRead(GHM_CAPS_BLINK_RATE, &newval);
        if((int)newval != blinkRate)
        {
            valueChanged = true;
            blinkRate = newval;
        }

        // read the brightness control
        wifi.ghmRead(GHM_CAPS_BRIGHTNESS, &newval);
        if((int)newval != brightness)
        {
            valueChanged = true;
            brightness = newval;
            matrix.setBrightness(brightness);
        }

        // read the scroll rate control
        wifi.ghmRead(GHM_CAPS_SCROLL_RATE, &newval);
        if((int)newval != scrollRate)
        {
            valueChanged = true;
            scrollRate = newval;
        }

        // read the scroll type control
        wifi.ghmRead(GHM_CAPS_SCROLL_ENABLED, &newval);
        if((int)newval != scrollEnabled)
        {
            valueChanged = true;
            scrollEnabled = newval;
        }

        if(valueChanged)
        {
            LOG_SERIAL.println("------------------");
            LOG_SERIAL.print("On/off state: ");
            LOG_SERIAL.println(onoff);
            LOG_SERIAL.print("Rotation: ");
            LOG_SERIAL.println(rotation);
            LOG_SERIAL.print("Blink state: ");
            LOG_SERIAL.println(blink);
            LOG_SERIAL.print("Blink rate: ");
            LOG_SERIAL.println(blinkRate);
            LOG_SERIAL.print("Brightness: ");
            LOG_SERIAL.println(brightness);
            LOG_SERIAL.print("Scroll rate: ");
            LOG_SERIAL.println(scrollRate);
            LOG_SERIAL.print("Scroll enabled: ");
            LOG_SERIAL.println(scrollEnabled);
        }

        // now retreive a message list
        ghmPollState = GHM_STATE_GET_MSG_LIST;
        // this command can take awhile to complete so enable non-blocking mode
        wifi.setBlockingEnabled(false);
    }
    if(ghmPollState == GHM_STATE_GET_MSG_LIST)
    {
        // continuously call until the api call completes
        if(!WICONNECT_IS_PROCESSING(result, wifi.ghmListMessages(msgList, 1)))
        {
            // the call has completed, is a msg available?
            if(msgList.getCount())
            {
                // yes, open the msg for reading
                ghmPollState = GHM_STATE_OPEN_MSG;
            }
            else
            {
                // no, wait for the next poll timeout
                ghmPollState = GHM_STATE_WAIT_FOR_TIMEOUT;
            }
        }
    }
    if(ghmPollState == GHM_STATE_OPEN_MSG)
    {
        // continuously call until the api call completes
        if(!WICONNECT_IS_PROCESSING(result, wifi.ghmGetMessage(ghmMsgSocket)))
        {
            // the call has completed, go to the read msg state
            ghmPollState = GHM_STATE_READ_MSG;
        }
    }
    if(ghmPollState == GHM_STATE_READ_MSG)
    {
        uint16_t bytesRead = 0;
        // continuously call until the api call completes
        if(!WICONNECT_IS_PROCESSING(result, ghmMsgSocket.read(msgBuffer, sizeof(msgBuffer)-1, &bytesRead)))
        {
            // the call has completed
            // null-terminate the string
            msgBuffer[bytesRead+1] = 0;
            // process the message
            processMessage();

            // cleanup the socket
            // enabled blocking mode again to make our lives easier
            // (the calls execute fairly fast anyways)
            wifi.setBlockingEnabled(true);
            // close the ghm msg socket
            ghmMsgSocket.close();
            // delete the msg from the server
            wifi.ghmDeleteMessage((uint8_t)0);

            // wait for the next timeout
            ghmPollState = GHM_STATE_WAIT_FOR_TIMEOUT;
        }
    }

}

/*--------------------------------------------------------------------------------------
 Process the received message
 --------------------------------------------------------------------------------------*/
void processMessage()
{
    smiley_type = SMILEY_NONE;

    // check if the message is a smiley face
    if(strcmp(msgBuffer, ":|") == 0)
    {
        smiley_type = SMILEY_NEUTRAL;
        LOG_PRINT("*** New msg: Neutral simley");
    }
    else if(strcmp(msgBuffer, ":)") == 0)
    {
        smiley_type = SMILEY_HAPPY;
        LOG_PRINT("*** New msg: Happy simley");
    }
    else if(strcmp(msgBuffer, ";)") == 0)
    {
        smiley_type = SMILEY_WINK;
        LOG_PRINT("*** New msg: Wink simley");
    }
    else if(strcmp(msgBuffer, ":(") == 0)
    {
        smiley_type = SMILEY_SAD;
        LOG_PRINT("*** New msg: Sad simley");
    }

    // if we received a smiley face, then disable msg scrolling
    if(smiley_type != SMILEY_NONE)
    {
        displayText = false;
    }
    else
    {
        displayText = true;
        msgCursor = 0;
        LOG_SERIAL.print("*** New msg: ");
        LOG_SERIAL.println(msgBuffer);
        writeToDisplay(msgCursor, NULL);
    }

}

void writeToDisplay(int cursor, const uint8_t *bitmap)
{
    const int displayState = (onoff && blinkState);
    matrix.clear();

    if(cursor != -1)
    {
        previousSmiley = NULL;
        previousCursor = cursor;
    }
    else if(bitmap != NULL)
    {
        previousSmiley = bitmap;
        previousCursor = -1;
    }

    if(previousSmiley != NULL)
    {
        matrix.drawBitmap(0, 0, previousSmiley, 8, 8, displayState);
    }
    else if(displayText)
    {
        matrix.setTextColor(displayState);
        matrix.setCursor(-previousCursor,0);
        matrix.print(msgBuffer);
    }

    matrix.writeDisplay();
}
