/*
  Repeating Wifi Web Client

 This sketch connects to a a web server and makes a request
 using an Arduino Wifi shield.

 Circuit:
 * WiFi shield attached to pins SPI pins and pin 7

 created 23 April 2012
 modified 31 May 2012
 by Tom Igoe
 modified 13 Jan 2014
 by Federico Vanzati

 http://arduino.cc/en/Tutorial/WifiWebClientRepeating
 This code is in the public domain.
 */

#include <Wiconnect.h>

#include "target_config.h" // this is needed for platform specific configuration


char ssid[] = "yourNetwork";      //  your network SSID (name)
char pass[] = "secretPassword";   // your network password



void httpRequest();
void printWifiStatus();



SerialConfig serialConfig(WICONNECT_UART);
uint8_t wiconnectInternalBuffer[WICONNECT_INTERNAL_BUFFER_SIZE];
Wiconnect wiconnectIfc(serialConfig, sizeof(wiconnectInternalBuffer), wiconnectInternalBuffer, WICONNECT_RESET_PIN); // NOTE: this MUST be initialized first!!

// Initialize the Wifi client library
WiFiClient client;

int status = WL_IDLE_STATUS;
// server address:
const char server[] = "www.arduino.cc";
//IPAddress server(64,131,82,241);

unsigned long lastConnectionTime = 0;            // last time you connected to the server, in milliseconds
const unsigned long postingInterval = 10L * 1000L; // delay between updates, in milliseconds

void setup() {
    //Initialize serial and wait for port to open:
    Serial.begin(CONSOLE_BAUD);
    while (!Serial) {
        ; // wait for serial port to connect. Needed for Leonardo only
    }

    //ADD_DEBUG_LOGGER();
    Serial.println("Waiting for WiFi module to initialize...");

    // check for the presence of the shield:
    if (WiFi.status() == WL_NO_SHIELD) {
        Serial.println("WiFi shield not present");
        // don't continue:
        while (true);
    }

    String fv = WiFi.firmwareVersion();
    Serial.print("Firmware version:");
    Serial.println(fv);

    // attempt to connect to Wifi network:
    while ( status != WL_CONNECTED) {
        Serial.print("Attempting to connect to SSID: ");
        Serial.println(ssid);
        // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
        status = WiFi.begin(ssid, pass);

        if(status != WL_CONNECTED)
        {
            // wait 10 seconds for connection:
            delay(10000);
        }
    }
    // you're connected now, so print out the status:
    printWifiStatus();
}

void loop() {
    // if there's incoming data from the net connection.
    // send it out the serial port.  This is for debugging
    // purposes only:

    while (client.available()) {
        char c = client.read();
        Serial.write(c);
    }

    // if ten seconds have passed since your last connection,
    // then connect again and send data:
    if (millis() - lastConnectionTime > postingInterval)
    {
        httpRequest();
    }

}

// this method makes a HTTP connection to the server:
void httpRequest() {
    // close any connection before send a new request.
    // This will free the socket on the WiFi shield
    client.stop();

    // if there's a successful connection:
    Serial.println("connecting...");
    if (client.connect(server, 80)) {
        // send the HTTP PUT request:
        client.println("GET /latest.txt HTTP/1.1");
        client.print("Host: ");
        client.println(server);
        client.println("User-Agent: ArduinoWiFi/1.1");
        client.println("Connection: close");
        client.println();

        // note the time that the connection was made:
        lastConnectionTime = millis();
    }
    else {
        // if you couldn't make a connection:
        Serial.println("connection failed");
    }
}


void printWifiStatus() {
    // print the SSID of the network you're attached to:
    Serial.print("SSID: ");
    Serial.println(WiFi.SSID());

    // print your WiFi shield's IP address:
    IPAddress ip = WiFi.localIP();
    Serial.print("IP Address: ");
    Serial.println(ip);

    // print the received signal strength:
    long rssi = WiFi.RSSI();
    Serial.print("signal strength (RSSI):");
    Serial.print(rssi);
    Serial.println(" dBm");
}

