/*
 * Copyright (C) 2014, ACKme Networks, Inc. http://ack.me
 * All Rights Reserved.
 * Version: 7b472aa
 *
 * This software may be used and modified by customers of ACKme Networks.
 * It is provided on an AS-IS basis. The software may only be used in
 * conjunction with ACKme products and all code derivatives must include
 * this header.
 *
 * This and other reference materials may be found at http://ack.me
 *
 * SUMMARY
 *   This demo is a wireless display that can be updated remotely
 *   using a web browser on your phone, tablet or PC connected
 *   to http://goHACK.me
 *
 * HARDWARE
 *   Wi-Fi
 *      AWM006-A02 Mantis board from ACKme Networks
 *      User Guide:
 *      OR
 *      AMW004-E03 Mackerel board from ACKme Networks
 *      User Guide: http://ack.me/resources/download/231/AUG_MW004E
 *      OR
 *      Any other ACKme WiFi module will work as well
 *
 *   Display:
 *      1.2" 8x8 Matrix LED from Adafruit
 *      Install: https://learn.adafruit.com/adafruit-led-backpack/1-2-8x8-matrix
 *
 *   Arduino:
 *      Mega2560 (other Arduinos ok too, but must be 3.3V)
 *
 * DESCRIPTION
 *   The app first checks if the ACKme board is activated with
 *   http://goHACK.me. If not, using the specified username and password,
 *   it activates the module.
 *   The app then connects to goHACK.me via Wi-Fi and retrieves messages
 *   and control information such as:
 *     - Text rotation
 *     - Text Blink
 *     - Display Brightness
 *     - Display On/Off
 *
 * NOTE:
 *   Grab the Adafruit LED backpack library from
 *   the Adafruit website (link above), program the Arduino, and
 *   connect the ACKme board's UART to the Arduino Serial interface.
 *
 *
 */


#define NETWORK_NAME      "<YourNetworkName>"
#define NETWORK_PASSWORD  "<YourNetworkPassword>"

// goto http://gohack.me to register
#define GHM_USERNAME "<YourGoHACK.meName>"
#define GHM_PASSWORD "<YourGoHACK.mePassword>"

#define WEBPAGE    "http://finance.yahoo.com/d/quotes.csv?s="
#define WEB_FORMAT "&f=nol1"
#define STOCK_SYMBOL "BRCM"


/*--------------------------------------------------------------------------------------
 Includes
 --------------------------------------------------------------------------------------*/
#include <Wiconnect.h>
#include <Wire.h>
#include "Adafruit_GFX.h"
#include "target_config.h" // this is needed for platform specific configuration
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>


#define SOLID_MODE   0
#define FLASH_MODE   1
#define FILL_MODE    2
#define CHASE_MODE   3
#define RAINBOW_MODE 4
#define STOCK_MODE   5
#define MESSAGE_MODE 6


// function proto-types
void update_display();
void ghm_poll();
void processMessage();
void processStockData();
void colorWipe(uint32_t c, uint8_t wait);
void ScrollText(char message[20]);
void flash(uint32_t c, uint8_t wait);
void rainbow(uint8_t wait);
void theaterChase(uint32_t c, uint8_t wait);
uint32_t Wheel(byte WheelPos);




/*--------------------------------------------------------------------------------------
 Display Definition
 --------------------------------------------------------------------------------------*/
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(8, 8, 3, 1, DISPLAY_PIN,
  NEO_TILE_TOP   + NEO_TILE_RIGHT   + NEO_TILE_ROWS   + NEO_TILE_PROGRESSIVE +
  NEO_MATRIX_TOP     + NEO_MATRIX_RIGHT +
  NEO_MATRIX_COLUMNS + NEO_MATRIX_PROGRESSIVE,
  NEO_GRB            + NEO_KHZ800);
  
Adafruit_NeoPixel strip = Adafruit_NeoPixel(192, DISPLAY_PIN, NEO_GRB + NEO_KHZ800);

/*--------------------------------------------------------------------------------------
 Serial Interface Definitions
 --------------------------------------------------------------------------------------*/
#define LOG_SERIAL       Serial
#define LOG_PRINT(a)     LOG_SERIAL.println(a)
#define LOG_WRITE(a)     LOG_SERIAL.write(a)
#define BAUD_RATE        9600

/*--------------------------------------------------------------------------------------
 Defines
 --------------------------------------------------------------------------------------*/

// goHACK.me caps control slug names
#define GHM_CAPS_ONOFF         "onoff"
#define GHM_CAPS_MODE          "mode"
#define GHM_CAPS_BRIGHTNESS    "brightness"
#define GHM_CAPS_RED           "red"
#define GHM_CAPS_GREEN         "green"
#define GHM_CAPS_BLUE          "blue"

#define GHM_CONTROL_POLL_INTERVAL 5000 //ms
#define GHM_MSG_POLL_INTERVAL 5000 //ms

/*--------------------------------------------------------------------------------------
 Variables
 --------------------------------------------------------------------------------------*/

// Internal buffer for wiconnect library
uint8_t wiconnectInternalBuffer[WICONNECT_INTERNAL_BUFFER_SIZE];
// Instantiate the WiConnect Library
Wiconnect wifi(SerialConfig(WICONNECT_UART, WICONNECT_RTS_PIN, PIN_NC), sizeof(wiconnectInternalBuffer), wiconnectInternalBuffer, WICONNECT_RESET_PIN);


// Globals controlling the matrix
char   msgBuffer[20]   = { 0 };
int    msgCursor        = -1;

unsigned long    msgScrollTimestamp = 0;
unsigned long    blinkTimestamp = 0;
unsigned long    ghmPollTimestamp = 0;


char stockUrlBuffer[100] = {  WEBPAGE""STOCK_SYMBOL""WEB_FORMAT };
char stockDataBuffer[128] = { 0 };


typedef enum
{
    GHM_STATE_WAIT_FOR_TIMEOUT,
    GHM_STATE_GET_STOCK,
    GHM_STATE_SYNC,
    GHM_STATE_READ_CONTROLS,
    GHM_STATE_GET_MSG_LIST,
    GHM_STATE_OPEN_MSG,
    GHM_STATE_READ_MSG,
} ghmPollStateType;
ghmPollStateType ghmPollState = GHM_STATE_WAIT_FOR_TIMEOUT;

// note that these object must be statically allocated to support the API's non-blocking nature
char msgListBuffer[sizeof(GhmMessage)];
GhmMessageList msgList(sizeof(msgListBuffer), msgListBuffer);
WiconnectSocket ghmMsgSocket;
WiconnectSocket httpSocket;

long int count = 0;
char local_display_state = 0;
unsigned long int display_timer = 0;

uint32_t matrix_color = matrix.Color(255,255,255);
uint32_t strip_color = strip.Color(255,255,255);
int brightness = 20;
int red = 255;
int blue = 255;
int green = 255;
char mode = 0;
char message[20];
int msglen;
char onoff = 0;

/*--------------------------------------------------------------------------------------
 setup()
 Called by the Arduino framework once, before the main loop begins
 --------------------------------------------------------------------------------------*/
void setup()
{
    WiconnectResult result;
    bool isActivated;

    matrix.begin();
    matrix.setTextWrap(false);
    matrix.fillScreen(0);
    strcpy(message, "MESSAGE");
    // uncomment to via Wiconnect library debug msgs
    //wifi.setDebugLogger(wiconnectDebugLogger);

    // Setup WiConnect serial ports
    LOG_SERIAL.begin(BAUD_RATE);
    LOG_PRINT("Booting the goHACK.me Display Demo...");


    // The WiConnect API should be blocking while we're setting up
    wifi.setBlockingEnabled(true);

    init_start:
    // Initialize WiConnect library communication with WiFi module
    if(WICONNECT_FAILED(result, wifi.init()))
    {
        LOG_PRINT("Failed to initialize communications with WiFi module. Is it connected correctly?");
        for(;;); // loop forever
    }

    // check if flow control is enabled
    char *val;
    wifi.getSetting("uart.flow 0", &val);
    if(strcmp(val, "off") == 0)
    {
        wifi.setSetting("uart.flow", "0 on");
        wifi.saveSettings();
        wifi.reset();
        goto init_start;
    }

    // attempt to join WiFi network
    while(WICONNECT_FAILED(result, wifi.join(NETWORK_NAME, NETWORK_PASSWORD)))
    {
        LOG_PRINT("Failed to join Wi-Fi network. Are your credentials correct?");
        delay(5000);
        LOG_PRINT("Retrying");
    }

    // Is the WiFi module activated with http://goHACK.me?
    wifi.ghmIsActivated(&isActivated);
    if(!isActivated)
    {
        LOG_PRINT("Activating module with http://goHACK.me");
        // We're not activated, attempt to activate now.
        // first download the LED Matrix capabilities file from the ACKme server
        //if(WICONNECT_FAILED(result, wifi.ghmDownloadCapabilities("arduino_ledmatrix")))
        //{
        //    LOG_PRINT("Failed to download ACKme LED Matrix capabilities file");
        //    for(;;); // loop forever
        //}
        // now attempt to activate the module with http://goHACK.me
        while(WICONNECT_FAILED(result, wifi.ghmActivate(GHM_USERNAME, GHM_PASSWORD, "ghm_display_demo.json")))
        {
            LOG_PRINT("Failed to activate module with http://goHACK.me\r\nIs your username and password correct?");
            delay(5000); // loop forever
            LOG_PRINT("Retrying");
        }

    }
    
    LOG_PRINT("Ready. Login to http://goHACK.me to control the LED matrix.");
}

/*--------------------------------------------------------------------------------------
 Arduino main loop
 --------------------------------------------------------------------------------------*/
void loop()
{
    update_display();
    ghm_poll();
}

/*--------------------------------------------------------------------------------------
 LED Matrix: Update the display
 --------------------------------------------------------------------------------------*/
void update_display()
{
  if (onoff == 1){
    switch (mode) {
      case SOLID_MODE:
        matrix.fillScreen(matrix_color);
        matrix.show();
        break;
      case FILL_MODE:
        colorWipe(strip_color, 50);
        break;
      case MESSAGE_MODE:
        ScrollText(message);
        break;
      case STOCK_MODE:
        // this is handled in ghm_poll()
        break;
      case CHASE_MODE:
        theaterChase(strip_color, 50);
        break;
      case RAINBOW_MODE:
        rainbow(10);
        break;
      case FLASH_MODE:
        flash(matrix_color, 50);
        break;
      default:
        mode = FILL_MODE;
        break;
    }
  } else
  {
    strip.clear();
    strip.show();
  }
}


/*--------------------------------------------------------------------------------------
 Poll http://goHACK.me for control updates
 --------------------------------------------------------------------------------------*/
void ghm_poll()
{
    WiconnectResult result;

    // wait until the polling period times out
    if(ghmPollState == GHM_STATE_WAIT_FOR_TIMEOUT)
    {
        // only poll at specified intervals
        const unsigned long now = millis();
        if((now - ghmPollTimestamp) < GHM_CONTROL_POLL_INTERVAL)
        {
            // still haven't timed-out, just return
            return;
        }
        // we timed-out
        ghmPollTimestamp = now;

        // this command can take awhile to complete so enable non-blocking mode
        wifi.setBlockingEnabled(false);

        if(mode == STOCK_MODE)
        {
            ghmPollState = GHM_STATE_GET_STOCK;
            LOG_SERIAL.print("Issuing HTTP Request: ");
            LOG_SERIAL.println(stockUrlBuffer);
        }
        else
        {
            // set state to synchronize with http://goHACK.me
            ghmPollState = GHM_STATE_SYNC;
            LOG_SERIAL.println("Syncing");
        }
    }

    if(ghmPollState == GHM_STATE_GET_STOCK)
    {
        // continuously call until the api call completes
        if(!WICONNECT_IS_PROCESSING(result, wifi.httpGet(httpSocket, stockUrlBuffer)))
        {
            if(result == WICONNECT_SUCCESS)
            {
                uint16_t bytesRead;
                char *ptr = stockDataBuffer;
                wifi.setBlockingEnabled(true);

                *ptr = 0;
                loop:
                if(httpSocket.read(ptr, (uint16_t)(&stockDataBuffer[sizeof(stockDataBuffer)] - ptr), &bytesRead) == WICONNECT_SUCCESS)
                {
                    for(; (signed)(bytesRead--) > 0; ++ptr)
                    {
                        if(*ptr == '\r')
                        {
                            *ptr = 0;
                            goto close_socket;
                        }
                    }
                    goto loop;
                }
                close_socket:
                httpSocket.close();
                processStockData();
            }
            else
            {
                LOG_SERIAL.println("Failed to retrieve stock data");
            }

            wifi.setBlockingEnabled(false);
            ghmPollState = GHM_STATE_SYNC;
        }
    }

    if(ghmPollState == GHM_STATE_SYNC)
    {
        //LOG_SERIAL.print("-");
        // continuously call until the api call completes
        if(!WICONNECT_IS_PROCESSING(result, wifi.ghmSynchronize()))
        {         
          LOG_SERIAL.println("Done Syncing");
            // the call has completed, read the control values
            ghmPollState = GHM_STATE_READ_CONTROLS;
            // these calls are fast so lets re-enabled blocking mode (to make our lives easier ;)
            wifi.setBlockingEnabled(true);
        }
    }
    if(ghmPollState == GHM_STATE_READ_CONTROLS)
    {
        LOG_SERIAL.println("Reading");
        uint32_t newval;
        bool valueChanged = false;

        // read the onoff control
        wifi.ghmRead(GHM_CAPS_ONOFF, &newval);
        if((int)newval != onoff)
        {
            valueChanged = true;
            onoff = newval;
        }

        // read the brightness control
        wifi.ghmRead(GHM_CAPS_BRIGHTNESS, &newval);
        if((int)newval != brightness)
        {
            valueChanged = true;
            brightness = newval;
            matrix.setBrightness(brightness);
            strip.setBrightness(brightness);
        }

        // read the red control
        wifi.ghmRead(GHM_CAPS_RED, &newval);
        if((int)newval != red)
        {
            valueChanged = true;
            red = newval;
            matrix_color = matrix.Color(red, green, blue);
            strip_color = strip.Color(red, green, blue);
        }
        // read the green control
        wifi.ghmRead(GHM_CAPS_GREEN, &newval);
        if((int)newval != green)
        {
            valueChanged = true;
            green = newval;
            matrix_color = matrix.Color(red, green, blue);
            strip_color = strip.Color(red, green, blue);
        }
        // read the blue control
        wifi.ghmRead(GHM_CAPS_BLUE, &newval);
        if((int)newval != blue)
        {
            valueChanged = true;
            blue = newval;
            matrix_color = matrix.Color(red, green, blue);
            strip_color = strip.Color(red, green, blue);
        }
         
        // read the mode control
        wifi.ghmRead(GHM_CAPS_MODE, &newval);
        if((int)newval != mode)
        {
            valueChanged = true;
            mode = newval;
            local_display_state = 0;
            count = 0;
        }

        if(valueChanged)
        {
            LOG_SERIAL.println("------------------");
            LOG_SERIAL.print("On/off state: ");
            LOG_SERIAL.println(onoff, DEC);
            LOG_SERIAL.print("Brightness: ");
            LOG_SERIAL.println(brightness, DEC);
            LOG_SERIAL.print("RED: ");
            LOG_SERIAL.println(red, DEC);
            LOG_SERIAL.print("GREEN: ");
            LOG_SERIAL.println(green, DEC);
            LOG_SERIAL.print("BLUE: ");
            LOG_SERIAL.println(blue, DEC);
            LOG_SERIAL.print("Mode: ");
            LOG_SERIAL.println(mode, DEC);
            LOG_SERIAL.print("Matrix Color: ");
            LOG_SERIAL.println(matrix_color, HEX);
            LOG_SERIAL.print("Strip Color: ");
            LOG_SERIAL.println(strip_color, HEX);
        }

        // now retreive a message list
        ghmPollState = GHM_STATE_WAIT_FOR_TIMEOUT;
        // this command can take awhile to complete so enable non-blocking mode
        wifi.setBlockingEnabled(false);
        LOG_SERIAL.println("Getting Message List");
        ghmPollState = GHM_STATE_GET_MSG_LIST;
    }
    if(ghmPollState == GHM_STATE_GET_MSG_LIST)
    {
        // continuously call until the api call completes
        if(!WICONNECT_IS_PROCESSING(result, wifi.ghmListMessages(msgList, 1)))
        {
            LOG_SERIAL.println("Message list received");

            // the call has completed, is a msg available?
            if(msgList.getCount() > 0)
            {
                LOG_SERIAL.println("Getting Message");
                // yes, open the msg for reading
                ghmPollState = GHM_STATE_OPEN_MSG;
            }
            else
            {
                // no, wait for the next poll timeout
                ghmPollState = GHM_STATE_WAIT_FOR_TIMEOUT;
            }
        }
    }
    if(ghmPollState == GHM_STATE_OPEN_MSG)
    {
        // continuously call until the api call completes
        if(!WICONNECT_IS_PROCESSING(result, wifi.ghmGetMessage(ghmMsgSocket)))
        {
            LOG_SERIAL.println("Message received");

            // the call has completed, go to the read msg state
            ghmPollState = GHM_STATE_READ_MSG;
        }
    }
    if(ghmPollState == GHM_STATE_READ_MSG)
    {
        uint16_t bytesRead = 0;
        // continuously call until the api call completes
        if(!WICONNECT_IS_PROCESSING(result, ghmMsgSocket.read(msgBuffer, sizeof(msgBuffer)-1, &bytesRead)))
        {
            // the call has completed
            // null-terminate the string
            msgBuffer[bytesRead+1] = 0;
            // process the message
            processMessage();

            // cleanup the socket
            // enabled blocking mode again to make our lives easier
            // (the calls execute fairly fast anyways)
            wifi.setBlockingEnabled(true);
            // close the ghm msg socket
            ghmMsgSocket.close();
            // delete the msg from the server
            wifi.ghmDeleteMessage((uint8_t)0);

            // wait for the next timeout
            ghmPollState = GHM_STATE_WAIT_FOR_TIMEOUT;
        }
    }
}
/*--------------------------------------------------------------------------------------
 Process the received message
 --------------------------------------------------------------------------------------*/
void processMessage()
{
    strcpy(message, msgBuffer);
    LOG_SERIAL.print("*** New msg: ");
    LOG_SERIAL.println(msgBuffer);

    if(*msgBuffer == '#')
    {
        strcpy(stockUrlBuffer, WEBPAGE);
        strcat(stockUrlBuffer, &msgBuffer[1]);
        strcat(stockUrlBuffer, WEB_FORMAT);

        LOG_SERIAL.print("Updated stock URL to: ");
        LOG_SERIAL.println(stockUrlBuffer);
    }
}

/*--------------------------------------------------------------------------------------
 Process stock data
 --------------------------------------------------------------------------------------*/
void processStockData()
{
    if(*stockDataBuffer != 0)
    {
        LOG_SERIAL.print("New stock data: ");
        LOG_SERIAL.println(stockDataBuffer);
    }
}

void ScrollText(char message[20])
{
  switch(local_display_state)
  {
    case (0):
      matrix.setBrightness(brightness);  
      matrix.setTextColor(matrix_color);      
      count = matrix.width();   
       
      msglen = 0 - (String(message).length()*6 + count);
      matrix.fillScreen(0);
      local_display_state = 1;
      display_timer = 0;
      break;
    case(1):
        if (millis() > (display_timer + 100))
        {
          matrix.fillScreen(0);
          matrix.setCursor(count, 0);
          matrix.print(message);
          matrix.show();
          display_timer = millis();          
          if (--count < msglen)
          {
            msglen = 0 - (String(message).length()*6);
            count = matrix.width();    
          }
        }
        break;
  }
}

void flash(uint32_t c, uint8_t wait){
  switch(local_display_state)
  {
    case (0):
      display_timer = 0;
      matrix.fillScreen(c);    
      matrix.show();
      display_timer = millis();  
      local_display_state = 1;     
      break; 
    case (1):
      if (millis() > (display_timer + wait))
      {
        matrix.fillScreen(c);    
        matrix.show();
        display_timer = millis();  
        local_display_state = 2;      
      }
      break;
    case (2):
      if (millis() > (display_timer + wait))
      {
        matrix.fillScreen(0);    
        matrix.show();
        display_timer = millis();  
        local_display_state = 1;      
      }
      break;
  }
}  


// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  switch(local_display_state)
  {
    case (0):
      strip.setBrightness(brightness); 
      matrix.fillScreen(0); 
      strip.show();
      local_display_state = 1;
      count = 0;
      display_timer = 0;
      break;
    case (1):
      if (millis() > (display_timer + wait))
      {      
        strip.setPixelColor(count, c);
        strip.show();
        display_timer = millis();          
        if (count++ > strip.numPixels()){
          count = 0;
          strip.clear();
          strip.show();
        }
      }
      break;
  }
}

void rainbow(uint8_t wait) {
  switch(local_display_state)
  {
    case (0):
      strip.setBrightness(brightness);
      count=0;
      local_display_state = 1;
      display_timer = 0;
      break;
    case (1):
      if (millis() > (display_timer + wait))
      {      
        for(int i=0; i<strip.numPixels(); i++) {
          strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + count) & 255));
        }
        strip.show();
        display_timer = millis();          
        if(count++ == 256){
          count = 0;
        } 
      }
  }
}

//Theatre-style crawling lights.
void theaterChase(uint32_t c, uint8_t wait) {
  switch(local_display_state)
  {
    case (0):
      strip.setBrightness(brightness);
      count=0;
      local_display_state = 1;
      display_timer = 0;
      break;
    case(1):
      for (int q=0; q < 3; q++) {
        for (int i=0; i < strip.numPixels(); i=i+3) {
          strip.setPixelColor(i+q, c);    //turn every third pixel on
        }
        strip.show();
       
        delay(wait);
       
        for (int i=0; i < strip.numPixels(); i=i+3) {
          strip.setPixelColor(i+q, 0);        //turn every third pixel off
        }
      }
      break;
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
   return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else if(WheelPos < 170) {
    WheelPos -= 85;
   return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  } else {
   WheelPos -= 170;
   return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  }
}
