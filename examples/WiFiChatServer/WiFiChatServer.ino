/*
 Chat  Server

 A simple server that distributes any incoming messages to all
 connected clients.  To use telnet to  your device's IP address and type.
 You can see the client's input in the serial monitor as well.

 This example is written for a network using WPA encryption. For
 WEP or WPA, change the Wifi.begin() call accordingly.


 Circuit:
 * WiFi shield attached

 created 18 Dec 2009
 by David A. Mellis
 modified 31 May 2012
 by Tom Igoe

 */

#include <Wiconnect.h>

#include "target_config.h" // this is needed for platform specific configuration


char ssid[] = "yourNetwork"; //  your network SSID (name)
char pass[] = "secretPassword";    // your network password (use for WPA)

void printWifiStatus();


SerialConfig serialConfig(WICONNECT_UART);
uint8_t wiconnectInternalBuffer[WICONNECT_INTERNAL_BUFFER_SIZE];
Wiconnect wiconnectIfc(serialConfig, sizeof(wiconnectInternalBuffer), wiconnectInternalBuffer, WICONNECT_RESET_PIN); // NOTE: this MUST be initialized first!!

WiFiServer server(23);
WiFiClient client;


int status = WL_IDLE_STATUS;
boolean alreadyConnected = false; // whether or not the client was connected previously

void setup() {
    //Initialize serial and wait for port to open:
    Serial.begin(CONSOLE_BAUD);
    while (!Serial) {
        ; // wait for serial port to connect. Needed for Leonardo only
    }
    //ADD_DEBUG_LOGGER();
    Serial.println("Waiting for WiFi module to initialize...");

    // check for the presence of the shield:
    if (WiFi.status() == WL_NO_SHIELD) {
        Serial.println("WiFi shield not present");
        // don't continue:
        while(true);
    }

    String fv = WiFi.firmwareVersion();
    Serial.print("Firmware version:");
    Serial.println(fv);

    // attempt to connect to Wifi network:
    while ( status != WL_CONNECTED) {
        Serial.print("Attempting to connect to SSID: ");
        Serial.println(ssid);
        // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
        status = WiFi.begin(ssid, pass);

        if(status != WL_CONNECTED)
        {
            // wait 10 seconds for connection:
            delay(10000);
        }
    }

    // you're connected now, so print out the status:
    printWifiStatus();

    // start the server:
    server.begin();
    Serial.print("Echo TCP server listening on ");
    Serial.println(server.getListeningPort());
}



void loop() {
    // wait for a new client if necessary:
    server.available(client);


    // when the client sends the first byte, say hello:
    if (client) {
        if (!alreadyConnected) {
            // clead out the input buffer:
            client.flush();
            Serial.println("We have a new client");
            client.println("Hello, client!");
            alreadyConnected = true;
        }

        if (client.available(true) > 0) {
            // read the bytes incoming from the client:
            char thisChar = client.read();
            // echo the bytes back to the client:
            server.write(thisChar);
            // echo the bytes to the server as well:
            Serial.write(thisChar);
        }

        if(alreadyConnected && !client.connected())
        {
            alreadyConnected = false;
            Serial.println("Client disconnected");
            Serial.println("Waiting for next client...");
        }
    }
}


void printWifiStatus() {
    // print the SSID of the network you're attached to:
    Serial.print("SSID: ");
    Serial.println(WiFi.SSID());

    // print your WiFi shield's IP address:
    IPAddress ip = WiFi.localIP();
    Serial.print("IP Address: ");
    Serial.println(ip);

    // print the received signal strength:
    long rssi = WiFi.RSSI();
    Serial.print("signal strength (RSSI):");
    Serial.print(rssi);
    Serial.println(" dBm");
}

