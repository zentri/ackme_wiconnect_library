/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */

#include "internal/CommandCommon.h"
#include "api/StringUtil.h"



/*************************************************************************************************/
void Wiconnect::setCommandDefaultTimeout(TimerTimeout timeoutMs)
{
    defaultTimeoutMs = timeoutMs;
}

/*************************************************************************************************/
TimerTimeout Wiconnect::getCommandDefaultTimeout()
{
    return defaultTimeoutMs;
}

/*************************************************************************************************/
void Wiconnect::setBlockingEnabled(bool blockingEnabled)
{
    nonBlocking = !blockingEnabled;
}

/*************************************************************************************************/
bool Wiconnect::getBlockingEnabled(void)
{
    return !nonBlocking;
}

/*************************************************************************************************/
char* Wiconnect::getResponseBuffer()
{
    return internalBuffer;
}

/*************************************************************************************************/
uint16_t Wiconnect::getLastCommandResponseLength()
{
    CommandHeader *header = (CommandHeader*)commandHeaderBuffer;
    return header->response_len;
}

/*************************************************************************************************/
const char* Wiconnect::getLastCommandResponseCodeStr()
{
    if(!initialized)
    {
        return NULL;
    }
    static const char* const response_error_strings[] ={
            "Null",
            "Success",
            "Failed",
            "Parse error",
            "Unknown command",
            "Too few arguments",
            "Too many arguments",
            "Unknown command option",
            "Bad command arguments"
    };
    CommandHeader *header = (CommandHeader*)commandHeaderBuffer;

    return response_error_strings[header->response_code];
}


/*************************************************************************************************/
WiconnectResult Wiconnect::responseToUint32(uint32_t *uint32Ptr)
{
    CHECK_INITIALIZED();
    return StringUtil::strToUint32(internalBuffer, uint32Ptr) ? WICONNECT_SUCCESS : WICONNECT_RESPONSE_PARSE_ERROR;
}


/*************************************************************************************************/
WiconnectResult Wiconnect::responseToInt32(int32_t *int32Ptr)
{
    CHECK_INITIALIZED();
    return StringUtil::strToInt32(internalBuffer, int32Ptr) ? WICONNECT_SUCCESS : WICONNECT_RESPONSE_PARSE_ERROR;
}


