# README #

The WiConnect Library runs on a host MCU and controls a WiConnect enabled WiFi module.

* [Library Documentation](http://wiconnect.ack.me/arduino)
* [WiConnect Documentation](http://wiconnect.ack.me/2.0/general_overview)
* [WiConnect Information ](https://www.ack.me/WiConnect)