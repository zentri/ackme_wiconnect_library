/**
 * ACKme WiConnect Host Library is licensed under the BSD licence:
 *
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 */
#include <stdarg.h>

#include "Wiconnect.h"
#include "internal/common.h"
#include "api/StringUtil.h"


#define CHECK_CONNECTED() if(!isConnected()) return WICONNECT_NOT_CONNECTED

extern WiconnectResult readerCallback(void *user, void *data, int maxReadSize, int *bytesRead);


/*************************************************************************************************/
WiconnectUdpServer::WiconnectUdpServer(int rxBufferLen_, void *rxBuffer_, int txBufferLen_, void *txBuffer_) :
WiconnectSocket(rxBufferLen_, rxBuffer_, txBufferLen_, txBuffer_)
{
}

/*************************************************************************************************/
WiconnectResult WiconnectUdpServer::getRemoteClient(uint32_t *ipAddress, uint16_t *port)
{
    WiconnectResult result;

    CHECK_CONNECTED();
    CHECK_OTHER_COMMAND_EXECUTING();

    if(WICONNECT_SUCCEEDED(result, wiconnect->sendCommand("udps read -q")))
    {
        char *portStr = strchr(wiconnect->internalBuffer, ',');
        if(portStr == NULL)
        {
            result = WICONNECT_RESPONSE_PARSE_ERROR;
        }
        else
        {
            *portStr++ = 0;
            Wiconnect::strToIp(wiconnect->internalBuffer, ipAddress);
            StringUtil::strToUint16(portStr, port);
        }
    }

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult WiconnectUdpServer::setRemoteClient(const char* _host, uint16_t port)
{
    WiconnectResult result;
    uint32_t ip;

    if(WICONNECT_SUCCEEDED(result, wiconnect->lookup(_host, &ip)))
    {
        ip = htonl(ip); // due to a bug in the firmware, need to reserve byte order
        Wiconnect::ipToStr(ip, host);
        remotePort = port;
    }

    return result;
}

/*************************************************************************************************/
WiconnectResult WiconnectUdpServer::setRemoteClient(uint32_t ip, uint16_t port)
{
    ip = htonl(ip); // due to a bug in the firmware, need to reserve byte order
    Wiconnect::ipToStr(ip, host);
    remotePort = port;
    return WICONNECT_SUCCESS;
}

/*************************************************************************************************/
WiconnectResult WiconnectUdpServer::flushTxBuffer()
{
    WiconnectResult result = WICONNECT_SUCCESS;

    CHECK_CONNECTED();

    if(txBuffer.size == 0)
    {
        CHECK_OTHER_COMMAND_EXECUTING();
    }


    if(txBuffer.bytesPending > 0)
    {
        loop:
        if(host[0] != 0)
        {
            result = wiconnect->sendCommand(ReaderFunc(readerCallback), &this->txBuffer, "udps write %u %s %u", txBuffer.bytesPending, host, remotePort);
        }
        else
        {
            result = wiconnect->sendCommand(ReaderFunc(readerCallback), &this->txBuffer, "udps write %u", txBuffer.bytesPending);
        }

        // if still processing and in non-blocking mode and using a txtBuffer,
        // then this api call must block until all the data is sent
        if(result == WICONNECT_PROCESSING && wiconnect->nonBlocking && txBuffer.size > 0)
        {
            goto loop;
        }
    }

    if(txBuffer.size == 0)
    {
        CHECK_CLEANUP_COMMAND();
    }

    if(result != WICONNECT_PROCESSING)
    {
        txBuffer.ptr = txBuffer.buffer;
        txBuffer.bytesPending = 0;
    }

    return result;
}
