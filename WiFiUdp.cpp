
#include <string.h>

#include "api/types/WiFi/WiFi.h"
#include "api/types/WiFi/WiFiUdp.h"
#include "Wiconnect.h"

extern Wiconnect *driver;


/* Constructor */
/*************************************************************************************************/
WiFiUDP::WiFiUDP() : _udpServer(sizeof(rxBuffer), rxBuffer, sizeof(txBuffer), txBuffer)
{

}

/*************************************************************************************************/
/* Start WiFiUDP socket, listening at local port PORT */
uint8_t WiFiUDP::begin(uint16_t port)
{
    WiconnectResult result;

    if(driver == NULL)
    {
        return 0;
    }

    while(WICONNECT_IS_PROCESSING(result, driver->udpListen(_udpServer, port)))
    {
    }

    return (result == WICONNECT_SUCCESS) ? 1 : 0;
}

/*************************************************************************************************/
/* return number of bytes available in the current packet,
   will return zero if parsePacket hasn't been called yet */
int WiFiUDP::available()
{
    WiconnectResult result;
    bool dataAvailable;
    while(WICONNECT_IS_PROCESSING(result, _udpServer.poll(&dataAvailable)))
    {
    }
    return dataAvailable;
}

/*************************************************************************************************/
/* Release any resources being used by this WiFiUDP instance */
void WiFiUDP::stop()
{
    WiconnectResult result;

    while(WICONNECT_IS_PROCESSING(result, _udpServer.close()))
    {
    }
}

/*************************************************************************************************/
int WiFiUDP::beginPacket(const char *host, uint16_t port)
{
    WiconnectResult result;

    while(WICONNECT_IS_PROCESSING(result, _udpServer.setRemoteClient(host, port)))
    {
    }
    return (result == WICONNECT_SUCCESS) ? 1 : 0;
}

/*************************************************************************************************/
int WiFiUDP::beginPacket(IPAddress ip, uint16_t port)
{
    _udpServer.setRemoteClient((uint32_t)ip, port);
    return 1;
}

/*************************************************************************************************/
int WiFiUDP::endPacket()
{
    WiconnectResult result = _udpServer.flushTxBuffer();
    return (result == WICONNECT_SUCCESS) ? 1 : 0;
}

/*************************************************************************************************/
size_t WiFiUDP::write(uint8_t byte)
{
    return write(&byte, 1);
}

/*************************************************************************************************/
size_t WiFiUDP::write(const uint8_t *buffer, size_t size)
{
    WiconnectResult result = _udpServer.write(buffer, size, false);
    return (result == WICONNECT_SUCCESS) ? size : 0;
}

/*************************************************************************************************/
int WiFiUDP::parsePacket()
{
    return available();
}

/*************************************************************************************************/
int WiFiUDP::read()
{
    if (available())
    {
        uint8_t b;
        WiconnectResult result = _udpServer.getc(&b);
        return (result == WICONNECT_SUCCESS) ? b : -1;
    }
    else
    {
        return -1;
    }
}

/*************************************************************************************************/
int WiFiUDP::read(unsigned char* buffer, size_t len)
{
    if (available())
    {
        WiconnectResult result;
        uint16_t bytesRead;
        while(WICONNECT_IS_PROCESSING(result, _udpServer.read(buffer, len, &bytesRead)))
        {
        }

        return (result == WICONNECT_SUCCESS) ? bytesRead : -1;
    }
    else
    {
        return -1;
    }
}

/*************************************************************************************************/
int WiFiUDP::peek()
{
    // TODO: support this call
    return -1;
}

/*************************************************************************************************/
void WiFiUDP::flush()
{
    while (available())
        read();
}

/*************************************************************************************************/
IPAddress  WiFiUDP::remoteIP()
{
    WiconnectResult result;
    uint32_t ip = 0;
    uint16_t port = 0;

    while(WICONNECT_IS_PROCESSING(result, _udpServer.getRemoteClient(&ip, &port)))
    {
    }

    return (IPAddress)ip;
}

/*************************************************************************************************/
uint16_t  WiFiUDP::remotePort()
{
    WiconnectResult result;
    uint32_t ip = 0;
    uint16_t port = 0;

    while(WICONNECT_IS_PROCESSING(result, _udpServer.getRemoteClient(&ip, &port)))
    {
    }

    return port;
}

