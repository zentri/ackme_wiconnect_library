/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#include "Wiconnect.h"
#include "internal/common.h"



/*************************************************************************************************/
WiconnectResult NetworkInterface::startWebSetup(const char *ssid, const char *password, const Callback &completeHandler_)
{
    WiconnectResult result = WICONNECT_ERROR;;

    enum
    {
        FS_SET_SSID,
        FS_SET_PASSWORD,
        FS_NETWORK_UP
    };

    CHECK_CALLBACK_AVAILABLE(completeHandler_);
    CHECK_OTHER_COMMAND_EXECUTING();

    if(wiconnect->internalProcessingState == FS_SET_SSID)
    {
        if(ssid == NULL ||
           WICONNECT_SUCCEEDED(result, wiconnect->sendCommand("set setup.web.ssid %s", ssid)))
        {
            wiconnect->internalProcessingState = FS_SET_PASSWORD;
        }
    }

    if(wiconnect->internalProcessingState == FS_SET_PASSWORD)
    {
        if(password == NULL ||
           WICONNECT_SUCCEEDED(result, wiconnect->sendCommand("set setup.web.passkey %s", password)))
        {
            wiconnect->internalProcessingState = FS_NETWORK_UP;
        }
    }

    if(wiconnect->internalProcessingState == FS_NETWORK_UP)
    {
        if(WICONNECT_SUCCEEDED(result, wiconnect->sendCommand("setup web")))
        {
#ifdef WICONNECT_ASYNC_TIMER_ENABLED
            if(completeHandler_.isValid())
            {
#ifdef WICONNECT_ASYNC_TIMER_ENABLED
                monitorTimer.stop();
#endif
                completeHandler = completeHandler_;
                monitorTimer.start(this, &NetworkInterface::webSetupStatusMonitor, 1000);
            }
#endif
        }
    }

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult NetworkInterface::stopWebSetup()
{
    WiconnectResult result;

    CHECK_OTHER_COMMAND_EXECUTING();

    #ifdef WICONNECT_ASYNC_TIMER_ENABLED
        monitorTimer.stop();
    #endif
    result = wiconnect->sendCommand("setup stop");

    CHECK_CLEANUP_COMMAND();

    return result;
}


/*************************************************************************************************/
WiconnectResult NetworkInterface::isWebSetupRunning(bool *isRunningPtr)
{
    WiconnectResult result;

    CHECK_OTHER_COMMAND_EXECUTING();

    if(WICONNECT_SUCCEEDED(result, wiconnect->sendCommand("setup status")))
    {
        int32_t status;
        if(!WICONNECT_FAILED(result, wiconnect->responseToInt32(&status)))
        {
            if(status)
            {
#ifdef WICONNECT_ASYNC_TIMER_ENABLED
                monitorTimer.stop();
#endif
            }
            *isRunningPtr = (bool)status;
        }
    }

    CHECK_CLEANUP_COMMAND();

    return result;
}


#ifdef WICONNECT_ASYNC_TIMER_ENABLED

/*************************************************************************************************/
void NetworkInterface::webSetupStatusMonitor()
{
    static char responseBuffer[4];
    static uint8_t cmdBuffer[sizeof(QueuedCommand)];
    QueuedCommand *cmd = (QueuedCommand*)cmdBuffer;

    monitorTimer.stop();

    *cmd = QueuedCommand(sizeof(responseBuffer), responseBuffer, "setup status");

    wiconnect->enqueueCommand(cmd, Callback(this, &NetworkInterface::webSetupStatusCheckCallback));
}

/*************************************************************************************************/
void NetworkInterface::webSetupStatusCheckCallback(WiconnectResult result, void *arg1, void *arg2)
{
    bool isComplete = true;

    QueuedCommand *cmd = (QueuedCommand*)arg1;

    if(result == WICONNECT_SUCCESS)
    {
        int32_t status;
        if(!StringUtil::strToInt32(cmd->responseBuffer, &status))
        {
            result = WICONNECT_RESPONSE_PARSE_ERROR;
        }
        else if(status > 0)
        {
            isComplete = false;
        }
    }

    if(isComplete)
    {
        completeHandler.call(result, NULL, NULL);
    }
    else
    {
        monitorTimer.start(this, &NetworkInterface::webSetupStatusMonitor, 1000);
    }
}

#endif
