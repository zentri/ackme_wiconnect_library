/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */

#include "Wiconnect.h"
#include "internal/common.h"

static bool useRTS, useCTS;
static Gpio rts(PIN_NC);
static Gpio cts(PIN_NC);

/*************************************************************************************************/
WiconnectSerial::WiconnectSerial(const SerialConfig  &config, Wiconnect *wiconnect)
{
    serial = config.serial;
    baudRate = config.baud;

    if(config.rtsPin != PIN_NC)
    {
        useRTS = true;
        rts = Gpio(config.rtsPin);
        rts = true;
    }
    if(config.ctsPin != PIN_NC)
    {
        useCTS = true;
        cts = Gpio(config.ctsPin, false);
    }
}

/*************************************************************************************************/
WiconnectSerial::~WiconnectSerial()
{

}

/*************************************************************************************************/
void WiconnectSerial::initialize(void)
{
    serial->begin(baudRate);
}

/*************************************************************************************************/
void WiconnectSerial::flush(void)
{
    char buffer[8];
    serial->flush();

    while(read(buffer, sizeof(buffer), 50) > 0){}
}

/*************************************************************************************************/
int WiconnectSerial::write(const void *data, int bytesToWrite, TimerTimeout timeoutMs)
{
    uint8_t *ptr = (uint8_t*)data;
    int remaining = bytesToWrite;

    while(remaining > 0)
    {
        if(useCTS && cts.read())
            break;
        serial->write((int)*ptr);
        ++ptr;
        --remaining;
    }

    return bytesToWrite - remaining;
}

/*************************************************************************************************/
int WiconnectSerial::read(void *data, int bytesToRead, TimerTimeout timeoutMs)
{
    uint8_t *ptr = (uint8_t*)data;
    int remaining = bytesToRead;

    if(useRTS)
    {
        rts = false;
    }

    while(remaining > 0)
    {
        if(serial->available() == 0)
        {
            if(timeoutMs == 0)
            {
                // wait just a little bit to see if any data comes
                timeoutTimer.reset();
                while(serial->available() == 0)
                {
                    if(!useRTS || timeoutTimer.readMs() >= 10)
                        goto exit;
                }
            }
            else
            {
                timeoutTimer.reset();
                while(serial->available() == 0)
                {
                    if(timeoutTimer.readMs() >= timeoutMs)
                    {
                        goto exit;
                    }
                }
            }
        }

        *ptr++ = (char)serial->read();
        --remaining;
    }

    exit:
    if(useRTS)
    {
        rts = true;
    }

    return bytesToRead - remaining;
}

