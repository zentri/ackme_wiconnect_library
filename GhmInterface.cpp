/**
 * ACKme WiConnect Host Library is licensed under the BSD licence:
 *
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 */

#include "Wiconnect.h"
#include "internal/common.h"
#include "api/StringUtil.h"


/*************************************************************************************************/
GhmInterface::GhmInterface(Wiconnect *wiconnect_)
{
    wiconnect = wiconnect_;
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmDownloadCapabilities(const char *capsNameOrCustomUrl, uint32_t version)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        strcpy(cmdBuffer, "gca download ");
        if(capsNameOrCustomUrl != NULL)
        {
            strcat(cmdBuffer, capsNameOrCustomUrl);
            if(version > 0)
            {
                char temp[16];
                char *ptr = cmdBuffer + strlen(cmdBuffer);
                *ptr++ = ' ';
                sprintf(ptr, "%s", Wiconnect::fileVersionIntToStr(version, true, temp));
            }
        }
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    result = wiconnect->sendCommand(cmdBuffer);

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmActivate(const char *userName, const char *password, const char *capsFilename)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        sprintf(cmdBuffer, "gac %s %s %s", userName, password, (capsFilename == NULL) ? "" : capsFilename);
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    result = wiconnect->sendCommand(cmdBuffer);

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmDeactivate(const char *userName, const char *password)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        sprintf(cmdBuffer, "gac %s %s", userName, password);
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    result = wiconnect->sendCommand(cmdBuffer);

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmIsActivated(bool *statusPtr)
{
    WiconnectResult result;
    uint32_t status;

    if(WICONNECT_SUCCEEDED(result, wiconnect->getSetting("ghm.status", &status)))
    {
        *statusPtr = (bool)status;
    }

    return result;
}


/*************************************************************************************************/
WiconnectResult GhmInterface::ghmRead(const char *controlName, const char **valueStrPtr)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        sprintf(cmdBuffer, "gre %s", controlName);
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    if(WICONNECT_SUCCEEDED(result, wiconnect->sendCommand(cmdBuffer)))
    {
        *valueStrPtr = wiconnect->internalBuffer;
    }

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmRead(const char *controlName, uint32_t *valueIntPtr)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        sprintf(cmdBuffer, "gre %s", controlName);
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    if(WICONNECT_SUCCEEDED(result, wiconnect->sendCommand(cmdBuffer)))
    {
        result = wiconnect->responseToUint32(valueIntPtr);
    }

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmRead(const char *controlName, char *valueBuffer, uint16_t valueBufferLen)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        sprintf(cmdBuffer, "gre %s", controlName);
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    result = wiconnect->sendCommand(valueBuffer, valueBufferLen, cmdBuffer);

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmWrite(const char *elementName, const char *strValue)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        sprintf(cmdBuffer, "gwr %s %s", elementName, strValue);
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    result = wiconnect->sendCommand(cmdBuffer);

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmWrite(const char *elementName, uint32_t uintValue)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        sprintf(cmdBuffer, "gwr %s %lu", elementName, uintValue);
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    result = wiconnect->sendCommand(cmdBuffer);

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmWrite(const char *elementName, int32_t intValue)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        sprintf(cmdBuffer, "gwr %s %ld", elementName, intValue);
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    result = wiconnect->sendCommand(cmdBuffer);

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmWrite(const GhmElementArray *elementArray)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        char *ptr = &cmdBuffer[4];
        sprintf(cmdBuffer, "gwr ");

        const GhmElement *elEnd = &elementArray->elements[elementArray->count];
        for(const GhmElement *el = &elementArray->elements[0]; el < elEnd; ++el)
        {
            if(el-> type == GHM_VALUE_INT)
            {
                ptr += sprintf(ptr, "%s,%lu|", el->elementName, el->u.intValue);
            }
            else
            {
                ptr += sprintf(ptr, "%s,%s|", el->elementName, el->u.strValue);
            }
        }
        *(ptr-1) = 0;
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    result = wiconnect->sendCommand(cmdBuffer);

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmSynchronize(GhmSyncType type)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        sprintf(cmdBuffer, "gsy %s", (type == GHM_SYNC_PUSH_ONLY) ? "push" :
                                     (type == GHM_SYNC_PULL_ONLY) ? "pull" : "");
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    result = wiconnect->sendCommand(cmdBuffer);

    CHECK_CLEANUP_COMMAND();

    return result;
}
