/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#include "Wiconnect.h"
#include "api/types/GhmMessageList.h"
#include "internal/common.h"


/*************************************************************************************************/
GhmMessageList::GhmMessageList(int bufferLen_, void *buffer_)
{
    Wiconnect *wiconnect = Wiconnect::getInstance();
    wiconnect_assert(wiconnect, "GhmMessageList(), bad buffer", (bufferLen_ == 0 && buffer_ == NULL) || (bufferLen_ != 0 && buffer_ != NULL));

    buffer = (uint8_t*)buffer_;
    bufferLen = bufferLen_;
    reset();
}

/*************************************************************************************************/
GhmMessageList::~GhmMessageList()
{
    reset();
}

/*************************************************************************************************/
void GhmMessageList::reset(void)
{
#ifdef WICONNECT_ENABLE_MALLOC
    if(buffer == NULL)
    {
        GhmMessage* result = listHead;
        while(result != NULL)
        {
            GhmMessage* tmp = result;
            result = result->next;
            delete tmp;
        }
    }
#endif
    listHead = listTail = NULL;
    bufferPtr = buffer;
    bufferRemaining = bufferLen;
    count = 0;
}

/*************************************************************************************************/
WiconnectResult GhmMessageList::add(const char *msgId, const char* timestamp, const char *length)
{
    WiconnectResult result;
    GhmMessage *res;

    if(buffer == NULL)
    {
#ifdef WICONNECT_ENABLE_MALLOC
        res = new GhmMessage();
        if(res == NULL)
#endif
        {
            return WICONNECT_NULL_BUFFER;
        }
    }
    else
    {
        if(bufferRemaining < sizeof(GhmMessage))
        {
            return WICONNECT_OVERFLOW;
        }
        res = (GhmMessage*)bufferPtr;
        memset(res, 0, sizeof(GhmMessage));
        bufferRemaining -= sizeof(GhmMessage);
        bufferPtr += sizeof(GhmMessage);
    }

    if(WICONNECT_FAILED(result, res->init(msgId, timestamp, length)))
    {
        if(buffer == NULL)
        {
            delete res;
        }
    }
    else
    {
        if(listHead == NULL)
        {
            listHead = listTail = res;
        }
        else
        {
            res->previous = listTail;
            listTail->next = res;
            listTail = res;
        }
        ++count;
    }

    return result;
}

/*************************************************************************************************/
const GhmMessage* GhmMessageList::getListHead() const
{
    return listHead;
}

/*************************************************************************************************/
int GhmMessageList::getCount() const
{
    return count;
}

/*************************************************************************************************/
const GhmMessage* GhmMessageList::getResult(int i) const
{
    if(i >= count)
        return NULL;

    GhmMessage* result = listHead;
    while(i-- != 0)
        result = result->next;

    return result;
}

/*************************************************************************************************/
const GhmMessage* GhmMessageList::operator [](int i) const
{
    return getResult(i);
}





