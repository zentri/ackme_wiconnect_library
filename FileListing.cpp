/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */

#include "Wiconnect.h"
#include "internal/common.h"
#include "api/StringUtil.h"


static bool nameMatches(const char *needle, const char* haystack);



/*************************************************************************************************/
WiconnectResult FileInterface::listFiles(FileList &list, const char *name, FileType type, uint32_t version)
{
    WiconnectResult result;

    CHECK_OTHER_COMMAND_EXECUTING();

    if(WICONNECT_SUCCEEDED(result, wiconnect->sendCommand("ls -v")))
    {
        result = processFileList(wiconnect->internalBuffer, list, name, type, version);
    }

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult FileInterface::processFileList(char *responseStr, FileList &list, const char *name, FileType type, uint32_t version)
{
    WiconnectResult result = WICONNECT_SUCCESS;
    char *line, *savedLine;

    for(savedLine = responseStr; (line = StringUtil::strtok_r(savedLine, "\r\n", &savedLine)) != NULL;)
    {
        uint32_t tmp;
        char *toks[7], *savedTok;

        if(*line != '#')
        {
            continue;
        }
        savedTok = line + 2;

        for(int i = 0; i < 6 && (toks[i] = StringUtil::strtok_r(savedTok, " ", &savedTok)) != NULL; ++i)
        {
            if(toks[i] == NULL)
            {
                result = WICONNECT_RESPONSE_PARSE_ERROR;
                goto exit;
            }
        }


        if(name != NULL && !nameMatches(name, savedTok+1))
        {
            continue;
        }
        else if((type != FILE_TYPE_ANY) &&
                StringUtil::strHexToUint32((const char*)&toks[1][2], &tmp) &&
                (type != (FileType)tmp))
        {
            continue;
        }
        else if((version != 0) &&
                FileInterface::fileVersionStrToInt(toks[5], &tmp) &&
                (version != tmp))
        {
            continue;
        }
        else if(WICONNECT_FAILED(result, list.add(toks[1], toks[2], toks[4], toks[5], savedTok+1)))
        {
            goto exit;
        }
    }

    exit:
    return result;
}


/*************************************************************************************************/
static bool nameMatches(const char *needle, const char* haystack)
{
    const int haystackLen = strlen(haystack);

    if(*needle == '*')
    {
        const int n = strlen(needle + 1);

        if(n > haystackLen)
        {
            return false;
        }
        return strcmp(needle+1, &haystack[haystackLen - n]) == 0;
    }
    else
    {
        return strcmp(needle, haystack) == 0;
    }
}
