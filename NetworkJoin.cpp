/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#include "Wiconnect.h"
#include "internal/common.h"

/*************************************************************************************************/
WiconnectResult NetworkInterface::join(const char* ssid, const char *password, const Callback &completeHandler_)
{
    WiconnectResult result = WICONNECT_ERROR;

    enum
    {
        FS_SET_NETWORK_DOWN,
        FS_SET_SSID,
        FS_SET_PASSWORD,
        FS_NETWORK_UP,
        FS_GET_STATUS
    };

    CHECK_CALLBACK_AVAILABLE(completeHandler_);

    CHECK_OTHER_COMMAND_EXECUTING();

    if(wiconnect->internalProcessingState == FS_SET_NETWORK_DOWN)
    {
        if(WICONNECT_SUCCEEDED(result, wiconnect->sendCommand("network_down")))
        {
            wiconnect->internalProcessingState = FS_SET_SSID;
        }
    }

    if(wiconnect->internalProcessingState == FS_SET_SSID)
    {
        if(ssid == NULL ||
           WICONNECT_SUCCEEDED(result, wiconnect->sendCommand("set wlan.ssid %s", ssid)))
        {
            wiconnect->internalProcessingState = FS_SET_PASSWORD;
        }
    }

    if(wiconnect->internalProcessingState == FS_SET_PASSWORD)
    {
        if(password == NULL || *password == 0 ||
           WICONNECT_SUCCEEDED(result, wiconnect->sendCommand("set wlan.passkey %s", password)))
        {
            wiconnect->internalProcessingState = FS_NETWORK_UP;
        }
    }

    if(wiconnect->internalProcessingState == FS_NETWORK_UP)
    {
        if(WICONNECT_SUCCEEDED(result, wiconnect->sendCommand("network_up")))
        {
            if(!completeHandler_.isValid())
            {
                wiconnect->internalProcessingState = FS_GET_STATUS;
            }
#ifdef WICONNECT_ASYNC_TIMER_ENABLED
            else
            {
                monitorTimer.stop();
                completeHandler = completeHandler_;
                monitorTimer.start(this, &NetworkInterface::joinStatusMonitor, 1000);
            }
#endif
        }
    }

    if(wiconnect->internalProcessingState == FS_GET_STATUS)
    {
#define MAX_JOIN_TIME 30000
        TimeoutTimer timeout;

        status_loop:
        if(WICONNECT_SUCCEEDED(result, wiconnect->sendCommand("get wlan.join.result")))
        {
            int32_t status;
            if(!WICONNECT_FAILED(result, wiconnect->responseToInt32(&status)))
            {
                if((NetworkJoinResult)status == NETWORK_JOIN_RESULT_JOINING)
                {
                    if(timeout.timedOut(MAX_JOIN_TIME))
                    {
                        result = WICONNECT_TIMEOUT;
                    }
                    else if(!wiconnect->nonBlocking)
                    {
                        goto status_loop;
                    }
                    else
                    {
                        result = WICONNECT_PROCESSING;
                    }
                }
                else
                {
                    result = ((NetworkJoinResult)status == NETWORK_JOIN_RESULT_SUCCESS) ? WICONNECT_SUCCESS : WICONNECT_NOT_CONNECTED;
                }
            }
        }
    }

    CHECK_CLEANUP_COMMAND();

    return result;
}



/*************************************************************************************************/
WiconnectResult NetworkInterface::leave()
{
    WiconnectResult result;

    CHECK_OTHER_COMMAND_EXECUTING();

#ifdef WICONNECT_ASYNC_TIMER_ENABLED
    monitorTimer.stop();
#endif
    result = wiconnect->sendCommand("network_down");

    CHECK_CLEANUP_COMMAND();

    return result;
}


/*************************************************************************************************/
WiconnectResult NetworkInterface::getNetworkStatus(NetworkStatus *statusPtr)
{
    WiconnectResult result;

    CHECK_OTHER_COMMAND_EXECUTING();

    if(WICONNECT_SUCCEEDED(result, wiconnect->sendCommand("get network.status")))
    {
        int32_t status;
        if(!WICONNECT_FAILED(result, wiconnect->responseToInt32(&status)))
        {
            if(status != NETWORK_STATUS_DOWN)
            {
#ifdef WICONNECT_ASYNC_TIMER_ENABLED
                monitorTimer.stop();
#endif
            }
            *statusPtr = (NetworkStatus)status;
        }
    }

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult NetworkInterface::getNetworkJoinResult(NetworkJoinResult *joinResultPtr)
{
    WiconnectResult result;

    CHECK_OTHER_COMMAND_EXECUTING();

    if(WICONNECT_SUCCEEDED(result, wiconnect->sendCommand("get wlan.join.result")))
    {
        int32_t status;
        if(!WICONNECT_FAILED(result, wiconnect->responseToInt32(&status)))
        {
            if(status != NETWORK_JOIN_RESULT_JOINING)
            {
#ifdef WICONNECT_ASYNC_TIMER_ENABLED
                monitorTimer.stop();
#endif
            }
            *joinResultPtr = (NetworkJoinResult)status;
        }
    }

    CHECK_CLEANUP_COMMAND();

    return result;
}

#ifdef WICONNECT_ASYNC_TIMER_ENABLED


/*************************************************************************************************/
// this is called every 1s by the monitorTimer
void NetworkInterface::joinStatusMonitor()
{
    static char responseBuffer[4];
    static uint8_t cmdBuffer[sizeof(QueuedCommand)];
    QueuedCommand *cmd = (QueuedCommand*)cmdBuffer;

    monitorTimer.stop();

   *cmd = QueuedCommand(sizeof(responseBuffer), responseBuffer, "get wlan.join.result");

    wiconnect->enqueueCommand(cmd, Callback(this, &NetworkInterface::joinStatusCheckCallback));
}

/*************************************************************************************************/
// this is called on the completion of the 'get'wlan.join.result' command above
void NetworkInterface::joinStatusCheckCallback(WiconnectResult result, void *arg1, void *arg2)
{
    bool isComplete = false;
    NetworkJoinResult joinResult = NETWORK_JOIN_RESULT_NONE;

    QueuedCommand *cmd = (QueuedCommand*)arg1;

    if(result == WICONNECT_SUCCESS)
    {
        int32_t status;
        if(!StringUtil::strToInt32(cmd->responseBuffer, &status))
        {
            result = WICONNECT_RESPONSE_PARSE_ERROR;
        }
        else if((NetworkJoinResult)status != NETWORK_JOIN_RESULT_JOINING)
        {
            isComplete = true;
            joinResult = (NetworkJoinResult)status;
            result = (joinResult == NETWORK_JOIN_RESULT_SUCCESS) ? WICONNECT_SUCCESS : WICONNECT_NOT_CONNECTED;
        }
    }

    if(isComplete || result != WICONNECT_SUCCESS)
    {
        completeHandler.call(result, (void*)joinResult, NULL);
    }
    else
    {
        monitorTimer.start(this, &NetworkInterface::joinStatusMonitor, 1000);
    }
}

#endif
