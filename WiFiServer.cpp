#include <string.h>

extern "C" {
#include "api/types/WiFi/utility/wl_definitions.h"
#include "api/types/WiFi/utility/wl_types.h"
}

#include "api/types/WiFi/WiFi.h"
#include "api/types/WiFi/WiFiServer.h"

#include "Wiconnect.h"

extern Wiconnect *driver;


/*************************************************************************************************/
WiFiServer::WiFiServer(uint16_t port)
{
    _listenPort = port;
    _serverActive = false;
    _client = NULL;
}

/*************************************************************************************************/
void WiFiServer::begin()
{
    if(driver == NULL)
    {
        return;
    }
    else
    {
        WiconnectResult result;

        while(WICONNECT_IS_PROCESSING(result, driver->tcpListen(_listenPort, 1)))
        {
        }
        _serverActive = (result == WICONNECT_SUCCESS);
    }
}

/*************************************************************************************************/
int WiFiServer::available(WiFiClient &client)
{

    if(driver != NULL && _serverActive)
    {
        if(!client.connected())
        {
            WiconnectResult result;

            while(WICONNECT_IS_PROCESSING(result, driver->tcpAccept(client._socket)))
            {
            }
            _client = &client;
        }
        return client.connected();
    }

    return 0;
}

/*************************************************************************************************/
uint8_t WiFiServer::status()
{
    return (driver != NULL && _serverActive);
}

/*************************************************************************************************/
size_t WiFiServer::write(uint8_t b)
{
    return write(&b, 1);
}

/*************************************************************************************************/
size_t WiFiServer::write(const uint8_t *buffer, size_t size)
{
    if(!status() || _client == NULL)
    {
        return 0;
    }
    return _client->write(buffer, size);
}
