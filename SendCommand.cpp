/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */

#include "internal/CommandCommon.h"


/*************************************************************************************************/
WiconnectResult Wiconnect::sendCommand(const Callback &completeCallback, char *responseBuffer,
                                       int responseBufferLen, TimerTimeout timeoutMs, const ReaderFunc &reader, void *user,
                                       const char *cmd, va_list vaList)
{
    // preprocessor assertion
    ct_assert(sizeof(commandContext) >=sizeof(CommandContext));
    CHECK_INITIALIZED();
    CHECK_NULL_BUFFER(responseBuffer);
    if(commandExecuting)
    {
        return checkCurrentCommand();
    }

    CommandContext *context = (CommandContext*)commandContext;
    CommandHeader *header = (CommandHeader*)commandHeaderBuffer;

    int len = vsnprintf(commandFormatBuffer, sizeof(commandFormatBuffer)-3, cmd, vaList);

    if(len > (int)(sizeof(commandFormatBuffer)-3))
    {
        DEBUG_ERROR("Command overflowed: %d > %d", len, sizeof(commandFormatBuffer)-3);
        return WICONNECT_OVERFLOW;
    }

    commandFormatBuffer[len++] = '\r';
    commandFormatBuffer[len++] = '\n';
    commandFormatBuffer[len] = 0;

    RESET_CMD_HEADER(header);

    memset(context, 0, sizeof(CommandContext));
    context->responseBuffer = responseBuffer;
    context->responseBufferPtr = context->responseBuffer;
    context->responseBufferLen = responseBufferLen;
    context->commandLen = len;
    context->commandPtr = commandFormatBuffer;
    context->reader = reader;
    context->user = user;
    context->timeoutMs = timeoutMs;
    context->callback = completeCallback;
    context->nonBlocking = nonBlocking;

    DEBUG_CMD_SEND(commandFormatBuffer);

    commandExecuting = true;
    flush(0);
    timeoutTimer.reset();

    return checkCurrentCommand();
}


/*************************************************************************************************/
WiconnectResult Wiconnect::sendCommand(char *responseBuffer, int responseBufferLen, TimerTimeout timeoutMs,
                                       const ReaderFunc &reader, void *user, const char *cmd, va_list vaList)
{
    return sendCommand(Callback(), responseBuffer, responseBufferLen, timeoutMs, reader, user, cmd, vaList);
}

/*************************************************************************************************/
WiconnectResult Wiconnect::sendCommand(char *responseBuffer, int responseBufferLen,  TimerTimeout timeoutMs,
                                       const ReaderFunc &reader, void *user, const char *cmd, ...)
{
    WiconnectResult result;
    va_list args;
    va_start(args, cmd);
    result = sendCommand(Callback(), responseBuffer, responseBufferLen, timeoutMs, reader, user, cmd, args);
    va_end(args);
    return result;
}

/*************************************************************************************************/
WiconnectResult Wiconnect::sendCommand(TimerTimeout timeoutMs, const ReaderFunc &reader, void *user, const char *cmd, ...)
{
    WiconnectResult result;
    va_list args;
    va_start(args, cmd);
    result = sendCommand(Callback(), internalBuffer, internalBufferSize, timeoutMs, reader, user, cmd, args);
    va_end(args);
    return result;
}

/*************************************************************************************************/
WiconnectResult Wiconnect::sendCommand(const ReaderFunc &reader, void *user, const char *cmd, ...)
{
    WiconnectResult result;
    va_list args;
    va_start(args, cmd);
    result = sendCommand(Callback(), internalBuffer, internalBufferSize, defaultTimeoutMs, reader, user, cmd, args);
    va_end(args);
    return result;
}

/*************************************************************************************************/
WiconnectResult Wiconnect::sendCommand(char *responseBuffer, int responseBufferLen,  TimerTimeout timeoutMs, const char *cmd, ...)
{
    WiconnectResult result;
    va_list args;
    va_start(args, cmd);
    result = sendCommand(Callback(), responseBuffer, responseBufferLen, timeoutMs, ReaderFunc(), NULL, cmd, args);
    va_end(args);
    return result;
}

/*************************************************************************************************/
WiconnectResult Wiconnect::sendCommand(const Callback &completeCallback, char *responseBuffer, int responseBufferLen, const char *cmd, ...)
{
    WiconnectResult result;
    va_list args;
    va_start(args, cmd);
    result = sendCommand(completeCallback, responseBuffer, responseBufferLen, defaultTimeoutMs, ReaderFunc(), NULL, cmd, args);
    va_end(args);
    return result;
}

/*************************************************************************************************/
WiconnectResult Wiconnect::sendCommand(char *responseBuffer, int responseBufferLen, const char *cmd, ...)
{
    WiconnectResult result;
    va_list args;
    va_start(args, cmd);
    result = sendCommand(Callback(), responseBuffer, responseBufferLen, defaultTimeoutMs, ReaderFunc(), NULL, cmd, args);
    va_end(args);
    return result;
}

/*************************************************************************************************/
WiconnectResult Wiconnect::sendCommand(const Callback &completeCallback, const char *cmd, ...)
{
    WiconnectResult result;
    va_list args;
    va_start(args, cmd);
    result = sendCommand(completeCallback, internalBuffer, internalBufferSize, defaultTimeoutMs, ReaderFunc(), NULL, cmd, args);
    va_end(args);
    return result;
}

/*************************************************************************************************/
WiconnectResult Wiconnect::sendCommand(const char *cmd, ...)
{
    WiconnectResult result;
    va_list args;
    va_start(args, cmd);
    result = sendCommand(Callback(), internalBuffer, internalBufferSize, defaultTimeoutMs, ReaderFunc(), NULL, cmd, args);
    va_end(args);
    return result;
}

/*************************************************************************************************/
WiconnectResult Wiconnect::sendCommand(const Callback &completeCallback, TimerTimeout timeoutMs, const char *cmd, ...)
{
    WiconnectResult result;
    va_list args;
    va_start(args, cmd);
    result = sendCommand(completeCallback, internalBuffer, internalBufferSize, timeoutMs, ReaderFunc(), NULL, cmd, args);
    va_end(args);
    return result;
}

/*************************************************************************************************/
WiconnectResult Wiconnect::sendCommand(TimerTimeout timeoutMs, const char *cmd, ...)
{
    WiconnectResult result;
    va_list args;
    va_start(args, cmd);
    result = sendCommand(Callback(), internalBuffer, internalBufferSize, timeoutMs, ReaderFunc(), NULL, cmd, args);
    va_end(args);
    return result;
}

/*************************************************************************************************/
WiconnectResult Wiconnect::sendCommand(const char *cmd, va_list vaList)
{
    return sendCommand(Callback(), internalBuffer, internalBufferSize, defaultTimeoutMs, ReaderFunc(), NULL, cmd, vaList);
}

