/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#pragma once


#include <stdio.h>
#include <stdarg.h>
#include "Wiconnect.h"
#include "api/StringUtil.h"
#include "internal/common.h"


#define CHECK_NULL_BUFFER(buf) if(buf == NULL) return WICONNECT_NULL_BUFFER
#define CHECK_INITIALIZED() if(!initialized) return WICONNECT_NOT_INITIALIZED
#define RESET_CMD_HEADER(header)  memset(header, 0, sizeof(CommandHeader)); header->bytes_remaining = WICONNECT_HEADER_LENGTH

#ifdef WICONNECT_ENABLE_DEBUGGING
#define DEBUG_CMD_SEND(cmd)         debugLog(">> CMD: %s", cmd)
#define DEBUG_CMD_LOG(res)          debugLog("<< LOG: %s", res)
#define DEBUG_CMD_RESPONSE(res)     debugLog("<< RES: %s", res)
#define DEBUG_CMD_ERROR(code)       debugLog("<< ERR:(%d) %s", code, getLastCommandResponseCodeStr())
#define DEBUG_ERROR(msg, ...)       debugLog("   ERR: " msg, ## __VA_ARGS__)
#define DEBUG_INFO(msg, ...)        debugLog("   DBG: " msg, ## __VA_ARGS__)
#else
#define DEBUG_CMD_SEND(cmd)
#define DEBUG_CMD_LOG(res)
#define DEBUG_CMD_RESPONSE(res)
#define DEBUG_CMD_ERROR(code)
#define DEBUG_ERROR(msg, ...)
#define DEBUG_INFO(msg, ...)
#endif


typedef enum
{
    WICONNECT_CMD_TYPE_NULL = 0,
    WICONNECT_CMD_TYPE_REPLY = 'R',
    WICONNECT_CMD_TYPE_LOG = 'L',
    WICONNECT_CMD_TYPE_SAFEMODE = 'S'
} ResponseType;

typedef enum
{
    WICONNECT_CMD_CODE_NULL         = 0,
    WICONNECT_CMD_SUCCESS           = 1, // The command was successful
    WICONNECT_CMD_FAILED            = 2, // The command failed, most likely in the firmware
    WICONNECT_CMD_PARSE_ERROR       = 3, // Failed to parse the command
    WICONNECT_CMD_UNKNOWN           = 4, // Unknown command
    WICONNECT_CMD_TOO_FEW_ARGS      = 5, // Not enough command arguments
    WICONNECT_CMD_TOO_MANY_ARGS     = 6, // Too many command arguments
    WICONNECT_CMD_UNKNOWN_OPTION    = 7, // Unknown option (e.g. known 'set' command option)
    WICONNECT_CMD_BAD_ARGS          = 8, // Invalid command arguments
} ResponseCode;

#define WICONNECT_HEADER_LENGTH 9
typedef struct
{
    ResponseType response_type;
    ResponseCode response_code;
    uint16_t response_len;
    uint8_t len_buffer[WICONNECT_HEADER_LENGTH];
    uint8_t *len_buffer_ptr;
    uint8_t bytes_remaining;
} CommandHeader;

typedef struct
{
    char *responseBuffer;
    char *responseBufferPtr;
    int responseBufferLen;
    ReaderFunc reader;
    void *user;
    char *commandPtr;
    int commandLen;
    int bytesToWrite;
    int bytesToRead;
    TimerTimeout timeoutMs;
    Callback callback;
    bool nonBlocking;
} CommandContext;

