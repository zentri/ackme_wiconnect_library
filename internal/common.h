/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#ifndef _COMMON_H_
#define _COMMON_H_


#include "WiconnectCommands.h"



/* Note we need the 2 concats below because arguments to ##
 * are not expanded, so we need to expand __LINE__ with one indirection
 * before doing the actual concatenation. */
#define ASSERT_CONCAT_(a, b) a ## b
#define ASSERT_CONCAT(a, b) ASSERT_CONCAT_(a, b)
#define ct_assert(e) enum { ASSERT_CONCAT(assert_line_, __LINE__) = 1/(!!(e)) }

#define wiconnect_assert(_wiconnect, msg, expr) if(!(expr)){if(_wiconnect != NULL) _wiconnect->assertLogger.call(msg); for(;;); }



#ifndef WICONNECT_ASYNC_TIMER_ENABLED
#define CHECK_CALLBACK_AVAILABLE(cb) if(cb.isValid()) return WICONNECT_UNSUPPORTED
#else
#define CHECK_CALLBACK_AVAILABLE(cb)
#endif

#define UNUSED(expr)  do { (void)(expr); } while (0)

#define CHECK_CLEANUP_COMMAND()                                     \
    if(result != WICONNECT_PROCESSING)                              \
    {                                                               \
        wiconnect->internalProcessingState = 0;                     \
        wiconnect->currentCommandId = NULL;                         \
    }

#define CHECK_OTHER_COMMAND_EXECUTING()                             \
{                                                                   \
    static const volatile uint8_t __funcId = 0;                     \
    if(wiconnect->currentCommandId == NULL)                         \
    {                                                               \
        wiconnect->currentCommandId = (void*)&__funcId;             \
    }                                                               \
    else if(wiconnect->currentCommandId != (void*)&__funcId)        \
    {                                                               \
        return WICONNECT_ANOTHER_CMD_EXECUTING;                     \
    }                                                               \
}

#define _CHECK_CLEANUP_COMMAND()                                    \
    if(result != WICONNECT_PROCESSING)                              \
    {                                                               \
        internalProcessingState = 0;                                \
        currentCommandId = NULL;                                    \
    }

#define _CHECK_OTHER_COMMAND_EXECUTING()                            \
{                                                                   \
    static const volatile uint8_t __funcId = 0;                     \
    if(currentCommandId == NULL)                                    \
    {                                                               \
        currentCommandId = (void*)&__funcId;                        \
    }                                                               \
    else if(currentCommandId != (void*)&__funcId)                   \
    {                                                               \
        return WICONNECT_ANOTHER_CMD_EXECUTING;                     \
    }                                                               \
}

#define WICONNECT_IS_IDLE() (wiconnect->currentCommandId == NULL)
#define _WICONNECT_IS_IDLE() (currentCommandId == NULL)



#ifdef WICONNECT_USE_DEFAULT_STRING_BUFFERS
#define SET_STR_BUFFER(_buffer, size)                               \
    char *ptr;                                                      \
    static char defaultBuffer[size];                                \
    ptr = (_buffer == NULL) ? defaultBuffer : _buffer;
#else
#define SET_STR_BUFFER(_buffer, size)                               \
    char *ptr;                                                      \
    if(_buffer == NULL)                                             \
    {                                                               \
        return "<null>";                                            \
    }                                                               \
    ptr = _buffer;
#endif


#endif
