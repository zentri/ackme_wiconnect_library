/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */

#include "Wiconnect.h"
#include "internal/common.h"

#ifdef WICONNECT_ASYNC_TIMER_ENABLED

/*************************************************************************************************/
QueuedCommand::QueuedCommand(int responseBufferLen_, char *responseBuffer_, int timeoutMs_, const ReaderFunc &reader_, void *user_, const char *cmd_, va_list vaList)
{
    initialize(responseBufferLen_, responseBuffer_, timeoutMs_, reader_, user_, cmd_, vaList);
}

/*************************************************************************************************/
QueuedCommand::QueuedCommand(int responseBufferLen_, char* responseBuffer_, int timeoutMs_, const char *cmd_, ...)
{
    va_list args;
    va_start(args, cmd_);
    initialize(responseBufferLen_, responseBuffer_, timeoutMs_, ReaderFunc(), NULL, cmd_, args);
    va_end(args);
}

/*************************************************************************************************/
QueuedCommand::QueuedCommand(int responseBufferLen_, char *responseBuffer_, const char *cmd_, ...)
{
    va_list args;
    va_start(args, cmd_);
    initialize(responseBufferLen_, responseBuffer_, WICONNECT_DEFAULT_TIMEOUT, ReaderFunc(), NULL, cmd_, args);
    va_end(args);
}

/*************************************************************************************************/
QueuedCommand::QueuedCommand(int timeoutMs_, const char *cmd_, ...)
{
    va_list args;
    va_start(args, cmd_);
    initialize(0, NULL, timeoutMs_, ReaderFunc(), NULL, cmd_, args);
    va_end(args);
}

/*************************************************************************************************/
QueuedCommand::QueuedCommand(const char *cmd_, ...)
{
    va_list args;
    va_start(args, cmd_);
    initialize(0, NULL, WICONNECT_DEFAULT_TIMEOUT, ReaderFunc(), NULL, cmd_, args);
    va_end(args);
}

/*************************************************************************************************/
QueuedCommand::~QueuedCommand()
{
#ifdef WICONNECT_ENABLE_MALLOC
    if(allocatedBuffer)
    {
        Wiconnect::getInstance()->_free(responseBuffer);
    }
#endif
}

/*************************************************************************************************/
char *QueuedCommand::getResponseBuffer()
{
    return responseBuffer;
}
/*************************************************************************************************/
int QueuedCommand::getResponseBufferLen()
{
    return responseBufferLen;
}
/*************************************************************************************************/
int QueuedCommand::getTimeoutMs()
{
    return timeoutMs;
}
/*************************************************************************************************/
ReaderFunc QueuedCommand::getReader()
{
    return reader;
}
/*************************************************************************************************/
void * QueuedCommand::getReaderUserData()
{
    return user;
}
/*************************************************************************************************/
char* QueuedCommand::getCommand()
{
    return command;
}
/*************************************************************************************************/
Callback QueuedCommand::getCompletedCallback()
{
    return completeCallback;
}
/*************************************************************************************************/
void QueuedCommand::setCompletedCallback(const Callback &cb)
{
    completeCallback = cb;
}

/*************************************************************************************************/
QueuedCommand& QueuedCommand::operator=( const QueuedCommand& other )
{
    responseBuffer = other.responseBuffer;
    responseBufferLen = other.responseBufferLen;
    timeoutMs = other.timeoutMs;
    reader = other.reader;
    user = other.user;
    completeCallback = other.completeCallback;
    memcpy(command, other.command, sizeof(command));
    return *this;
}

/*************************************************************************************************/
void* QueuedCommand::operator new(size_t size)
{
    Wiconnect *wiconnect = Wiconnect::getInstance();
    wiconnect_assert(wiconnect, "QueuedCommand:new malloc not defined", wiconnect->_malloc != NULL);
    return Wiconnect::getInstance()->_malloc(size);
}

/*************************************************************************************************/
void QueuedCommand::operator delete(void* ptr)
{
    Wiconnect *wiconnect = Wiconnect::getInstance();
    wiconnect_assert(wiconnect, "QueuedCommand:delete free not defined", wiconnect->_free != NULL);
    Wiconnect::getInstance()->_free(ptr);
}

/*************************************************************************************************/
void QueuedCommand::initialize(int responseBufferLen_, char *responseBuffer_, int timeoutMs_, const ReaderFunc &reader_, void *user_, const char *cmd_, va_list vaList)
{
    if(responseBufferLen_ > 0)
    {
#ifdef WICONNECT_ENABLE_MALLOC
        Wiconnect *wiconnect = Wiconnect::getInstance();
        allocatedBuffer = false;
        if(responseBuffer_ == NULL)
        {
            wiconnect_assert(wiconnect, "QueuedCommand() malloc not defined", wiconnect->_malloc != NULL);
            responseBuffer = (char*)wiconnect->_malloc(responseBufferLen_);
            wiconnect_assert(wiconnect, "QueuedCommand() responseBuffer malloc failed", responseBuffer != NULL);
            allocatedBuffer = true;
        }
        else
#endif
        {
            wiconnect_assert(wiconnect, "QueuedCommand(), null buffer", responseBuffer_ != NULL);
            responseBuffer = responseBuffer_;
        }
    }
    responseBufferLen = responseBufferLen_;
    timeoutMs = timeoutMs_;
    reader = reader_;
    user = user_;
    userData = NULL;

    if(cmd_ != NULL)
    {
        int len = vsnprintf(command, sizeof(command)-3, cmd_, vaList);
        command[len++] = '\r';
        command[len++] = '\n';
        command[len] = 0;
    }
}


#endif
