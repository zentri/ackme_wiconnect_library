/**
 * ACKme WiConnect Host Library is licensed under the BSD licence:
 *
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 */

#include "Wiconnect.h"
#include "internal/common.h"
#include "api/StringUtil.h"


#define GHM_HOST_URL "api.gohack.me"


/*************************************************************************************************/
WiconnectResult GhmInterface::ghmPostMessage(WiconnectSocket &socket, bool jsonFormatted)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        strcpy(cmdBuffer, "gme post ");
        if(jsonFormatted)
        {
            strcat(cmdBuffer, "json");
        }
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    if(WICONNECT_SUCCEEDED(result, wiconnect->sendCommand(cmdBuffer)))
    {
        int32_t handle;
        if(!WICONNECT_FAILED(result, wiconnect->responseToInt32(&handle)))
        {
            socket.init(handle, SOCKET_TYPE_GHM, GHM_HOST_URL, 443, SOCKET_ANY_PORT);
        }
    }

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmGetMessage(WiconnectSocket &socket, GhmMessageGetType getType)
{
    return ghmGetMessage(socket, 255, NULL, getType);
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmGetMessage(WiconnectSocket &socket, uint8_t listIndex, GhmMessageGetType getType)
{
    return ghmGetMessage(socket, listIndex, NULL, getType);
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmGetMessage(WiconnectSocket &socket, const char *msgId, GhmMessageGetType getType)
{
    return ghmGetMessage(socket, 255, msgId, getType);
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmGetMessage(WiconnectSocket &socket, uint8_t listIndex, const char *msgId, GhmMessageGetType getType)
{
    WiconnectResult result;
    int32_t handle;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        char *ptr = &cmdBuffer[7];
        strcpy(cmdBuffer, "gme get");
        if(listIndex != 255)
        {
            sprintf(ptr, " %u", listIndex);
        }
        else if(msgId != NULL)
        {
            sprintf(ptr, " %s", msgId);
        }

        if(getType == GHM_MSG_GET_BODY)
        {
            strcat(cmdBuffer, " body");
        }
        else if(getType == GHM_MSG_GET_ALL)
        {
            strcat(cmdBuffer, " all");
        }
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    if(WICONNECT_SUCCEEDED(result, wiconnect->sendCommand(cmdBuffer)))
    {
        if(!WICONNECT_FAILED(result, wiconnect->responseToInt32(&handle)))
        {
            socket.init(handle, SOCKET_TYPE_GHM, GHM_HOST_URL, 443, SOCKET_ANY_PORT);
        }
    }

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmDeleteMessage(uint8_t listIndex)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        sprintf(cmdBuffer, "gme delete %u", listIndex);
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    result = wiconnect->sendCommand(cmdBuffer);

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmDeleteMessage(const char *msgId)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        sprintf(cmdBuffer, "gme delete %s", msgId);
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    result = wiconnect->sendCommand(cmdBuffer);

    CHECK_CLEANUP_COMMAND();

    return result;
}

/*************************************************************************************************/
WiconnectResult GhmInterface::ghmListMessages(GhmMessageList &msgList, uint8_t maxCount)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;

    if(WICONNECT_IS_IDLE())
    {
        msgList.reset();
        strcpy(cmdBuffer, "gme list");
        if(maxCount != 0)
        {
            sprintf(&cmdBuffer[8], " -c %u", maxCount);
        }
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    if(WICONNECT_SUCCEEDED(result, wiconnect->sendCommand(cmdBuffer)))
    {
        result = processMessageList(wiconnect->internalBuffer, msgList);
    }

    CHECK_CLEANUP_COMMAND();

    return result;
}


/*************************************************************************************************/
WiconnectResult GhmInterface::processMessageList(char *resultStr, GhmMessageList &resultList)
{
    WiconnectResult result = WICONNECT_SUCCESS;
    char *line, *savedLine;

    for(savedLine = resultStr; (line = StringUtil::strtok_r(savedLine, "\r\n", &savedLine)) != NULL;)
    {
        char *toks[4], *savedTok;

        if(*line != '#')
        {
            continue;
        }
        savedTok = line + 2;

        for(int i = 0; i < 4 && (toks[i] = StringUtil::strtok_r(savedTok, " ", &savedTok)) != NULL; ++i)
        {
            if(toks[i] == NULL)
            {
                result = WICONNECT_RESPONSE_PARSE_ERROR;
                goto exit;
            }
        }

        if(WICONNECT_FAILED(result, resultList.add(toks[1], toks[2], toks[3])))
        {
            goto exit;
        }
    }

    exit:
    return result;
}
