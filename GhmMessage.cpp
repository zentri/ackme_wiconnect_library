/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#include "Wiconnect.h"
#include "api/types/GhmMessage.h"
#include "internal/common.h"




/*************************************************************************************************/
GhmMessage::GhmMessage()
{
    next = NULL;
    previous = NULL;
    messageId[0] = 0;
    timestamp[0] = 0;
    length = 0;
}

/*************************************************************************************************/
WiconnectResult GhmMessage::init(const char *msgIdStr, const char* timestampStr, const char *lengthStr)
{
    intmax_t r;
    if(!StringUtil::parseHex(lengthStr, &r, 0, USHRT_MAX))
    {
        return WICONNECT_RESPONSE_PARSE_ERROR;
    }
    length = (uint16_t)r;

    strncpy(messageId, msgIdStr, sizeof(messageId));
    strncpy(timestamp, timestampStr, sizeof(timestamp));

    return WICONNECT_SUCCESS;
}

#ifdef WICONNECT_ENABLE_MALLOC
/*************************************************************************************************/
void* GhmMessage::operator new(size_t size)
{
    Wiconnect *wiconnect = Wiconnect::getInstance();
    wiconnect_assert(wiconnect, "GhmMessage:new, malloc not defined", wiconnect->_malloc != NULL);
    return Wiconnect::getInstance()->_malloc(size);
}

/*************************************************************************************************/
void GhmMessage::operator delete(void* ptr)
{
    Wiconnect *wiconnect = Wiconnect::getInstance();
    wiconnect_assert(wiconnect, "GhmMessage:delete, free not defined", wiconnect->_free != NULL);
    Wiconnect::getInstance()->_free(ptr);
}
#endif

/*************************************************************************************************/
const char* GhmMessage::getMessageId(char *buffer) const
{
    const char *ptr;
    if(buffer != NULL)
    {
        strcpy(buffer, messageId);
        ptr = buffer;
    }
    else
    {
        ptr = messageId;
    }
    return ptr;
}

/*************************************************************************************************/
const char* GhmMessage::getTimestamp(char *buffer) const
{
    const char *ptr;
    if(buffer != NULL)
    {
        strcpy(buffer, timestamp);
        ptr = buffer;
    }
    else
    {
        ptr = timestamp;
    }
    return ptr;
}

/*************************************************************************************************/
uint16_t GhmMessage::getLength() const
{
    return length;
}

/*************************************************************************************************/
const GhmMessage* GhmMessage::getNext() const
{
    return next;
}

/*************************************************************************************************/
const GhmMessage* GhmMessage::getPrevious() const
{
    return previous;
}

