/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */


#include "Wiconnect.h"
#include "api/types/FileList.h"



/*************************************************************************************************/
FileList::FileList(int bufferLen_, void *buffer_)
{
    count = 0;
    listHead = listTail = NULL;
    buffer = (uint8_t*)buffer_;
    bufferPtr = buffer;
    bufferLen = bufferLen_;
}

/*************************************************************************************************/
FileList::~FileList()
{
    if(buffer == NULL)
    {
        WiconnectFile* result = listHead;
        while(result != NULL)
        {
            WiconnectFile* tmp = result;
            result = result->next;
            delete tmp;
        }
    }
}

/*************************************************************************************************/
WiconnectResult FileList::add(const char *typeStr, const char *flagsStr, const char* sizeStr, const char *versionStr, const char *nameStr)
{
    WiconnectResult result;
    WiconnectFile *res;

    if(buffer == NULL)
    {
#ifdef WICONNECT_ENABLE_MALLOC
        res = new WiconnectFile();
        if(res == NULL)
#endif
        {
            return WICONNECT_NULL_BUFFER;
        }
    }
    else
    {
        if(bufferLen < sizeof(WiconnectFile))
        {
            return WICONNECT_OVERFLOW;
        }
        res = (WiconnectFile*)bufferPtr;
        memset(res, 0, sizeof(WiconnectFile));
        bufferLen -= sizeof(WiconnectFile);
        bufferPtr += sizeof(WiconnectFile);
    }

    if(WICONNECT_FAILED(result, res->initWithListing(typeStr, flagsStr, sizeStr, versionStr, nameStr)))
    {
        if(buffer == NULL)
        {
            delete res;
        }
    }
    else
    {
        if(listHead == NULL)
        {
            listHead = listTail = res;
        }
        else
        {
            res->previous = listTail;
            listTail->next = res;
            listTail = res;
        }
        ++count;
    }

    return result;
}

/*************************************************************************************************/
const WiconnectFile* FileList::getListHead() const
{
    return listHead;
}

/*************************************************************************************************/
int FileList::getCount() const
{
    return count;
}

/*************************************************************************************************/
const WiconnectFile* FileList::getResult(int i) const
{
    if(i >= count)
        return NULL;

    WiconnectFile* result = listHead;
    while(i-- != 0)
        result = result->next;

    return result;
}

/*************************************************************************************************/
const WiconnectFile* FileList::operator [](int i) const
{
    return getResult(i);
}

