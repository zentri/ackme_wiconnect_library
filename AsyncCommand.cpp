/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */

#include "internal/CommandCommon.h"

#ifdef WICONNECT_ASYNC_TIMER_ENABLED



/*************************************************************************************************/
WiconnectResult Wiconnect::enqueueCommand(QueuedCommand *command, const Callback &commandCompleteHandler)
{
    if(commandQueue.isFull())
    {
        return WICONNECT_OVERFLOW;
    }
    command->setCompletedCallback(commandCompleteHandler);
    DEBUG_INFO("Queuing command: %s", command->getCommand());
    commandQueue.push(command);
    processNextQueuedCommand();
    return WICONNECT_SUCCESS;
}

/*************************************************************************************************/
void Wiconnect::processNextQueuedCommand()
{
    if(commandQueue.isEmpty())
    {
        return;
    }
    else if(commandExecuting)
    {
        return;
    }

    CommandContext *context = (CommandContext*)commandContext;
    CommandHeader *header = (CommandHeader*)commandHeaderBuffer;

    commandExecuting = true;
    currentQueuedCommand = commandQueue.pop();

    DEBUG_INFO("Processing next cmd in queue");

    strcpy(commandFormatBuffer, currentQueuedCommand->command);

    RESET_CMD_HEADER(header);

    memset(context, 0, sizeof(CommandContext));
    if(currentQueuedCommand->responseBufferLen > 0)
    {
        context->responseBuffer = currentQueuedCommand->responseBuffer;
        context->responseBufferLen = currentQueuedCommand->responseBufferLen;
    }
    else
    {
        context->responseBuffer = internalBuffer;
        context->responseBufferLen = internalBufferSize;
    }

    context->responseBufferPtr = context->responseBuffer;
    context->commandLen = strlen(commandFormatBuffer);
    context->commandPtr = commandFormatBuffer;
    context->reader = currentQueuedCommand->reader;
    context->user = currentQueuedCommand->userData;
    context->timeoutMs = currentQueuedCommand->timeoutMs;
    context->callback = currentQueuedCommand->completeCallback;
    context->nonBlocking = true;

    DEBUG_CMD_SEND(commandFormatBuffer);

    commandExecuting = true;
    timeoutTimer.reset();

    commandProcessorTimer.start(this, &Wiconnect::commandProcessingTimerHandler, commandProcessingPeriod);
}

/*************************************************************************************************/
void Wiconnect::setCommandProcessingPeriod(uint32_t periodMs)
{
    commandProcessingPeriod = periodMs;
}

/*************************************************************************************************/
void Wiconnect::commandProcessingTimerHandler()
{
    checkCurrentCommand();
}

#endif

