/**
 * ACKme WiConnect Host Library is licensed under the BSD licence: 
 * 
 * Copyright (c)2014 ACKme Networks.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the author may not be used to endorse or promote products 
 * derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 */
#include "Wiconnect.h"
#include "internal/common.h"


#define SCAN_TIMEOUT 15000




/*************************************************************************************************/
WiconnectResult NetworkInterface::scan(ScanResultList &resultList, const uint8_t *channelList, const char* ssid)
{
    WiconnectResult result;
    char *cmdBuffer = wiconnect->internalBuffer;


    if(WICONNECT_IS_IDLE())
    {
#define SCAN_CMD "scan -v "
        char *cmdBufferPtr = cmdBuffer + sizeof(SCAN_CMD)-1;

        strcpy(cmdBuffer, SCAN_CMD);

        if(channelList != NULL)
        {
            for(const uint8_t *ch = (const uint8_t *)channelList; *ch != 0; ++ch)
            {
                cmdBufferPtr += sprintf(cmdBufferPtr, "%d,", *ch);
            }
            *(cmdBufferPtr-1) = ' ';
        }
        else
        {
            strcat(cmdBufferPtr, "all ");
            cmdBufferPtr += 4;
        }

        if(ssid != NULL)
        {
            strcpy(cmdBufferPtr, ssid);
        }

        resultList.reset();
    }

    CHECK_OTHER_COMMAND_EXECUTING();

    //if(!completeHandler_.isValid())
    {
        if(WICONNECT_SUCCEEDED(result, wiconnect->sendCommand(SCAN_TIMEOUT, cmdBuffer)))
        {
            result = processScanResults(wiconnect->internalBuffer, resultList);
        }
    }
//#ifdef WICONNECT_ASYNC_TIMER_ENABLED
//    else
//    {
//        QueuedCommand *cmd = new QueuedCommand(NULL, 4096, SCAN_TIMEOUT, cmdBuffer);
//        cmd->userData = (void*)resultList;
//        completeHandler = completeHandler_;
//        if(WICONNECT_FAILED(result, wiconnect->enqueueCommand(cmd, Callback(this, &NetworkInterface::scanCompleteCallback))))
//        {
//            delete cmd;
//        }
//        else
//        {
//            result = WICONNECT_PROCESSING;
//        }
//    }
//#endif

    CHECK_CLEANUP_COMMAND();

    return result;
}





/*************************************************************************************************/
WiconnectResult NetworkInterface::processScanResults(char *resultStr, ScanResultList &resultList)
{
    WiconnectResult result = WICONNECT_SUCCESS;
    char *line, *savedLine;

    for(savedLine = resultStr; (line = StringUtil::strtok_r(savedLine, "\r\n", &savedLine)) != NULL;)
    {
        char *toks[9], *savedTok;

        if(*line != '#')
        {
            continue;
        }
        savedTok = line + 2;

        for(int i = 0; i < 8 && (toks[i] = StringUtil::strtok_r(savedTok, " ", &savedTok)) != NULL; ++i)
        {
            if(toks[i] == NULL)
            {
                result = WICONNECT_RESPONSE_PARSE_ERROR;
                goto exit;
            }
        }

        if(WICONNECT_FAILED(result, resultList.add(toks[1], toks[2], toks[3], toks[4], toks[5], savedTok)))
        {
            goto exit;
        }
    }

    exit:
    return result;
}

//#ifdef WICONNECT_ASYNC_TIMER_ENABLED
//
/*************************************************************************************************/
//void NetworkInterface::scanCompleteCallback(WiconnectResult result, void *arg1, void *arg2)
//{
//    QueuedCommand *cmd = (QueuedCommand*)arg1;
//    ScanResultList *listPtr = (ScanResultList*)cmd->userData;
//
//    if(result == WICONNECT_SUCCESS)
//    {
//        result = processScanResults(cmd->responseBuffer, listPtr);
//    }
//    delete cmd;
//
//    completeHandler.call(result, listPtr, NULL);
//}
//
//#endif
